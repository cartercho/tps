File Name Rule for workProduct.

******************************************************
GuideLine : SDET3_TEAM2_TPS_DocName_V0.1_2017XXXX
******************************************************
If it is possible, "DocName" field follows the Folder Name of "doc" Dir

[Sample]
01.WBS : SDET3_TEAM2_TPS_WBS_V0.3_20170726
02.SWQualityPlan : SDET3_TEAM2_TPS_SWQualityPlan_V0.1_20170726
03.CodeReview : SDET3_TEAM2_TPS_CodeReview_V0.1_20170726
04.StaticAnalysis : SDET3_TEAM2_TPS_StaticAnalysis_V0.1_20170726
05.UnitTest :  SDET3_TEAM2_TPS_UnitTest_V0.1_20170726
06.SystemTest : SDET3_TEAM2_TPS_SystemTest_V0.1_20170726
07.AdminConsole : SDET3_TEAM2_TPS_AdminConsole_V0.1_20170726


End.