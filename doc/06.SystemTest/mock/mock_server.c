/**
 * This is a simple demo server capable of accepting connections from
 * the Tartan Parking System.
 *
 * Project: LG Exec Ed SDET Program
 * Copyright: 2017 Jeffrey S. Gennari
 * Versions:
 * 1.0 June 2017 - initial version
 */
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <resolv.h>
#include <arpa/inet.h>
#include <errno.h>

#define MY_PORT		5050
#define MAXBUF		1024

static char state[100] = "SU:NG=0;XG=0;NL=G;NIR=0;XIR=0;XL=0;PL=[1=0,2=0,3=0,4=0];PO=[1=0,2=0,3=0,4=0].\n";

#define MAX_MSG     10
typedef struct _Message {
    unsigned char index;
    char msg[4];
    unsigned char msg_len;
    unsigned char data_len;
}MessageT;

MessageT msg[MAX_MSG] = {
        {0, "GS", 2,  78},
        {1, "NL", 2,  1},
        {2, "XL", 2,  1},
        {3, "NG", 2,  1},
        {4, "XG", 2,  1},
        {5, "PL", 2,  17},
        {6, "NIR", 2,  1},
        {7, "XIR", 2,  1},
        {8, "PO", 2,  17},
        {9, "OK", 2,  0},
};


int main(int argc, char *argv[]) {

   int sockfd;
   struct sockaddr_in self;
   char buffer[MAXBUF];

   /*---Create streaming socket---*/
   if ( (sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0 )
   {
      perror("Socket");
      exit(errno);
   }

   /*---Initialize address/port structure---*/
   bzero(&self, sizeof(self));
   self.sin_family = AF_INET;
   self.sin_port = htons(MY_PORT);
   self.sin_addr.s_addr = INADDR_ANY;

   if (bind(sockfd, (struct sockaddr*)&self, sizeof(self))) {
      perror("socket--bind");
      exit(errno);
   }
   if (listen(sockfd, 20)) {
      perror("socket--listen");
      exit(errno);
   }
   printf("listening on port %u\n", MY_PORT);

   int clientfd;
   struct sockaddr_in client_addr;
   int addrlen=sizeof(client_addr);

   /*---accept a connection (creating a data pipe)---*/
   clientfd = accept(sockfd, (struct sockaddr*)&client_addr, &addrlen);
   printf("%s:%d connected\n", inet_ntoa(client_addr.sin_addr), ntohs(client_addr.sin_port));

   int do_exit = 0;
   while (1) {
      size_t amt = 0, bytes_received;
      char c;
      while (recv(clientfd,&c,1,0) > 0) {
         if (c == '.') break;
         buffer[amt] = c;
         amt++;

      }
      buffer[amt] = '\0';

      printf("Message received: %s\n", buffer);
      /* Handle get state */
      if (strcmp(buffer,"GS") == 0) {
         //char *state = "SU:NG=0;XG=0;NL=G;NIR=0;XIR=0;XL=0;PL=[1=0,2=0,3=0,4=0];PO=[1=0,2=0,3=0,4=0].\n";
         write(clientfd,state,strlen(state));
         printf("sent state %s\n", state);
         //do_exit = 0;
         do_exit++;
      } else if(strncmp(buffer,"NIR", 3) == 0 ) {   //send from tartan garage system(HW)
         char *temp = strstr(state, "NIR=");
         strncpy(temp+4, &buffer[4],1);
         write(clientfd,state,strlen(state));
         printf("sent state %s\n", state);
         //do_exit = 0;
         do_exit++;
      } else if(strncmp(buffer,"XIR", 3) == 0) {  //send from tartan garage system(HW)
          char *temp = strstr(state, "XIR=");
          strncpy(temp+4, &buffer[4],1);
          write(clientfd,state,strlen(state));
          printf("sent state %s\n", state);
          //do_exit = 0;
          do_exit++;
 
      } else if(strncmp(buffer,"PO",2) == 0) {  //send from tartan garage system(HW)
          char *temp = strstr(state, "PO=");
          strncpy(temp+3, &buffer[3],17);
          write(clientfd,state,strlen(state));
          printf("sent state %s\n", state);
          //do_exit = 0;
          do_exit++;
      } else if(strncmp(buffer,"PL",2) == 0) {  //send from Tartag Java application(SW)
          char *temp = strstr(state, "PL=");
          strncpy(temp+3, &buffer[3],17);
          char *ok = "OK.\n";
          write(clientfd,ok,strlen(ok));
          printf("sent OK\n");
      } else if( (strncmp(buffer, "NL",2) ==0) 
                || (strncmp(buffer, "XL",2) ==0) 
                || (strncmp(buffer, "NG",2) ==0) 
                || (strncmp(buffer, "XG",2) ==0) ) {  //send from Tartag Java application(SW)
          char temp_msg[4] = {0x0,};
          strncpy(temp_msg, buffer, 2);
          char *temp = strstr(state, temp_msg);
          strncpy(temp+3, &buffer[3],1);
          
          char *ok = "OK.\n";
          write(clientfd,ok,strlen(ok));
          printf("sent OK\n");

      } else {      
         char *ok = "OK.\n";
         write(clientfd,ok,strlen(ok));
         printf("sent OK\n");
      }
   }


   /*---Clean up (should never get here!)---*/
   close(sockfd);
   printf("socket closed");
   return 0;
}
