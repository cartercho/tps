package edu.cmu.tartan.test;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

/**
 * Created by suraj.malsingh on 7/27/2017.
 */
public class ReservationTestRunner {

    public static void main(String[] args) {

        // Load and run the tests defined in the LGTests class
        Result result = JUnitCore.runClasses(ReservationTest.class);

        // Report the failures and successses
        for (Failure failure : result.getFailures()) {
            System.out.println(failure.getTestHeader() + " : " + failure.getMessage());
        }

        System.out.println(result.wasSuccessful());
    }
}
