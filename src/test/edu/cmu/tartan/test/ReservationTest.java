package edu.cmu.tartan.test;

import edu.cmu.tartan.edu.cmu.tartan.reservation.Payment;
import edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation;
import edu.cmu.tartan.service.TartanParams;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.*;

/**
 * Created by suraj.malsingh on 7/26/2017.
 */
public class ReservationTest {

    /** The date format for start and end times */
    private SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy:MM:dd:HH:mm");

    @Test
    public void test_creatObjectWithDefaultConstructor(){
        boolean exceptionOccured = false;
        try {
        Reservation r = new Reservation();
        assertNotNull("Failed creating Revervation object with default constructor !!!",r);
        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Exception occurred while creating Revervation object with default constructor !!!",exceptionOccured, false);
    }

   @Test
    public void test_creatObjectWithNullParametersInConstructor(){
        boolean exceptionOccured = false;
        try {
        Reservation r = new Reservation(null,null,null,null);
        assertNotNull("Failed creating Revervation object with constructor with all null parameters!!!",r);
        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Exception occurred while creating Revervation object with constructor with all null parameters!!!",exceptionOccured, false);
    }


    @Test
    public void test_creatObjectWithEmptyParametersInConstructor(){
        boolean exceptionOccured = false;
        try {
            Reservation r = new Reservation("", "", "", "");
            assertNotNull("Failed Object creation while creating Reservation object with empty string parameters",r);
        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Exception occurred while creating Revervation object with constructor with all empty parameters!!!",exceptionOccured, false);
    }


    @Test
    public void test_serializable() {
        boolean exceptionOccured = false;
        try{
        Reservation obj1 = new Reservation();

        File f = new File("testSer.ser");

        FileOutputStream fs = new FileOutputStream(f);
        ObjectOutputStream os = new ObjectOutputStream(fs);
        os.writeObject(obj1);
        os.close();


        FileInputStream fis = new FileInputStream(f);
        ObjectInputStream ois = new ObjectInputStream(fis);
        Reservation obj2 = (Reservation) ois.readObject();

        ois.close();

        if (f.exists()){
            f.delete();
            f = null;
        }
        assertNotNull("DeSerialised object returned NULL",obj2); // Just checking if we are getting the object back and not NULL

        }catch(Exception e){
            e.printStackTrace();
            exceptionOccured = true;
        }
        assertEquals("Exception occurred while serialzation and deserialisation object!!!",exceptionOccured, false);
    }

    @Test
    public void test_isComplete_ForDefaultReservationObject()  {
        // Ching if reservation completed for an invalid reservation.
     boolean exceptionOccured = false;
        try {
            Reservation r = new Reservation();
            assertEquals(false,r.isComplete());
        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Exception occurred while serialzation and deserialisation object!!!",exceptionOccured, false);
    }

    @Test
    public void test_isComplete_ForInvalidReservation()  {
        // Ching if reservation completed for an invalid reservation.
        boolean exceptionOccured = false;
        try {
            Reservation r = new Reservation(null,null,null,null);
            assertEquals("Returns wrong with isComplete with reservation object with null parameters",false,r.isComplete());
        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Exception occured when checking isComplete for invalid reservation !!!",exceptionOccured, false);
    }

    @Test
    public void test_getSpotId_setSpotId_Valid() {
     boolean exceptionOccured = false;
        try {
            Reservation r = new Reservation();
            r.setSpotId(1234567);
            assertEquals("Problem occured while setting and getting  valid spot Id  !!! ",true,r.getSpotId() == 1234567);
        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Exception occured while setting and getting  valid spot Id  !!! ",exceptionOccured, false);
    }
    @Test
    public void test_getSpotId_setSpotId_ForZero() {
        boolean exceptionOccured = false;
        try {
            Reservation r = new Reservation();
            r.setSpotId(0);
            assertEquals("Probelm occured while setting and getting  spot Id as zero !!! ",true,r.getSpotId() == 0);
        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Exception occured while setting and getting  spot Id as zero !!! ",exceptionOccured, false);
    }
    @Test
    public void test_getSpotId_setSpotId_InvalidSpotId() {
        boolean exceptionOccured = false;
        try {
            Reservation r = new Reservation();
            r.setSpotId(TartanParams.INVALID_SPOT);
            assertEquals("Problem occured while setting and getting invalid spot Id !!! ",true,r.getSpotId() == TartanParams.INVALID_SPOT);
        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Exception occured while setting and getting invalid spot Id !!! ",exceptionOccured, false);
    }

    @Test
    public void test_getStartTime_setStartTime_validDate() {
     boolean exceptionOccured = false;
        try {
            Reservation r = new Reservation();
            Date d1 = new Date();
            r.setStartTime(d1);
            Date d2 = r.getStartTime();
            assertEquals("Problem occured while setting and getting valid date for startTime !!! ",true,d1.equals(d2));

        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Exception occured while setting and getting valid date for startTime !!! ",exceptionOccured, false);
    }

   @Test
    public void test_getStartTime_setStartTime_ValidString() {

     boolean exceptionOccured = false;
        try {
            Reservation r = new Reservation();
            Date d1 = new Date();
            r.setStartTime(sdFormat.format(d1));
            Date d2 = r.getStartTime();
            assertEquals("Problem occured while setting and getting valid string for startTime !!! ",true,sdFormat.format(d1).equals(sdFormat.format(d2)));
        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Exception occured while setting and getting valid string for startTime !!! ",exceptionOccured, false);
    }

    @Test
    public void test_getStartTime_setStartTime_InValidString() {
        boolean exceptionOccured = false;
        try {
            Reservation r = new Reservation();
            Date d1 = new Date();
            r.setStartTime(sdFormat.format(d1)+"WRONG SEED");
            Date d2 = r.getStartTime();
            //System.out.println(sdFormat.format(d2));
            assertEquals("Probelm occured while setting and getting valid date for startTime !!! ",true,sdFormat.format(d1).equals(sdFormat.format(d2)));
        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Exception occured while setting and getting valid date for startTime !!! ",exceptionOccured, false);
    }

    @Test
    public void test_getStartTime_setStartTime_EmptyString() {
        boolean exceptionOccured = false;
        try {
            Reservation r = new Reservation();
            Date d1 = new Date();
            r.setStartTime(sdFormat.format(""));
            Date d2 = r.getStartTime();
            assertEquals(true,sdFormat.format(d1).equals(sdFormat.format(d2)));

            r.setStartTime("");
            d2 = r.getStartTime();
            assertEquals("Problem in getStartTime when time was set as empty formatted string !!!",true,sdFormat.format(d1).equals(sdFormat.format(d2)));

        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Exception occured while retreiving start time when time was set to empty formatted string  !!!",exceptionOccured, false);
    }
    @Test
    public void test_getStartTime_setStartTime_EmptyFormattedString() {
        boolean exceptionOccured = false;
        try {
            Reservation r = new Reservation();
            Date d1 = new Date();
            r.setStartTime("");
            Date d2 = r.getStartTime();
            assertEquals("Problem when calling getStartTime when empty string is set in setStartTime !!!",true,sdFormat.format(d1).equals(sdFormat.format(d2)));

        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Exception occured when checking isComplete for invalid reservation !!!",exceptionOccured, false);
    }

     @Test
    public void test_getEndTime_setEndTime_validDate() {
     boolean exceptionOccured = false;
        try {
            Reservation r = new Reservation();
            Date d1 = new Date();
            r.setEndTime(d1);
            Date d2 = r.getEndTime();
            assertEquals("Probelm occured while setting and getting valid date for startTime !!! ",true,d1.equals(d2));

        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Exception occured while setting and getting valid date for startTime !!! ",exceptionOccured, false);
    }

   @Test
    public void test_getEndTime_setEndTime_ValidString() {

     boolean exceptionOccured = false;
        try {
            Reservation r = new Reservation();
            Date d1 = new Date();
            r.setEndTime(sdFormat.format(d1));
            Date d2 = r.getEndTime();
            assertEquals("Probelm occured while setting and getting invalid  string for startTime !!! ",true,sdFormat.format(d1).equals(sdFormat.format(d2)));
        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Exception occured while setting and getting valid  string for startTime !!! ",exceptionOccured, false);
    }

    @Test
    public void test_getEndTime_setEndTime_InValidString() {
        boolean exceptionOccured = false;
        try {
            Reservation r = new Reservation();
            Date d1 = new Date();
            r.setEndTime(sdFormat.format(d1)+"WRONG SEED");
            Date d2 = r.getEndTime();
            //System.out.println(sdFormat.format(d2));
            assertEquals("Probelm occured while setting and getting invalid  string for startTime !!! ",true,sdFormat.format(d1).equals(sdFormat.format(d2)));
        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Exception occured while setting and getting invalid  string for startTime !!! ",exceptionOccured, false);
    }

    @Test
    public void test_getEndTime_setEndTime_EmptyFormattedString() {
        boolean exceptionOccured = false;
        try {
            Reservation r = new Reservation();
            Date d1 = new Date();
            r.setEndTime(sdFormat.format(""));
            Date d2 = r.getEndTime();
            assertEquals("Problem occured when get set endTime with empty string !!!",true,sdFormat.format(d1).equals(sdFormat.format(d2)));
        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Exception occured when get set endTime with empty string !!!",exceptionOccured, false);
    }
    @Test
    public void test_getEndTime_setEndTime_EmptyString() {
        boolean exceptionOccured = false;
        try {
            Reservation r = new Reservation();
            Date d1 = new Date();
            r.setEndTime("");
            Date d2 = r.getEndTime();
            assertEquals("Problem occured while setting and getting end time with empty string !!!!!!",true,sdFormat.format(d1).equals(sdFormat.format(d2)));

        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Exception occured while setting and getting end time with empty string !!!",exceptionOccured, false);
    }


    @Test
    public void test_setIsRedeemed_getIsRedeemed_forTrue()  {
     boolean exceptionOccured = false;
        try {
            Reservation r = new Reservation();
            r.setIsRedeemed(true);
            assertEquals("Probelm occured while checking set and get isReddemed for True !!! ",true,r.getIsRedeemed());
        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Exception occured while checking set and get isReddemed for True !!! ",exceptionOccured, false);
    }

    @Test
    public void test_setIsRedeemed_getIsRedeemed_forFalse()  {
        boolean exceptionOccured = false;
        try {
            Reservation r = new Reservation();
            r.setIsRedeemed(false);
            assertEquals("Problem occured while checking set and get isReddemed for False !!! ",false,r.getIsRedeemed());
        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Exception occured while checking set and get isReddemed for False !!! ",exceptionOccured, false);
    }

    @Test
    public void test_setIsRedeemed_forNull_forException()  {
        boolean exceptionOccured = false;
        try {
            Reservation r = new Reservation();
            r.setIsRedeemed(null);
        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Exception occured while checking set isReddemed for Null !!! ",exceptionOccured, false);
    }

    @Test
    public void test_setIsRedeemed_getIsRedeemed_forNull_forException()  {
        boolean exceptionOccured = false;
        try {
            Reservation r = new Reservation();
            r.setIsRedeemed(null);
            assertEquals("Problem occured while checking set and get isReddemed for Null !!! ",null,r.getIsRedeemed());
        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Exception occured while checking set and get isReddemed for Null !!! ",exceptionOccured, false);
    }



    @Test
    public void test_setPayment_ValidPayment()   {
     boolean exceptionOccured = false;
        try {
            Reservation r = new Reservation();
            r.setPayment(new Payment());
        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Exception occured while checking seting valid payment !!! ",exceptionOccured, false);
    }

    @Test
    public void test_setPayment_getPayment_ValidPayment()   {
        boolean exceptionOccured = false;
        try {
            Reservation r = new Reservation();
            Payment p1 = new Payment();
            r.setPayment(p1);
            assertEquals("Probelm occured while checking getting valid payment !!! ",true, p1.equals(r.getPayment()));
        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Exception occured while checking seting and getting valid payment !!! ",exceptionOccured, false);
    }

    @Test
    public void test_setPayment_NullPaymentObject_forException()   {
        boolean exceptionOccured = false;
        try {
            Reservation r = new Reservation();
            r.setPayment(null);
        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Exception occured while checking seting Null payment !!! ",exceptionOccured, false);
    }

    @Test
    public void test_setPayment_getPayment_NullPaymentObject_ForException()   {
        boolean exceptionOccured = false;
        try {
            Reservation r = new Reservation();
            r.setPayment(null);
            assertEquals("Exception occured while checking seting and getting Null payment !!! ",true, r.getPayment() == null);
        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Exception occured while checking seting and getting  Null payment !!! ",exceptionOccured, false);
    }




    @Test
    public void test_setCustomerName_getCustomerName_forValidName()  {
     boolean exceptionOccured = false;
        try {
            Reservation r = new Reservation();
            r.setCustomerName("Suraj");
            assertEquals("Problem occured while setting and getting valid custome name !!!", true, "Suraj".equals(r.getCustomerName()));
        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Exception occured while setting and getting valid custome name !!!", true, exceptionOccured == false);
    }

    @Test
    public void test_setCustomerName_getCustomerName_forEmptyName()  {
        boolean exceptionOccured = false;
        try {
            Reservation r = new Reservation();
            r.setCustomerName("");
            assertEquals("Problem occured while setting and getting valid custome name !!!", true, "".equals(r.getCustomerName()));
        }catch(Exception e){
            exceptionOccured = true;
     assertEquals("Exception occured while setting and getting valid custome name !!!", true, exceptionOccured == false);
    }
    }

    @Test
    public void test_setCustomerName_forNull_forexception () {
        boolean exceptionOccured = false;
        try {
            Reservation r = new Reservation();
            r.setCustomerName(null);
        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Exception occured while setting NULL custome name !!!", true, exceptionOccured == false);
    }

    @Test
    public void test_setCustomerName_getCustomerName_forNull_forexception () {
        boolean exceptionOccured = false;
        try {
            Reservation r = new Reservation();
            r.setCustomerName(null);
            assertEquals("Problem occured while setting and getting Null custome name !!!", true, r.getCustomerName() == null);
        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Exception occured while setting NULL custome name !!!", true, exceptionOccured == false);
    }
    //====

    @Test
    public void test_setVehileId_getVehicleId_forValidId()  {
        boolean exceptionOccured = false;
        try {
            Reservation r = new Reservation();
            r.setVehicleID("Suraj");
            assertEquals("Problem occured while setting and getting valid vehile ID  !!!", true, "Suraj".equals(r.getVehicleID()));
        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Exception occured while setting valid vehile ID   !!!", true, exceptionOccured == false);
    }

    @Test
    public void test_setVehileId_getVehicleId_forEmptyName()  {
        boolean exceptionOccured = false;
        try {
            Reservation r = new Reservation();
            r.setVehicleID("");
            assertEquals("Problem occured while setting and getting empty vehicle ID !!!", true, "".equals(r.getVehicleID()));
        }catch(Exception e){
            exceptionOccured = true;
            assertEquals("Exception occured while setting and getting empty vehicle ID !!!", true, exceptionOccured == false);
        }
    }

    @Test
    public void test_setVehicleId_forNull_forexception () {
        boolean exceptionOccured = false;
        try {
            Reservation r = new Reservation();
            r.setVehicleID(null);
        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Exception occured while setting NULL vehicle ID !!!", true, exceptionOccured == false);
    }

    @Test
    public void test_setVehileId_getVehicleId_forNull_forexception () {
        boolean exceptionOccured = false;
        try {
            Reservation r = new Reservation();
            r.setVehicleID(null);
            assertEquals("Problem occured while setting and getting Null custome name !!!", true, r.getVehicleID() == null);
        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Exception occured while setting and getting NULL vehicle ID !!!", true, exceptionOccured == false);
    }

    @Test
    public void test_toString_ForNull()  {
     boolean exceptionOccured = false;
        try {
            Reservation r = new Reservation();
            assertNotNull("Null in toString when created object with default constroctor !!!" , r.toString());
        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Execption occured in toString() when created object with default constroctor !!!", exceptionOccured, false);
    }

    @Test
    public void test_toString_ForObjectWithEmptyString()  {
        boolean exceptionOccured = false;
        try {
            Reservation r = new Reservation("","","","");
            assertNotNull("Null in toString when created object with default constroctor !!!" , r.toString());
        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Execption occured in toString() when created object with parametraized constroctor !!!", exceptionOccured, false);
    }

    @Test
    public void test_toString_ForObjectWithValidParameters()  {
        boolean exceptionOccured = false;
        try {
            Reservation r = new Reservation(sdFormat.format(new Date()),sdFormat.format(new Date()),"Suraj","Hyundai");
            r.setIsRedeemed(false);
            r.setPayment(new Payment());
            r.setIsPaid(true);
            assertNotNull("Null in toString when created object with default constroctor !!!" , r.toString());
        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Execption occured in toString when created object with default constroctor !!!", exceptionOccured, false);
    }


    @Test
    public void test_setIsPaid_getIsPaid__forTrue()  {
        boolean exceptionOccured = false;
        try {
            Reservation r = new Reservation();
            r.setIsRedeemed(true);
            assertEquals("Probelm occured while checking set and get isReddemed for True !!! ",true,r.getIsRedeemed());
        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Exception occured while checking set and get isReddemed for True !!! ",exceptionOccured, false);
    }

    @Test
    public void test_setIsPaid_getIsPaid__forFalse()  {
        boolean exceptionOccured = false;
        try {
            Reservation r = new Reservation();
            r.setIsPaid(false);
            assertEquals("Problem occured while checking set and get isPaid for False !!! ",false,r.getIsPaid());
        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Exception occured while checking set and get isPaid for False !!! ",exceptionOccured, false);
    }

    @Test
    public void test_setIsPaid__forNull_forException()  {
        boolean exceptionOccured = false;
        try {
            Reservation r = new Reservation();
            r.setIsPaid(null);
        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Exception occured while checking set isPaid for Null !!! ",exceptionOccured, false);
    }

    @Test
    public void test_setIsPaid_getIsPaid_forNull_forException()  {
        boolean exceptionOccured = false;
        try {
            Reservation r = new Reservation();
            r.setIsPaid(null);
            assertEquals("Problem occured while checking set and get isPaid for Null !!! ",null,r.getIsPaid());
        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Exception occured while checking set and get isPaid for Null !!! ",exceptionOccured, false);
    }

    @Test
    public void test_compareTo_forDefaultContructorCreatedObject()  {
        boolean exceptionOccured = false;
        try {
            Reservation r1 = new Reservation();
            Reservation r2 = new Reservation();

            assertEquals("Problem occured while checking compareTo for default constructor created object !!! ",true,r1.compareTo(r2));
        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Exception occured while checking compareTo for default constructor created object !!! !!! ",exceptionOccured, false);
    }

    @Test
    public void test_compareTo_forParametraizedContructorCreatedObject()  {
        boolean exceptionOccured = false;
        try {
            Reservation r1 = new Reservation("","","","");
            Reservation r2 = new Reservation("","","","");

            assertEquals("Problem occured while checking compareTo for parametarized constructor created object !!! ",true,r1.compareTo(r2));
        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Exception occured while checking compareTo for parametarized constructor created object !!! !!! ",exceptionOccured, false);
    }

    @Test
    public void test_compareTo_forValidObject_ForTrue()  {
        boolean exceptionOccured = false;
        try {

            Date d = new Date();
            Reservation r1 = new Reservation(sdFormat.format(d),sdFormat.format(d),"Suraj","Hyundai");
            Reservation r2 = new Reservation(sdFormat.format(d),sdFormat.format(d),"Suraj","Hyundai");

            assertEquals("Problem occured while checking compareTo for parametarized constructor created object !!! ",0,r1.compareTo(r2));
        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Exception occured while checking compareTo for parametarized constructor created object !!! !!! ",exceptionOccured, false);
    }

    @Test
    public void test_compareTo_forValidObject_ForFalse()  {
        boolean exceptionOccured = false;
        try {

            Date d = new Date();
            Reservation r1 = new Reservation(sdFormat.format(d),sdFormat.format(d),"Suraj1","Hyundai");
            Reservation r2 = new Reservation(sdFormat.format(d),sdFormat.format(d),"Suraj","Hyundai");

            assertNotEquals("Problem occured while checking compareTo for parametarized constructor created object !!! ",0,r1.compareTo(r2));
        }catch(Exception e){
            exceptionOccured = true;
        }
        assertEquals("Exception occured while checking compareTo for parametarized constructor created object !!! !!! ",exceptionOccured, false);
    }
}