package edu.cmu.tartan.test;

import edu.cmu.tartan.hardware.TartanGarageConnection;
import edu.cmu.tartan.hardware.TartanGarageManager;
import edu.cmu.tartan.hardware.TartanSensors;
import edu.cmu.tartan.service.TartanParams;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import static org.junit.Assert.*;

public class TartanGarageManagerTest implements Observer {
	public final static int TARTAN_GARAGE_SPOT_CAPACITY		= 4;
	private TartanGarageManager mTartanGarageManager			= null;

	@Override
	public void update(Observable obs, Object obj) {
	}

	@Test
	public void test_getCapacity() throws Exception {
		System.out.println("TartanGarageManagerTest::test_getCapacity Start");
		MockServerThread mock_server_thread = new MockServerThread(5050);
		mock_server_thread.start();
		mTartanGarageManager = new TartanGarageManager(TartanGarageConnection.getConnection(TartanGarageConnectionTest.LOOP_BACK_ADDR));
		if (mTartanGarageManager == null) {
			Assert.assertTrue(false);
		}
		mTartanGarageManager.addObserver(this); // for notifications
		//
		if (mTartanGarageManager.getCapacity() != 4) {
			Assert.assertTrue(false);
		}
		//
		mTartanGarageManager.disconnectFromGarage();
		mock_server_thread.threadStop();
		mock_server_thread.join();
		System.out.println("TartanGarageManagerTest::test_getCapacity End");
	}

	@Test
	public void test_getParkingSpots() throws Exception {
		System.out.println("TartanGarageManagerTest::test_getParkingSpots Start");
		MockServerThread mock_server_thread = new MockServerThread(5050);
		mock_server_thread.start();
		mTartanGarageManager = new TartanGarageManager(TartanGarageConnection.getConnection(TartanGarageConnectionTest.LOOP_BACK_ADDR));
		if (mTartanGarageManager == null) {
			Assert.assertTrue(false);
		}
		mTartanGarageManager.addObserver(this); // for notifications
		//
		if (mTartanGarageManager.getParkingSpots().size() != 4) {
			Assert.assertTrue(false);
		}
		//
		mTartanGarageManager.disconnectFromGarage();
		mock_server_thread.threadStop();
		mock_server_thread.join();
		System.out.println("TartanGarageManagerTest::test_getParkingSpots End");
	}

	@Test
	public void test_closeExitGate() throws Exception {
		System.out.println("TartanGarageManagerTest::test_closeExitGate Start");
		MockServerThread mock_server_thread = new MockServerThread(5050);
		mock_server_thread.start();
		mTartanGarageManager = new TartanGarageManager(TartanGarageConnection.getConnection(TartanGarageConnectionTest.LOOP_BACK_ADDR));
		if (mTartanGarageManager == null) {
			Assert.assertTrue(false);
		}
		mTartanGarageManager.addObserver(this); // for notifications
		//
		mock_server_thread.setMockerServerFsmState(MockServerThread.MOCK_SERVER_STATE_GET_OK_WITHOUT_COMMA);
		mTartanGarageManager.closeExitGate();
		//
		mTartanGarageManager.disconnectFromGarage();
		mock_server_thread.threadStop();
		mock_server_thread.join();
		System.out.println("TartanGarageManagerTest::test_closeExitGate End");
	}

	@Test
	public void test_openExitGate() throws Exception {
		System.out.println("TartanGarageManagerTest::test_openExitGate Start");
		MockServerThread mock_server_thread = new MockServerThread(5050);
		mock_server_thread.start();
		mTartanGarageManager = new TartanGarageManager(TartanGarageConnection.getConnection(TartanGarageConnectionTest.LOOP_BACK_ADDR));
		if (mTartanGarageManager == null) {
			Assert.assertTrue(false);
		}
		mTartanGarageManager.addObserver(this); // for notifications
		//
		mock_server_thread.setMockerServerFsmState(MockServerThread.MOCK_SERVER_STATE_GET_OK_WITHOUT_COMMA);
		mTartanGarageManager.openExitGate();
		//
		mTartanGarageManager.disconnectFromGarage();
		mock_server_thread.threadStop();
		mock_server_thread.join();
		System.out.println("TartanGarageManagerTest::test_openExitGate End");
	}

	@Test
	public void test_closeEntryGate() throws Exception {
		System.out.println("TartanGarageManagerTest::test_closeEntryGate Start");
		MockServerThread mock_server_thread = new MockServerThread(5050);
		mock_server_thread.start();
		mTartanGarageManager = new TartanGarageManager(TartanGarageConnection.getConnection(TartanGarageConnectionTest.LOOP_BACK_ADDR));
		if (mTartanGarageManager == null) {
			Assert.assertTrue(false);
		}
		mTartanGarageManager.addObserver(this); // for notifications
		//
		mock_server_thread.setMockerServerFsmState(MockServerThread.MOCK_SERVER_STATE_GET_OK_WITHOUT_COMMA);
		mTartanGarageManager.closeEntryGate();
		//
		mTartanGarageManager.disconnectFromGarage();
		mock_server_thread.threadStop();
		mock_server_thread.join();
		System.out.println("TartanGarageManagerTest::test_closeEntryGate End");
	}

	@Test
	public void test_openEntryGate() throws Exception {
		System.out.println("TartanGarageManagerTest::test_openEntryGate Start");
		MockServerThread mock_server_thread = new MockServerThread(5050);
		mock_server_thread.start();
		mTartanGarageManager = new TartanGarageManager(TartanGarageConnection.getConnection(TartanGarageConnectionTest.LOOP_BACK_ADDR));
		if (mTartanGarageManager == null) {
			Assert.assertTrue(false);
		}
		mTartanGarageManager.addObserver(this); // for notifications
		//
		mock_server_thread.setMockerServerFsmState(MockServerThread.MOCK_SERVER_STATE_GET_OK_WITHOUT_COMMA);
		mTartanGarageManager.openEntryGate();
		//
		mTartanGarageManager.disconnectFromGarage();
		mock_server_thread.threadStop();
		mock_server_thread.join();
		System.out.println("TartanGarageManagerTest::test_openEntryGate End");
	}

	@Test
	public void test_toggleExitGateTrue() throws Exception {
		System.out.println("TartanGarageManagerTest::test_toggleExitGateTrue Start");
		MockServerThread mock_server_thread = new MockServerThread(5050);
		mock_server_thread.start();
		mTartanGarageManager = new TartanGarageManager(TartanGarageConnection.getConnection(TartanGarageConnectionTest.LOOP_BACK_ADDR));
		if (mTartanGarageManager == null) {
			Assert.assertTrue(false);
		}
		mTartanGarageManager.addObserver(this); // for notifications
		//
		mock_server_thread.setMockerServerFsmState(MockServerThread.MOCK_SERVER_STATE_GET_OK_WITHOUT_COMMA);
		if (mTartanGarageManager.toggleExitGate(true) != true) {
			Assert.assertTrue(false);
		}
		//
		mTartanGarageManager.disconnectFromGarage();
		mock_server_thread.threadStop();
		mock_server_thread.join();
		System.out.println("TartanGarageManagerTest::test_toggleExitGateTrue End");
	}

	@Test
	public void test_toggleExitGateFalse() throws Exception {
		System.out.println("TartanGarageManagerTest::test_toggleExitGateFalse Start");
		MockServerThread mock_server_thread = new MockServerThread(5050);
		mock_server_thread.start();
		mTartanGarageManager = new TartanGarageManager(TartanGarageConnection.getConnection(TartanGarageConnectionTest.LOOP_BACK_ADDR));
		if (mTartanGarageManager == null) {
			Assert.assertTrue(false);
		}
		mTartanGarageManager.addObserver(this); // for notifications
		//
		mock_server_thread.setMockerServerFsmState(MockServerThread.MOCK_SERVER_STATE_GET_OK_WITHOUT_COMMA);
		if (mTartanGarageManager.toggleExitGate(false) != true) {
			Assert.assertTrue(false);
		}
		//
		mTartanGarageManager.disconnectFromGarage();
		mock_server_thread.threadStop();
		mock_server_thread.join();
		System.out.println("TartanGarageManagerTest::test_toggleExitGateFalse End");
	}

	@Test
	public void test_setParkingSpotLights() throws Exception {
		System.out.println("TartanGarageManagerTest::test_setParkingSpotLights Start");
		MockServerThread mock_server_thread = new MockServerThread(5050);
		mock_server_thread.start();
		mTartanGarageManager = new TartanGarageManager(TartanGarageConnection.getConnection(TartanGarageConnectionTest.LOOP_BACK_ADDR));
		if (mTartanGarageManager == null) {
			Assert.assertTrue(false);
		}
		mTartanGarageManager.addObserver(this); // for notifications
		//
		ArrayList<String> state	= new ArrayList<String>();
		state.add("1");state.add("1");state.add("1");state.add("1");
		mock_server_thread.setMockerServerFsmState(MockServerThread.MOCK_SERVER_STATE_GET_OK_WITHOUT_COMMA);
		if (mTartanGarageManager.setParkingSpotLights(state) != true) {
			Assert.assertTrue(false);
		}
		//
		mTartanGarageManager.disconnectFromGarage();
		mock_server_thread.threadStop();
		mock_server_thread.join();
		System.out.println("TartanGarageManagerTest::test_setParkingSpotLights End");
	}

	@Test
	public void test_setExitLightRed() throws Exception {
		System.out.println("TartanGarageManagerTest::test_setExitLightRed Start");
		MockServerThread mock_server_thread = new MockServerThread(5050);
		mock_server_thread.start();
		mTartanGarageManager = new TartanGarageManager(TartanGarageConnection.getConnection(TartanGarageConnectionTest.LOOP_BACK_ADDR));
		if (mTartanGarageManager == null) {
			Assert.assertTrue(false);
		}
		mTartanGarageManager.addObserver(this); // for notifications
		//
		mock_server_thread.setMockerServerFsmState(MockServerThread.MOCK_SERVER_STATE_GET_OK_WITHOUT_COMMA);
		if (mTartanGarageManager.setExitLight("R") != true) {
			Assert.assertTrue(false);
		}
		//
		mTartanGarageManager.disconnectFromGarage();
		mock_server_thread.threadStop();
		mock_server_thread.join();
		System.out.println("TartanGarageManagerTest::test_setExitLightRed End");
	}

	@Test
	public void test_setExitLightGreen() throws Exception {
		System.out.println("TartanGarageManagerTest::test_setExitLightGreen Start");
		MockServerThread mock_server_thread = new MockServerThread(5050);
		mock_server_thread.start();
		mTartanGarageManager = new TartanGarageManager(TartanGarageConnection.getConnection(TartanGarageConnectionTest.LOOP_BACK_ADDR));
		if (mTartanGarageManager == null) {
			Assert.assertTrue(false);
		}
		mTartanGarageManager.addObserver(this); // for notifications
		//
		mock_server_thread.setMockerServerFsmState(MockServerThread.MOCK_SERVER_STATE_GET_OK_WITHOUT_COMMA);
		if (mTartanGarageManager.setExitLight("G") != true) {
			Assert.assertTrue(false);
		}
		//
		mTartanGarageManager.disconnectFromGarage();
		mock_server_thread.threadStop();
		mock_server_thread.join();
		System.out.println("TartanGarageManagerTest::test_setExitLightGreen End");
	}

	@Test
	public void test_setEntryLightRed() throws Exception {
		System.out.println("TartanGarageManagerTest::test_setEntryLightRed Start");
		MockServerThread mock_server_thread = new MockServerThread(5050);
		mock_server_thread.start();
		mTartanGarageManager = new TartanGarageManager(TartanGarageConnection.getConnection(TartanGarageConnectionTest.LOOP_BACK_ADDR));
		if (mTartanGarageManager == null) {
			Assert.assertTrue(false);
		}
		//
		mock_server_thread.setMockerServerFsmState(MockServerThread.MOCK_SERVER_STATE_GET_OK_WITHOUT_COMMA);
		if (mTartanGarageManager.setEntryLight("R") != true) {
			Assert.assertTrue(false);
		}
		mTartanGarageManager.addObserver(this); // for notifications
		//
		mTartanGarageManager.disconnectFromGarage();
		mock_server_thread.threadStop();
		mock_server_thread.join();
		System.out.println("TartanGarageManagerTest::test_setEntryLightRed End");
	}

	@Test
	public void test_setEntryLightGreen() throws Exception {
		System.out.println("TartanGarageManagerTest::test_setEntryLightGreen Start");
		MockServerThread mock_server_thread = new MockServerThread(5050);
		mock_server_thread.start();
		mTartanGarageManager = new TartanGarageManager(TartanGarageConnection.getConnection(TartanGarageConnectionTest.LOOP_BACK_ADDR));
		if (mTartanGarageManager == null) {
			Assert.assertTrue(false);
		}
		mTartanGarageManager.addObserver(this); // for notifications
		//
		mock_server_thread.setMockerServerFsmState(MockServerThread.MOCK_SERVER_STATE_GET_OK_WITHOUT_COMMA);
		if (mTartanGarageManager.setEntryLight("G") != true) {
			Assert.assertTrue(false);
		}
		//
		mTartanGarageManager.disconnectFromGarage();
		mock_server_thread.threadStop();
		mock_server_thread.join();
		System.out.println("TartanGarageManagerTest::test_setEntryLightGreen End");
	}

	@Test
	public void test_isConnected() throws Exception {
		System.out.println("TartanGarageManagerTest::test_isConnected Start");
		MockServerThread mock_server_thread = new MockServerThread(5050);
		mock_server_thread.start();
		mTartanGarageManager = new TartanGarageManager(TartanGarageConnection.getConnection(TartanGarageConnectionTest.LOOP_BACK_ADDR));
		if (mTartanGarageManager == null) {
			Assert.assertTrue(false);
		}
		mTartanGarageManager.addObserver(this); // for notifications
		//
		if (mTartanGarageManager.isConnected() != true) {
			Assert.assertTrue(false);
		}
		//
		mTartanGarageManager.disconnectFromGarage();
		mock_server_thread.threadStop();
		mock_server_thread.join();
		System.out.println("TartanGarageManagerTest::test_isConnected End");
	}

	@Test
	public void test_updateGarageState00() throws Exception {
		System.out.println("TartanGarageManagerTest::test_updateGarageState00 Start");
		MockServerThread mock_server_thread = new MockServerThread(5050);
		mock_server_thread.start();
		mTartanGarageManager = new TartanGarageManager(TartanGarageConnection.getConnection(TartanGarageConnectionTest.LOOP_BACK_ADDR));
		if (mTartanGarageManager == null) {
			Assert.assertTrue(false);
		}
		mTartanGarageManager.addObserver(this); // for notifications
		//
		mock_server_thread.setMockerServerFsmState(MockServerThread.MOCK_SERVER_STATE_GET_RSP_COVERAGE_00);
		mTartanGarageManager.updateGarageState();
		//
		mTartanGarageManager.disconnectFromGarage();
		mock_server_thread.threadStop();
		mock_server_thread.join();
		System.out.println("TartanGarageManagerTest::test_updateGarageState00 End");
	}

	@Test
	public void test_updateGarageState01() throws Exception {
		System.out.println("TartanGarageManagerTest::test_updateGarageState01 Start");
		MockServerThread mock_server_thread = new MockServerThread(5050);
		mock_server_thread.start();
		mTartanGarageManager = new TartanGarageManager(TartanGarageConnection.getConnection(TartanGarageConnectionTest.LOOP_BACK_ADDR));
		if (mTartanGarageManager == null) {
			Assert.assertTrue(false);
		}
		mTartanGarageManager.addObserver(this); // for notifications
		//
		mock_server_thread.setMockerServerFsmState(MockServerThread.MOCK_SERVER_STATE_GET_RSP_COVERAGE_01);
		mTartanGarageManager.updateGarageState();
		//
		mTartanGarageManager.disconnectFromGarage();
		mock_server_thread.threadStop();
		mock_server_thread.join();
		System.out.println("TartanGarageManagerTest::test_updateGarageState01 End");
	}

	@Test
	public void test_updateGarageState02() throws Exception {
		System.out.println("TartanGarageManagerTest::test_updateGarageState02 Start");
		MockServerThread mock_server_thread = new MockServerThread(5050);
		mock_server_thread.start();
		mTartanGarageManager = new TartanGarageManager(TartanGarageConnection.getConnection(TartanGarageConnectionTest.LOOP_BACK_ADDR));
		if (mTartanGarageManager == null) {
			Assert.assertTrue(false);
		}
		mTartanGarageManager.addObserver(this); // for notifications
		//
		mock_server_thread.setMockerServerFsmState(MockServerThread.MOCK_SERVER_STATE_GET_RSP_COVERAGE_02);
		mTartanGarageManager.updateGarageState();
		//
		mTartanGarageManager.disconnectFromGarage();
		mock_server_thread.threadStop();
		mock_server_thread.join();
		System.out.println("TartanGarageManagerTest::test_updateGarageState02 End");
	}

	@Test
	public void test_getSpotOccupiedState() throws Exception {
		System.out.println("TartanGarageManagerTest::test_getSpotOccupiedState Start");
		MockServerThread mock_server_thread = new MockServerThread(5050);
		mock_server_thread.start();
		mTartanGarageManager = new TartanGarageManager(TartanGarageConnection.getConnection(TartanGarageConnectionTest.LOOP_BACK_ADDR));
		if (mTartanGarageManager == null) {
			Assert.assertTrue(false);
		}
		mTartanGarageManager.addObserver(this); // for notifications
		//
		mTartanGarageManager.getSpotOccupiedState();
		//
		mTartanGarageManager.disconnectFromGarage();
		mock_server_thread.threadStop();
		mock_server_thread.join();
		System.out.println("TartanGarageManagerTest::test_getSpotOccupiedState End");
	}

	@Test
	public void test_startUpdateThread() throws Exception {
		System.out.println("TartanGarageManagerTest::test_startUpdateThread Start");
		MockServerThread mock_server_thread = new MockServerThread(5050);
		mock_server_thread.start();
		mTartanGarageManager = new TartanGarageManager(TartanGarageConnection.getConnection(TartanGarageConnectionTest.LOOP_BACK_ADDR));
		if (mTartanGarageManager == null) {
			Assert.assertTrue(false);
		}
		mTartanGarageManager.addObserver(this); // for notifications
		//
		mTartanGarageManager.setEntryLight(TartanSensors.RED);
		mTartanGarageManager.setExitLight(TartanSensors.RED);
		mTartanGarageManager.closeEntryGate();
		mTartanGarageManager.closeExitGate();

		ArrayList<String> lightState = new ArrayList<String>();
		for (int i=0; i < mTartanGarageManager.getCapacity(); i++) {
			lightState.add(TartanSensors.OFF);
		}
		mTartanGarageManager.setParkingSpotLights(lightState);
		mTartanGarageManager.startUpdateThread();
		//
		Thread.sleep(10000);
		mTartanGarageManager.disconnectFromGarage();
		mock_server_thread.threadStop();
		mock_server_thread.join();
		System.out.println("TartanGarageManagerTest::test_startUpdateThread End");
	}

}