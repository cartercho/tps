package edu.cmu.tartan.test;

import edu.cmu.tartan.hardware.TartanGarageConnection;
import edu.cmu.tartan.hardware.TartanSensors;
import org.junit.Assert;
import org.junit.Test;

public class TartanGarageConnectionTest {
	public final static String LOOP_BACK_ADDR					= "127.0.0.1";
	private final static String RECONNECT_TEST_ADDR			= "127.0.0.2";
	private TartanGarageConnection mTartanGarageConnection		= null;

	@Test
	public void test_normalConnection() throws Exception {
		System.out.println("TartanGarageConnectionTest::test_normalConnection Start");
		MockServerThread mock_server_thread = new MockServerThread(5050);
		mock_server_thread.start();
		mTartanGarageConnection = TartanGarageConnection.getConnection(LOOP_BACK_ADDR);
		if (mTartanGarageConnection == null) {
			Assert.assertTrue(false);
		}
		if (mTartanGarageConnection != TartanGarageConnection.getConnection(null)) {
			Assert.assertTrue(false);
		}
		mock_server_thread.threadStop();
		mock_server_thread.join();
		System.out.println("TartanGarageConnectionTest::test_normalConnection End");
	}

	@Test
	public void test_reConnection() throws Exception {
		System.out.println("TartanGarageConnectionTest::test_reConnection Start");
		MockServerThread mock_server_thread = new MockServerThread(5050);
		mock_server_thread.start();
		mTartanGarageConnection = TartanGarageConnection.getConnection(LOOP_BACK_ADDR);
		mTartanGarageConnection = TartanGarageConnection.getConnection(RECONNECT_TEST_ADDR);
		if (mTartanGarageConnection == null) {
			Assert.assertTrue(false);
		}
		if (mTartanGarageConnection != TartanGarageConnection.getConnection(null)) {
			Assert.assertTrue(false);
		}
		mock_server_thread.threadStop();
		mock_server_thread.join();
		System.out.println("TartanGarageConnectionTest::test_reConnection End");
	}

	@Test
	public void test_invalidConnection() throws Exception {
		System.out.println("TartanGarageConnectionTest::test_invalidConnection Start");
		MockServerThread mock_server_thread = new MockServerThread(5050);
		mock_server_thread.start();
		mTartanGarageConnection = TartanGarageConnection.getConnection(LOOP_BACK_ADDR);
		mTartanGarageConnection.disconnect();
		mTartanGarageConnection = TartanGarageConnection.getConnection(null);
		if (mTartanGarageConnection != null) {
			Assert.assertTrue(false);
		}
		mock_server_thread.threadStop();
		mock_server_thread.join();
		System.out.println("TartanGarageConnectionTest::test_invalidConnection End");
	}

	@Test
	public void test_getAddress() throws Exception {
		System.out.println("TartanGarageConnectionTest::test_getAddress Start");
		MockServerThread mock_server_thread = new MockServerThread(5050);
		mock_server_thread.start();
		mTartanGarageConnection = TartanGarageConnection.getConnection(LOOP_BACK_ADDR);
		if (mTartanGarageConnection == null) {
			Assert.assertTrue(false);
		}
		if (mTartanGarageConnection != TartanGarageConnection.getConnection(null)) {
			Assert.assertTrue(false);
		}
		if (mTartanGarageConnection == null ||
				mTartanGarageConnection.getAddress().compareTo(LOOP_BACK_ADDR) != 0) {
			Assert.assertTrue(false);
		}
		mock_server_thread.threadStop();
		mock_server_thread.join();
		System.out.println("TartanGarageConnectionTest::test_getAddress End");
	}

	@Test
	public void test_isConnected() throws Exception {
		System.out.println("TartanGarageConnectionTest::test_isConnected Start");
		MockServerThread mock_server_thread = new MockServerThread(5050);
		mock_server_thread.start();
		mTartanGarageConnection = TartanGarageConnection.getConnection(LOOP_BACK_ADDR);
		if (mTartanGarageConnection == null) {
			Assert.assertTrue(false);
		}
		if (mTartanGarageConnection != TartanGarageConnection.getConnection(null)) {
			Assert.assertTrue(false);
		}
		if (mTartanGarageConnection.isConnected() == false) {
			Assert.assertTrue(false);
		}
		mock_server_thread.threadStop();
		mock_server_thread.join();
		System.out.println("TartanGarageConnectionTest::test_isConnected End");
	}

	@Test
	public void test_sendMessageToGarage() throws Exception {
		System.out.println("TartanGarageConnectionTest::test_sendMessageToGarage Start");
		MockServerThread mock_server_thread = new MockServerThread(5050);
		mock_server_thread.start();
		mTartanGarageConnection = TartanGarageConnection.getConnection(LOOP_BACK_ADDR);
		if (mTartanGarageConnection == null) {
			Assert.assertTrue(false);
		}
		if (mTartanGarageConnection != TartanGarageConnection.getConnection(null)) {
			Assert.assertTrue(false);
		}
		String update = mTartanGarageConnection.sendMessageToGarage(TartanSensors.GET_STATE + TartanSensors.MSG_END);
		mock_server_thread.threadStop();
		mock_server_thread.join();
		System.out.println("TartanGarageConnectionTest::test_sendMessageToGarage End");
	}

	@Test
	public void test_disconnect() throws Exception {
		System.out.println("TartanGarageConnectionTest::test_disconnect Start");
		MockServerThread mock_server_thread = new MockServerThread(5050);
		mock_server_thread.start();
		mTartanGarageConnection = TartanGarageConnection.getConnection(LOOP_BACK_ADDR);
		if (mTartanGarageConnection == null) {
			Assert.assertTrue(false);
		}
		if (mTartanGarageConnection != TartanGarageConnection.getConnection(null)) {
			Assert.assertTrue(false);
		}
		mTartanGarageConnection.disconnect();
		if (mTartanGarageConnection.isConnected() == true) {
			Assert.assertTrue(false);
		}
		mock_server_thread.threadStop();
		mock_server_thread.join();
		System.out.println("TartanGarageConnectionTest::test_disconnect End");
	}

}