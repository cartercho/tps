package edu.cmu.tartan.test;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;

public class MockServerThread extends Thread {
	public final static int		MOCK_SERVER_STATE_GET_OK_WITH_COMMA						= 0;
	public final static int		MOCK_SERVER_STATE_GET_OK_WITHOUT_COMMA					= 1;
	public final static int		MOCK_SERVER_STATE_GET_RSP_COVERAGE_00						= 2;
	public final static int		MOCK_SERVER_STATE_GET_RSP_COVERAGE_01						= 3;;
	public final static int		MOCK_SERVER_STATE_GET_RSP_COVERAGE_02						= 4;
	private boolean mbThreadRun;
	private int miState;
	private ServerSocket mServerSocket;

	public MockServerThread(int port) throws IOException {
		mbThreadRun	= true;
		miState		= MOCK_SERVER_STATE_GET_OK_WITH_COMMA;
		mServerSocket = new ServerSocket(port);
	}

	public void threadStop() throws IOException {
		mbThreadRun	= false;
		mServerSocket.close();
	}

	public void setMockerServerFsmState(int state) {
		miState		= state;
	}

	public int getMockerServerFsmState() {
		return miState;
	}

	public void run() {
		while (mbThreadRun) {
			try {
				Socket server = mServerSocket.accept();
				DataInputStream in = new DataInputStream(server.getInputStream());
				DataOutputStream out = new DataOutputStream(server.getOutputStream());

				if (in != null) {
					System.out.println("MockServerThread - in()");
					String  strMsgToSnd = "OK.";
					switch (getMockerServerFsmState()) {
						case MOCK_SERVER_STATE_GET_RSP_COVERAGE_00:
							System.out.println("MOCK_SERVER_STATE_GET_RSP_COVERAGE_00");
							strMsgToSnd = "SU:NG=0;XG=0;NL=R;NIR=0;XIR=0;XL=R;PL=[1=0,2=0,3=0,4=0];PO=[1=0,2=0,3=0,4=0].";
							miState	= MOCK_SERVER_STATE_GET_OK_WITH_COMMA;
							break;
						case MOCK_SERVER_STATE_GET_RSP_COVERAGE_01:
							strMsgToSnd = "SU:NG=1;XG=1;NL=G;NIR=1;XIR=1;XL=G;PL=[1=1,2=1,3=1,4=1];PO=[1=1,2=1,3=1,4=1].";
							miState	= MOCK_SERVER_STATE_GET_OK_WITH_COMMA;
							break;
						case MOCK_SERVER_STATE_GET_RSP_COVERAGE_02:
							strMsgToSnd = "SU:NG=1;XG=1;NL=0;NIR=1;XIR=1;XL=0;PL=[1=1,2=1,3=1,4=1];PO=[1=1,2=1,3=1,4=1].";
							miState	= MOCK_SERVER_STATE_GET_OK_WITH_COMMA;
							break;
						case MOCK_SERVER_STATE_GET_OK_WITHOUT_COMMA:
							strMsgToSnd = "OK";
							miState	= MOCK_SERVER_STATE_GET_OK_WITH_COMMA;
							break;
						case MOCK_SERVER_STATE_GET_OK_WITH_COMMA:
						default:
							break;
					}
					out.writeBytes(strMsgToSnd);
					out.flush();
				}
				server.close();
			} catch(SocketTimeoutException s) {
				System.out.println("Socket timed out!");
				break;
			} catch(IOException e) {
				e.printStackTrace();
				break;
			}
		}
	}
}
