package edu.cmu.tartan.test;

import edu.cmu.tartan.edu.cmu.tartan.reservation.Payment;
import edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation;
import edu.cmu.tartan.service.PaymentService;
import edu.cmu.tartan.service.TartanParams;
import org.junit.After;
import org.junit.Before;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by hyojoon.moon on 8/2/2017.
 */

@RunWith(PowerMockRunner.class)
@PrepareForTest(PaymentService.class)
public class PaymentServiceTest {

    private SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy:MM:dd:HH:mm");

    @Before
    public void setUp() throws Exception {
        System.setProperty("org.apache.activemq.SERIALIZABLE_PACKAGES","*");
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void CheckExceptionWhenCreatePaymentServiceObjectTest() {
        boolean exceptionOccured = false;

        try {
            PaymentService paymentService = new PaymentService();
            assertNotNull("CheckExceptionWhenCreatePaymentServiceObjectTest Fails : PaymentService object create error", paymentService);
        } catch (Exception e) {
            exceptionOccured = true;
        }

        assertEquals("CheckExceptionWhenCreatePaymentServiceObjectTest Fails : Exception occured", false,exceptionOccured);
    }

    @Test
    public void PaymentServiceRunTest() {
        boolean exceptionOccured = false;

        try {
            PaymentService paymentService = new PaymentService();
            paymentService.run();
        } catch (Exception e) {
            exceptionOccured = true;
        }

        assertEquals("PaymentServiceRunTest Fails : Exception occured", false,exceptionOccured);
    }

    @Test
    public void PaymentServiceTerminateTest() {
        boolean exceptionOccured = false;

        try {
            PaymentService paymentService = new PaymentService();
            paymentService.run();
            try {
                Thread.sleep(1000);
            } catch (Exception e) {}
            paymentService.terminate();
        } catch (Exception e) {
            exceptionOccured = true;
        }
        assertEquals("PaymentServiceTerminateTest Fails : Exception occured", false,exceptionOccured);
    }

    @Test
    public void handleMessage_MSG_VALIDATE_PAYMENT_WithValidPayment_Test() {
        PaymentService paymentService = new PaymentService();
        boolean exceptionOccured = false;

        try {
            Payment payment = new Payment();
            payment.setCcNum("1234");
            payment.setCcName("hyojoon");
            Date date = new Date();
            payment.setCcExpDate(sdFormat.format(date));

            HashMap<String, Object> body = new HashMap<String, Object>();
            body.put(TartanParams.COMMAND, TartanParams.MSG_VALIDATE_PAYMENT);

            body.put(TartanParams.PAYLOAD, payment);

            paymentService.handleMessage(body);
        }catch (Exception e) {
            exceptionOccured = true;
            e.printStackTrace();
        }
        assertEquals("handleMessage_MSG_VALIDATE_PAYMENT_WithValidPayment_Test Fails : Exception occured", false,exceptionOccured);
    }

    @Test
    public void handleMessage_MSG_VALIDATE_PAYMENT_WithInvalidPayment_Test() {
        PaymentService paymentService = new PaymentService();
        boolean exceptionOccured = false;

        try {
            Payment payment = new Payment();
            Date date = new Date();
            payment.setCcExpDate(sdFormat.format(date));

            HashMap<String, Object> body = new HashMap<String, Object>();
            body.put(TartanParams.COMMAND, TartanParams.MSG_VALIDATE_PAYMENT);

            body.put(TartanParams.PAYLOAD, payment);

            paymentService.handleMessage(body);
        }catch (Exception e) {
            exceptionOccured = true;
            e.printStackTrace();
        }
        assertEquals("handleMessage_MSG_VALIDATE_PAYMENT_WithInvalidPayment_Test Fails : Exception occured", false,exceptionOccured);
    }

    @Test
    public void handleMessage_MSG_VALIDATE_PAYMENT_WithNullPayment_Test() {
        PaymentService paymentService = new PaymentService();
        boolean exceptionOccured = false;

        try {
            Payment payment = null;

            HashMap<String, Object> body = new HashMap<String, Object>();
            body.put(TartanParams.COMMAND, TartanParams.MSG_VALIDATE_PAYMENT);

            body.put(TartanParams.PAYLOAD, payment);

            paymentService.handleMessage(body);
        }catch (Exception e) {
            exceptionOccured = true;
            e.printStackTrace();
        }
        assertEquals("handleMessage_MSG_VALIDATE_PAYMENT_WithInvalidPayment_Test Fails : Exception occured", false,exceptionOccured);
    }

    @Test
    public void handleMessage_MSG_MAKE_PAYMENT_WithWeekdayDayTimeReservation_Test() {
        PaymentService paymentService = new PaymentService();
        boolean exceptionOccured = false;

        try {
            Reservation rsv = new Reservation();
            rsv.setCustomerName("hyojoon");
            rsv.setVehicleID("lge1234");

            String sd1 = "2017:08:01:11:00";
            Date d1 = sdFormat.parse(sd1);
            rsv.setStartTime(d1);
            String sd2 = "2017:08:01:14:30";
            Date d2 = sdFormat.parse(sd2);
            rsv.setEndTime(d2);

            HashMap<String, Object> body = new HashMap<String, Object>();
            body.put(TartanParams.COMMAND, TartanParams.MSG_MAKE_PAYMENT);

            body.put(TartanParams.PAYLOAD, rsv);

            String strFakeDate = "2017:08:01:10:58";
            Date fakeDate = sdFormat.parse(strFakeDate);
            Date spyDate = PowerMockito.spy(new Date());
            PowerMockito.whenNew(Date.class).withAnyArguments().thenReturn(fakeDate);

            paymentService.handleMessage(body);
        } catch (Exception e) {
            exceptionOccured = true;
            e.printStackTrace();
        }
        assertEquals("handleMessage_MSG_MAKE_PAYMENT_WithWeekdayDayTimeReservation_Test Fails : Exception occured", false,exceptionOccured);
    }

    @Test
    public void handleMessage_MSG_MAKE_PAYMENT_WithWeekdayNightTimeReservation_Test() {
        PaymentService paymentService = new PaymentService();
        boolean exceptionOccured = false;

        try {
            Reservation rsv = new Reservation();
            rsv.setCustomerName("hyojoon");
            rsv.setVehicleID("lge1234");

            String sd1 = "2017:08:01:18:00";
            Date d1 = sdFormat.parse(sd1);
            rsv.setStartTime(d1);
            String sd2 = "2017:08:01:20:30";
            Date d2 = sdFormat.parse(sd2);
            rsv.setEndTime(d2);

            HashMap<String, Object> body = new HashMap<String, Object>();
            body.put(TartanParams.COMMAND, TartanParams.MSG_MAKE_PAYMENT);

            body.put(TartanParams.PAYLOAD, rsv);

            String strFakeDate = "2017:08:01:18:05";
            Date fakeDate = sdFormat.parse(strFakeDate);
            Date spyDate = PowerMockito.spy(new Date());
            PowerMockito.whenNew(Date.class).withAnyArguments().thenReturn(fakeDate);

            paymentService.handleMessage(body);
        } catch (Exception e) {
            exceptionOccured = true;
            e.printStackTrace();
        }
        assertEquals("handleMessage_MSG_MAKE_PAYMENT_WithWeekdayNightTimeReservation_Test Fails : Exception occured", false,exceptionOccured);
    }

    @Test
    public void handleMessage_MSG_MAKE_PAYMENT_WithWeekendDayTimeReservation_Test() {
        PaymentService paymentService = new PaymentService();
        boolean exceptionOccured = false;

        try {
            Reservation rsv = new Reservation();
            rsv.setCustomerName("hyojoon");
            rsv.setVehicleID("lge1234");

            String sd1 = "2017:08:05:10:00";
            Date d1 = sdFormat.parse(sd1);
            rsv.setStartTime(d1);
            String sd2 = "2017:08:05:14:30";
            Date d2 = sdFormat.parse(sd2);
            rsv.setEndTime(d2);

            HashMap<String, Object> body = new HashMap<String, Object>();
            body.put(TartanParams.COMMAND, TartanParams.MSG_MAKE_PAYMENT);

            body.put(TartanParams.PAYLOAD, rsv);

            String strFakeDate = "2017:08:05:09:55";
            Date fakeDate = sdFormat.parse(strFakeDate);
            Date spyDate = PowerMockito.spy(new Date());
            PowerMockito.whenNew(Date.class).withAnyArguments().thenReturn(fakeDate);

            paymentService.handleMessage(body);
        } catch (Exception e) {
            exceptionOccured = true;
            e.printStackTrace();
        }
        assertEquals("handleMessage_MSG_MAKE_PAYMENT_WithWeekendDayTimeReservation_Test Fails : Exception occured", false,exceptionOccured);
    }

    @Test
    public void handleMessage_MSG_MAKE_PAYMENT_WithWeekendNightTimeReservation_Test() {
        PaymentService paymentService = new PaymentService();
        boolean exceptionOccured = false;

        try {
            Reservation rsv = new Reservation();
            rsv.setCustomerName("hyojoon");
            rsv.setVehicleID("lge1234");

            String sd1 = "2017:08:05:19:00";
            Date d1 = sdFormat.parse(sd1);
            rsv.setStartTime(d1);
            String sd2 = "2017:08:05:22:30";
            Date d2 = sdFormat.parse(sd2);
            rsv.setEndTime(d2);

            HashMap<String, Object> body = new HashMap<String, Object>();
            body.put(TartanParams.COMMAND, TartanParams.MSG_MAKE_PAYMENT);

            body.put(TartanParams.PAYLOAD, rsv);

            String strFakeDate = "2017:08:05:19:20";
            Date fakeDate = sdFormat.parse(strFakeDate);
            Date spyDate = PowerMockito.spy(new Date());
            PowerMockito.whenNew(Date.class).withAnyArguments().thenReturn(fakeDate);

            paymentService.handleMessage(body);
        } catch (Exception e) {
            exceptionOccured = true;
            e.printStackTrace();
        }
        assertEquals("handleMessage_MSG_MAKE_PAYMENT_WithWeekendNightTimeReservation_Test Fails : Exception occured", false,exceptionOccured);
    }

    @Test
    public void handleMessage_MSG_MAKE_PAYMENT_WithPenaltyDurationReservation_Test() {
        PaymentService paymentService = new PaymentService();
        boolean exceptionOccured = false;

        try {
            Reservation rsv = new Reservation();
            rsv.setCustomerName("hyojoon");
            rsv.setVehicleID("lge1234");

            String sd1 = "2017:08:05:19:00";
            Date d1 = sdFormat.parse(sd1);
            rsv.setStartTime(d1);
            String sd2 = "2017:08:05:22:30";
            Date d2 = sdFormat.parse(sd2);
            rsv.setEndTime(d2);

            HashMap<String, Object> body = new HashMap<String, Object>();
            body.put(TartanParams.COMMAND, TartanParams.MSG_MAKE_PAYMENT);

            body.put(TartanParams.PAYLOAD, rsv);

            String strFakeDate = "2017:08:05:23:50";
            Date fakeDate = sdFormat.parse(strFakeDate);
            Date spyDate = PowerMockito.spy(new Date());
            PowerMockito.whenNew(Date.class).withAnyArguments().thenReturn(fakeDate);

            paymentService.handleMessage(body);
        } catch (Exception e) {
            exceptionOccured = true;
            e.printStackTrace();
        }
        assertEquals("handleMessage_MSG_MAKE_PAYMENT_WithPenaltyDurationReservation_Test Fails : Exception occured", false,exceptionOccured);
    }

    @Test
    public void handleMessage_MSG_MAKE_PAYMENT_WithNullReservation_Test() {
        PaymentService paymentService = new PaymentService();
        boolean exceptionOccured = false;

        try {
            Reservation rsv = null;

            HashMap<String, Object> body = new HashMap<String, Object>();
            body.put(TartanParams.COMMAND, TartanParams.MSG_MAKE_PAYMENT);

            body.put(TartanParams.PAYLOAD, rsv);

            String strFakeDate = "2017:08:05:23:50";
            Date fakeDate = sdFormat.parse(strFakeDate);
            Date spyDate = PowerMockito.spy(new Date());
            PowerMockito.whenNew(Date.class).withAnyArguments().thenReturn(fakeDate);

            paymentService.handleMessage(body);
        } catch (Exception e) {
            exceptionOccured = true;
            e.printStackTrace();
        }
        assertEquals("handleMessage_MSG_MAKE_PAYMENT_WithNullReservation_Test Fails : Exception occured", false,exceptionOccured);
    }
}