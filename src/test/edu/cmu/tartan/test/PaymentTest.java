package edu.cmu.tartan.test;

import edu.cmu.tartan.edu.cmu.tartan.reservation.Payment;
import edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by sy84.lee on 7/26/2017.
 */
public class PaymentTest {
    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void paymentTestConstructorWithNoParameter() throws Exception {
        Payment payment = new Payment();
        Assert.assertNotNull(payment);
    }

    @Test
    public void paymentTestConstructorWithNagativeParameterValue() throws Exception {
        long amt = -10;
        Payment payment = new Payment(amt);
        Assert.assertNotNull(payment);
    }

    @Test
    public void paymentTestConstructorWithZeroParameterValue() throws Exception {
        long amt = 0;
        Payment payment = new Payment(amt);
        Assert.assertNotNull(payment);
    }

    @Test
    public void paymentTestConstructorWithPositiveParameterValue() throws Exception {
        long amt = 10;
        Payment payment = new Payment(amt);
        Assert.assertNotNull(payment);
    }

    @Test
    public void setReservationTestWithNormalParameterValue() throws Exception {
        Reservation r = new Reservation();
        Payment payment = new Payment();

        payment.setReservation(r);
    }

    @Test
    public void setReservationTestWithNullParameterValue() throws Exception {
        Payment payment = new Payment();

        payment.setReservation(null);
    }

    @Test
    public void getReservationTestWithNoReservation() throws Exception {
        Payment payment = new Payment();
        Reservation r = payment.getReservation();

        Assert.assertNotNull(r);
    }

    @Test
    public void getReservationTestWithReservation() throws Exception {
        Reservation r1 = new Reservation();
        Payment payment = new Payment();
        payment.setReservation(r1);

        Reservation r2 = payment.getReservation();

        Assert.assertNotNull(r2);
    }

    @Test
    public void setFeeTestWithNegativeParameterValue() throws Exception {
        long amt = -10;
        Payment payment = new Payment();

        payment.setFee(amt);
    }

    @Test
    public void setFeeTestWithZeorParameterValue() throws Exception {
        long amt = 0;
        Payment payment = new Payment();

        payment.setFee(amt);
    }

    @Test
    public void setFeeTestWithPositiveParameterValue() throws Exception {
        long amt = 10;
        Payment payment = new Payment();

        payment.setFee(amt);
    }

    @Test
    public void getFeeTestWIthNoSet() throws Exception {
        Payment payment = new Payment();
        try {
            long fee = payment.getFee();
            Assert.assertEquals(0, fee);
        } catch (Exception e) {
            assert (false);
        }
    }

    @Test
    public void getFeeTestWIthNegativeSet() throws Exception {
        long setFee = -10;
        Payment payment = new Payment();
        payment.setFee(setFee);

        long fee = payment.getFee();

        Assert.assertEquals(0, fee);
    }

    @Test
    public void getFeeTestWIthZeorSet() throws Exception {
        long setFee = 0;
        Payment payment = new Payment();
        payment.setFee(setFee);

        long fee = payment.getFee();

        Assert.assertEquals(0, fee);
    }

    @Test
    public void getFeeTestWIthPositiveSet() throws Exception {
        long setFee = 10;
        Payment payment = new Payment();
        payment.setFee(setFee);

        long fee = payment.getFee();

        Assert.assertEquals(setFee, fee);
    }

    @Test
    public void isValidTestWithNotSet() throws Exception {
        Payment payment = new Payment();
        Assert.assertFalse(payment.isValid());
    }

    @Test
    public void isValidTestWithOnlyCcNumSet() throws Exception {
        Payment payment = new Payment();
        payment.setCcNum("110");

        Assert.assertFalse(payment.isValid());
    }

    @Test
    public void isValidTestWithOnlyCcExpDateSet() throws Exception {
        Payment payment = new Payment();
        payment.setCcExpDate("2018-09-10");

        Assert.assertFalse(payment.isValid());
    }

    @Test
    public void isValidTestWithOnlyCcNameSet() throws Exception {
        Payment payment = new Payment();
        payment.setCcName("Lee");

        Assert.assertFalse(payment.isValid());
    }

    @Test
    public void isValidTestWithCcNumAndCcExpDateAndCcNameSet() throws Exception {
        Payment payment = new Payment();
        payment.setCcNum("110");
        payment.setCcExpDate("2018-09-10");
        payment.setCcName("Lee");

        Assert.assertTrue(payment.isValid());
    }

    @Test
    public void getCcNumTestWithNotSet() throws Exception {
        Payment payment = new Payment();
        try {
            Assert.assertNotNull(payment.getCcNum());
        } catch (Exception e) {
            assert(false);
        }
    }

    @Test
    public void getCcNumTestWithSet() throws Exception {
        Payment payment = new Payment();

        payment.setCcNum("1111");

        Assert.assertNotNull(payment.getCcNum());
    }

    @Test
    public void setCcNumTestWithNullParameter() throws Exception {
        Payment payment = new Payment();
        try {
            payment.setCcNum(null);
        } catch (Exception e) {
            assert(false);
        }
    }

    @Test
    public void setCcNumTestWithParameter() throws Exception {
        Payment payment = new Payment();

        payment.setCcNum("1111");
    }

    @Test
    public void getCcExpDateWithNotSet() throws Exception {
        Payment payment = new Payment();
        try {
            Assert.assertNotNull(payment.getCcExpDate());
        }catch (Exception e) {
            assert(false);
        }
    }

    @Test
    public void getCcExpDateWithSet() throws Exception {
        Payment payment = new Payment();

        payment.setCcExpDate("2018-09-10");

        Assert.assertNotNull(payment.getCcExpDate());
    }

    @Test
    public void setCcExpDateTestWithNullParameter() throws Exception {
        Payment payment = new Payment();
        try {
            payment.setCcExpDate(null);
        } catch (Exception e) {
            assert(false);
        }
    }

    @Test
    public void setCcExpDateTestWithParameter() throws Exception {
        Payment payment = new Payment();

        payment.setCcExpDate("2018-09-10");
    }

    @Test
    public void getCcNameWithNotSet() throws Exception {
        Payment payment = new Payment();
        try {
            Assert.assertNotNull(payment.getCcName());
        }catch (Exception e) {
            assert(false);
        }
    }

    @Test
    public void getCcNameWithSet() throws Exception {
        Payment payment = new Payment();

        payment.setCcName("Lee");

        Assert.assertNotNull(payment.getCcName());
    }

    @Test
    public void setCcNameTestWithNullParameter() throws Exception {
        Payment payment = new Payment();
        try {
            payment.setCcName(null);
        } catch (Exception e) {
            assert(false);
        }
    }

    @Test
    public void setCcNameTestWithParameter() throws Exception {
        Payment payment = new Payment();

        payment.setCcName("Lee");
    }

}