package edu.cmu.tartan.test;

import com.sun.org.apache.regexp.internal.RE;
import edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation;
import edu.cmu.tartan.edu.cmu.tartan.reservation.ReservationStore;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import static org.junit.Assert.*;

/**
 * Created by hyojoon.moon on 7/28/2017.
 */
public class ReservationStoreTest {
    private SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy:MM:dd:HH:mm");
    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void createObjectWithNullFilePathTest() {
        boolean exceptionOccured = false;

        try {
            ReservationStore rs = new ReservationStore("");
            assertNotNull("Fail to create object with null path in constructor", rs);
        } catch (Exception e) {
            exceptionOccured = true;
        }
        assertEquals("createObjectWithNullFilePathTest Fails : Exception occurred while creating ReservationStore object with null path in constructor !!!",false, exceptionOccured);
    }

    @Test
    public void createObjectWithNormalFilePathTest() {
        boolean exceptionOccured = false;

        try {
            ReservationStore rs = new ReservationStore("C:\\Temp\\");
            assertNotNull("Fail to create object with normal path in constructor", rs);
        } catch (Exception e) {
            exceptionOccured = true;
        }
        assertEquals("createObjectWithNormalFilePathTest Fails : Exception occurred while creating ReservationStore object with normal path in constructor !!!",false, exceptionOccured);
    }

    @Test
    public void addReservationWithDefaultReservationTest() {
        boolean result = false;
        boolean exceptionOccured = false;
        try {
            ReservationStore rs = new ReservationStore("C:\\Temp\\");

            Reservation r = new Reservation();

            result = rs.addReservation(r);
            assertTrue("Method call fail : ReservationStore.addResrevation() ", result);
        } catch(Exception e) {
            exceptionOccured = true;
        }
        assertEquals("addReservationWithDefaultReservationTest Fails : Exception occured while calling ReservationStore.addReservation()", false, exceptionOccured);
    }

//    @Test
//    public void addReservationWithEmptyReservationTest() {
//        boolean result = false;
//        boolean exceptionOccured = false;
//        try {
//            ReservationStore rs = new ReservationStore("test.txt");
//
//            Reservation r = new Reservation("", "", "", "");
//
//            result = rs.addReservation(r);
//            assertTrue("Method call fail : ReservationStore.addResrevation() ", result);
//        } catch(Exception e) {
//            exceptionOccured = true;
//        }
//        assertEquals("Exception occured while calling ReservationStore.addReservation()", false, exceptionOccured);
//    }

    @Test
    public void getReservationsWhenReservationIsNullTest() {
        boolean exceptionOccured = false;
        Vector<Reservation> reservations = new Vector<Reservation>();

        try {
            ReservationStore rs = new ReservationStore("C:\\Temp\\");

            reservations = rs.getReservations();
        } catch (Exception e) {
            exceptionOccured = true;
        }

        assertEquals("getReservationsWhenReservationIsNullTest Fails : Exception occured while calling ReservationStore.getReservations()", false, exceptionOccured);
    }

    @Test
    public void getReservationsWhenReservationExistOnlyOneTest() {
        boolean exceptionOccured = false;

        Vector<Reservation> reservations = new Vector<Reservation>();

        try {
            ReservationStore rs = new ReservationStore("C:\\Temp\\");

            Reservation r1 = new Reservation();
            rs.addReservation(r1);

            reservations = rs.getReservations();

            assertEquals("getReservationsWhenReservationExistOnlyOneTest Fails : ReservationStore.getReservations() returns different value", true, r1.equals(reservations.elementAt(0)));
        } catch (Exception e) {
            exceptionOccured = true;
        }

        assertEquals("getReservationsWhenReservationExistOnlyOneTest Fails : Exception occured while calling ReservationStore.getReservations()", false, exceptionOccured);
    }

    @Test
    public void getReservationsWhenReservationExistMoreThanOneTest() {
        boolean exceptionOccured = false;

        Vector<Reservation> reservations = new Vector<Reservation>();

        try {
            ReservationStore rs = new ReservationStore("C:\\Temp\\");

            Vector<Reservation> rsvs = new Vector<Reservation>();
            Reservation r1 = new Reservation();
            rs.addReservation(r1);
            rsvs.add(r1);

            Reservation r2 = new Reservation();
            r2.setCustomerName("hyojoon");
            r2.setVehicleID("lge");
            rs.addReservation(r2);
            rsvs.add(r2);

            reservations = rs.getReservations();

            for(int i = 0; i < reservations.size(); i++)  {
                assertEquals("getReservationsWhenReservationExistMoreThanOneTest Fails : ReservationStore.getReservations() returns different value", true, rsvs.elementAt(i).equals(reservations.elementAt(i)));
            }
        } catch (Exception e) {
            exceptionOccured = true;
        }

        assertEquals("getReservationsWhenReservationExistMoreThanOneTest Fails : Exception occured while calling ReservationStore.getReservations()", false, exceptionOccured);
    }

    @Test
    public void lookupByCustomerTest() {
        boolean exceptionOccured = false;

        try {
            ReservationStore rs = new ReservationStore("C:\\Temp\\");
            Vector<Reservation> rsvs = new Vector<Reservation>();

            Reservation r= new Reservation();
            r.setCustomerName("hyojoon");
            r.setVehicleID("lge1234");

            rs.addReservation(r);

            rsvs = rs.lookupByCustomer("hyojoon");
            assertEquals("lookupByCustomerTest Fails : ReservationStore.lookupByCustomer() returns different value", "hyojoon", rsvs.elementAt(0).getCustomerName());
        } catch (Exception e) {
            exceptionOccured = true;
        }

        assertEquals("lookupByCustomerTest Fails : Exception occured while calling ReservationStore.lookupByCustomer()", false, exceptionOccured);
    }

    @Test
    public void lookupByCustomerIsEmptyTest() {
        boolean exceptionOccured = false;

        try {
            ReservationStore rs = new ReservationStore("C:\\Temp\\");
            Vector<Reservation> rsvs = new Vector<Reservation>();

            Reservation r= new Reservation();
            r.setCustomerName("");
            r.setVehicleID("lge1234");

            rs.addReservation(r);

            rsvs = rs.lookupByCustomer("");
            assertEquals("lookupByCustomerIsEmptyTest Fails : ReservationStore.lookupByCustomer() returns different value", "", rsvs.elementAt(0).getCustomerName());
        } catch (Exception e) {
            exceptionOccured = true;
        }

        assertEquals("lookupByCustomerIsEmptyTest Fails : Exception occured while calling ReservationStore.lookupByCustomer()", false, exceptionOccured);
    }

    @Test
    public void lookupByCustomerIsNullTest() {
        boolean exceptionOccured = false;

        try {
            ReservationStore rs = new ReservationStore("C:\\Temp\\");
            Vector<Reservation> rsvs = new Vector<Reservation>();

            Reservation r= new Reservation();
            r.setVehicleID("lge1234");

            rs.addReservation(r);

            rsvs = rs.lookupByCustomer(null);
            assertEquals("lookupByCustomerIsNullTest Fails : ReservationStore.lookupByCustomer() returns different value", null, rsvs.elementAt(0).getCustomerName());
        } catch (Exception e) {
            exceptionOccured = true;
        }

        assertEquals("lookupByCustomerIsNullTest Fails : Exception occured while calling ReservationStore.lookupByCustomer()", false, exceptionOccured);
    }

    @Test
    public void lookupByVehicleTest() {
        boolean exceptionOccured = false;

        try {
            ReservationStore rs = new ReservationStore("C:\\Temp\\");
            Vector<Reservation> rsvs = new Vector<Reservation>();

            Reservation r= new Reservation();
            r.setCustomerName("hyojoon");
            r.setVehicleID("lge1234");

            rs.addReservation(r);

            rsvs = rs.lookupByVehicle("lge1234");
            assertEquals("lookupByVehicleTest Fails : ReservationStore.lookupByVehicle() returns different value", "lge1234", rsvs.elementAt(0).getVehicleID());
        } catch (Exception e) {
            exceptionOccured = true;
        }

        assertEquals("lookupByVehicleTest Fails : Exception occured while calling ReservationStore.lookupByVehicle()", false, exceptionOccured);
    }

    @Test
    public void lookupByVehicleIsEmptyTest() {
        boolean exceptionOccured = false;

        try {
            ReservationStore rs = new ReservationStore("C:\\Temp\\");
            Vector<Reservation> rsvs = new Vector<Reservation>();

            Reservation r= new Reservation();
            r.setCustomerName("hyojoon");
            r.setVehicleID("");

            rs.addReservation(r);

            rsvs = rs.lookupByVehicle("");
            assertEquals("lookupByVehicleIsEmptyTest Fails : ReservationStore.lookupByVehicle() returns different value", "", rsvs.elementAt(0).getVehicleID());
        } catch (Exception e) {
            exceptionOccured = true;
        }

        assertEquals("lookupByVehicleIsEmptyTest Fails : Exception occured while calling ReservationStore.lookupByVehicle()", false, exceptionOccured);
    }

    @Test
    public void lookupByVehicleIsNullTest() {
        boolean exceptionOccured = false;

        try {
            ReservationStore rs = new ReservationStore("C:\\Temp\\");
            Vector<Reservation> rsvs = new Vector<Reservation>();

            Reservation r= new Reservation();
            r.setCustomerName("hyojoon");

            rs.addReservation(r);

            rsvs = rs.lookupByVehicle(null);
            assertEquals("lookupByVehicleIsNullTest Fails : ReservationStore.lookupByVehicle() returns different value", null, rsvs.elementAt(0).getVehicleID());
        } catch (Exception e) {
            exceptionOccured = true;
        }

        assertEquals("lookupByVehicleIsNullTest Fails : Exception occured while calling ReservationStore.lookupByVehicle()", false, exceptionOccured);
    }

    @Test
    public void shutdownWithNoReservationTest() {
        boolean exceptionOccured = false;

        try {
            ReservationStore rs = new ReservationStore("C:\\Temp\\");

            rs.shutdown();
        } catch (Exception e) {
            exceptionOccured = true;
        }

        assertEquals("shutdownWithNoReservationTest Fails : Exception occured while calling ReservationStore.shutdown()", false, exceptionOccured);
    }

    @Test
    public void shutdownWithOneReservationTest() {
        boolean exceptionOccured = false;

        try {
            ReservationStore rs = new ReservationStore("C:\\Temp\\");
            Reservation r = new Reservation();
            r.setCustomerName("hyojoon");
            r.setVehicleID("lge1234");
            Date d1 = new Date();
            r.setStartTime(sdFormat.format(d1));
            Date d2 = new Date();
            r.setEndTime(sdFormat.format(d2));
            rs.addReservation(r);

            rs.shutdown();
        } catch (Exception e) {
            exceptionOccured = true;
        }

        assertEquals("shutdownWithOneReservationTest Fails : Exception occured while calling ReservationStore.shutdown()", false, exceptionOccured);
    }

    @Test
    public void shutdownWithMoreThanOneReservationTest() {
        boolean exceptionOccured = false;

        try {
            ReservationStore rs = new ReservationStore("C:\\Temp\\");
            Reservation r1 = new Reservation();
            r1.setCustomerName("hyojoon");
            r1.setVehicleID("lge1234");
            Date d1 = new Date();
            r1.setStartTime(sdFormat.format(d1));
            Date d2 = new Date();
            r1.setEndTime(sdFormat.format(d2));
            rs.addReservation(r1);

            Reservation r2 = new Reservation();
            r2.setCustomerName("hyojoon");
            r2.setVehicleID("lge1234");
            Date d3 = new Date();
            r2.setStartTime(sdFormat.format(d3));
            Date d4 = new Date();
            r2.setEndTime(sdFormat.format(d4));
            rs.addReservation(r2);

            rs.shutdown();
        } catch (Exception e) {
            exceptionOccured = true;
        }

        assertEquals("shutdownWithMoreThanOneReservationTest Fails : Exception occured while calling ReservationStore.shutdown()", false, exceptionOccured);
    }

    @Test
    public void loadReservationsWhenFileIsNotExistTest() {
        boolean exceptionOccured = false;
        File file = new File("C:\\Temp\\rsvp.txt");

        if(file.exists()) {
            System.out.println("file exist.");
            file.deleteOnExit();
        }

        try {
            ReservationStore rs = new ReservationStore("C:\\Temp\\");
            rs.loadReservations();
        } catch (Exception e) {
            exceptionOccured = true;
        }

        assertEquals("loadReservationsWhenFileIsNotExistTest Fails : Exception occured while calling ReservationStore.loadReservations()", false, exceptionOccured);
    }

    @Test
    public void loadReservationsWhenFileIsExistTest() {
        boolean exceptionOccured = false;

        try {
            ReservationStore rs = new ReservationStore("C:\\Temp\\");
            Reservation r1 = new Reservation();
            r1.setCustomerName("hyojoon");
            r1.setVehicleID("lge1234");
            Date d1 = new Date();
            r1.setStartTime(sdFormat.format(d1));
            Date d2 = new Date();
            r1.setEndTime(sdFormat.format(d2));
            rs.addReservation(r1);

            rs.shutdown();

            rs.loadReservations();
        } catch (Exception e) {
            exceptionOccured = true;
        }

        assertEquals("loadReservationsWhenFileIsExistTest Fails : Exception occured while calling ReservationStore.loadReservations()", false, exceptionOccured);
    }

    @Test
    public void isDuplicateWithNoReservationTest() {
        boolean exceptionOccured = false;
        boolean result = false;

        try {
            ReservationStore rs = new ReservationStore("C:\\Temp\\");
            Reservation r1 = new Reservation();
            r1.setCustomerName("hyojoon");
            r1.setVehicleID("lge1234");
            Date d1 = new Date();
            r1.setStartTime(sdFormat.format(d1));
            Date d2 = new Date();
            r1.setEndTime(sdFormat.format(d2));

            result = rs.isDuplicate(r1);
            assertEquals("isDuplicateWithNoReservationTest Fails : isDuplicateWithNoReservationTest()", false, result);
        }catch (Exception e) {
            exceptionOccured = true;
        }
        assertEquals("isDuplicateWithNoReservationTest Fails : Exception occured while calling isDuplicateWithNoReservationTest()", false, exceptionOccured);
    }

    @Test
    public void isDuplicateWithOneReservationTest() {
        boolean exceptionOccured = false;
        boolean result = false;

        try {
            ReservationStore rs = new ReservationStore("C:\\Temp\\");
            Reservation r1 = new Reservation();
            r1.setCustomerName("hyojoon");
            r1.setVehicleID("lge1234");
            Date d1 = new Date();
            r1.setStartTime(sdFormat.format(d1));
            Date d2 = new Date();
            r1.setEndTime(sdFormat.format(d2));
            rs.addReservation(r1);

            result = rs.isDuplicate(r1);
            assertEquals("isDuplicateWithOneReservationTest Fails :  isDuplicateWithMoreThanOneReservationTest()", true, result);
        }catch (Exception e) {
            exceptionOccured = true;
        }
        assertEquals("isDuplicateWithOneReservationTest Fails : Exception occured while calling isDuplicateWithMoreThanOneReservationTest()", false, exceptionOccured);
    }

    @Test
    public void isDuplicateWithMoreThanOneReservationTest() {
        boolean exceptionOccured = false;
        boolean result = false;

        try {
            ReservationStore rs = new ReservationStore("C:\\Temp\\");
            Reservation r1 = new Reservation();
            r1.setCustomerName("hyojoon");
            r1.setVehicleID("lge1234");
            Date d1 = new Date();
            r1.setStartTime(sdFormat.format(d1));
            Date d2 = new Date();
            r1.setEndTime(sdFormat.format(d2));
            rs.addReservation(r1);

            Reservation r2 = new Reservation();
            r2.setCustomerName("hyojoon2");
            r2.setVehicleID("lge12345");
            Date d3 = new Date();
            r2.setStartTime(sdFormat.format(d3));
            Date d4 = new Date();
            r2.setEndTime(sdFormat.format(d4));
            rs.addReservation(r2);

            result = rs.isDuplicate(r1);
            assertEquals("isDuplicateWithMoreThanOneReservationTest Fails : isDuplicateWithMoreThanOneReservationTest()", true, result);
        }catch (Exception e) {
            exceptionOccured = true;
        }
        assertEquals("isDuplicateWithMoreThanOneReservationTest Fails : Exception occured while calling isDuplicateWithMoreThanOneReservationTest()", false, exceptionOccured);
    }

    @Test
    public void isDuplicateWithOneReservationNoMatchTest() {
        boolean exceptionOccured = false;
        boolean result = false;

        try {
            ReservationStore rs = new ReservationStore("C:\\Temp\\");
            Reservation r1 = new Reservation();
            r1.setCustomerName("hyojoon");
            r1.setVehicleID("lge1234");
            Date d1 = new Date();
            r1.setStartTime(sdFormat.format(d1));
            Date d2 = new Date();
            r1.setEndTime(sdFormat.format(d2));
            rs.addReservation(r1);

            Reservation r2 = new Reservation();
            r2.setCustomerName("hyojoon123");
            r2.setVehicleID("lge12344");
            Date d3 = new Date();
            r2.setStartTime(sdFormat.format(d3));
            Date d4 = new Date();
            r2.setEndTime(sdFormat.format(d4));

            result = rs.isDuplicate(r2);
            assertEquals("isDuplicateWithOneReservationNoMatchTest Fails : isDuplicateWithMoreThanOneReservationTest()", false, result);
        }catch (Exception e) {
            exceptionOccured = true;
        }
        assertEquals("isDuplicateWithOneReservationNoMatchTest Fails : Exception occured while calling isDuplicateWithMoreThanOneReservationTest()", false, exceptionOccured);
    }

    @Test
    public void saveNewReservationWithNullReservationTest() {
        boolean exceptionOccured = false;
        boolean result = false;

        try {
            ReservationStore rs = new ReservationStore("C:\\Temp\\");

            Reservation r1 = new Reservation();
            result = rs.addReservation(r1);
            result = rs.saveNewReservation(r1);

            assertEquals("saveNewReservationWithNullReservationTest Fails", false, result);
        } catch (Exception e) {
            exceptionOccured = true;
        }

        assertEquals("saveNewReservationWithNullReservationTest Fails : Exception occured while calling saveNewReservationWithNullReservationTest()", false, exceptionOccured);
    }

    @Test
    public void saveNewReservationWithEmptyReservationTest() {
        boolean exceptionOccured = false;
        boolean result = false;

        try {
            ReservationStore rs = new ReservationStore("C:\\Temp\\");

            Reservation r1 = new Reservation("", "", "", "");
            result = rs.addReservation(r1);
            result = rs.saveNewReservation(r1);

            assertEquals("saveNewReservationWithEmptyReservationTest Fails", false, result);
        } catch (Exception e) {
            exceptionOccured = true;
        }

        assertEquals("saveNewReservationWithEmptyReservationTest Fails : Exception occured while calling saveNewReservationWithEmptyReservationTest()", false, exceptionOccured);
    }

    @Test
    public void saveNewReservationWithNoDuplicatedReservationTest() {
        boolean exceptionOccured = false;
        boolean result = false;

        try {
            ReservationStore rs = new ReservationStore("C:\\Temp\\");

            Reservation r1 = new Reservation();
            r1.setCustomerName("hyojoon");
            r1.setVehicleID("lge1234");
            Date d1 = new Date();
            r1.setStartTime(sdFormat.format(d1));
            Date d2 = new Date();
            r1.setEndTime(sdFormat.format(d2));

            result = rs.saveNewReservation(r1);

            assertEquals("saveNewReservationWithNoDuplicatedReservationTest Fails", false, result);
        } catch (Exception e) {
            exceptionOccured = true;
        }

        assertEquals("saveNewReservationWithNoDuplicatedReservationTest Fails : Exception occured while calling saveNewReservationWithNoDuplicatedReservationTest()", false, exceptionOccured);
    }

    @Test
    public void saveNewReservationWithDuplicatedReservationTest() {
        boolean exceptionOccured = false;
        boolean result = false;

        try {
            ReservationStore rs = new ReservationStore("C:\\Temp\\");

            Reservation r1 = new Reservation();
            r1.setCustomerName("hyojoon");
            r1.setVehicleID("lge1234");
            Date d1 = new Date();
            r1.setStartTime(sdFormat.format(d1));
            Date d2 = new Date();
            r1.setEndTime(sdFormat.format(d2));

            rs.addReservation(r1);
            result = rs.saveNewReservation(r1);

            assertEquals("saveNewReservationWithDuplicatedReservationTest Fails", true, result);
        } catch (Exception e) {
            exceptionOccured = true;
        }

        assertEquals("saveNewReservationWithDuplicatedReservationTest Fails : Exception occured while calling saveNewReservationWithDuplicatedReservationTest()", false, exceptionOccured);
    }

    @Test
    public void markReservationRedeemedWithNoReservation() {
        boolean exceptionOccured = false;

        try {
            ReservationStore rs = new ReservationStore("C:\\Temp\\");

            Reservation r1 = new Reservation();
            r1.setCustomerName("hyojoon");
            r1.setVehicleID("lge1234");
            Date d1 = new Date();
            r1.setStartTime(sdFormat.format(d1));
            Date d2 = new Date();
            r1.setEndTime(sdFormat.format(d2));

            rs.markReservationRedeemed(r1);
        } catch (Exception e) {
            exceptionOccured = true;
        }

        assertEquals("markReservationRedeemedWithNoReservation Fails : Exception occured while calling saveNewReservationWithDuplicatedReservationTest()", false, exceptionOccured);
    }

    @Test
    public void markReservationRedeemedWithNoDuplicatedReservation() {
        boolean exceptionOccured = false;

        try {
            ReservationStore rs = new ReservationStore("C:\\Temp\\");

            Reservation r1 = new Reservation();
            r1.setCustomerName("hyojoon");
            r1.setVehicleID("lge1234");
            Date d1 = new Date();
            r1.setStartTime(sdFormat.format(d1));
            Date d2 = new Date();
            r1.setEndTime(sdFormat.format(d2));

            rs.addReservation(r1);

            Reservation r2 = new Reservation();
            r2.setCustomerName("hyojoon123");
            r2.setVehicleID("lge1234567");
            Date d3 = new Date();
            r2.setStartTime(sdFormat.format(d3));
            Date d4 = new Date();
            r2.setEndTime(sdFormat.format(d4));

            rs.markReservationRedeemed(r2);
        } catch (Exception e) {
            exceptionOccured = true;
        }

        assertEquals("markReservationRedeemedWithNoDuplicatedReservation Fails : Exception occured while calling saveNewReservationWithDuplicatedReservationTest()", false, exceptionOccured);
    }

    @Test
    public void markReservationRedeemedWithDuplicatedReservation() {
        boolean exceptionOccured = false;

        try {
            ReservationStore rs = new ReservationStore("C:\\Temp\\");

            Reservation r1 = new Reservation();
            r1.setCustomerName("hyojoon");
            r1.setVehicleID("lge1234");
            Date d1 = new Date();
            r1.setStartTime(sdFormat.format(d1));
            Date d2 = new Date();
            r1.setEndTime(sdFormat.format(d2));

            rs.addReservation(r1);

            Reservation r2 = new Reservation();
            r2.setCustomerName("hyojoon123");
            r2.setVehicleID("lge1234567");
            Date d3 = new Date();
            r2.setStartTime(sdFormat.format(d3));
            Date d4 = new Date();
            r2.setEndTime(sdFormat.format(d4));

            rs.addReservation(r2);

            rs.markReservationRedeemed(r2);
        } catch (Exception e) {
            exceptionOccured = true;
        }

        assertEquals("markReservationRedeemedWithNoDuplicatedReservation Fails : Exception occured while calling saveNewReservationWithDuplicatedReservationTest()", false, exceptionOccured);
    }
}

