package edu.cmu.tartan;

import edu.cmu.tartan.hardware.TartanGarageConnection;
import edu.cmu.tartan.service.KioskService;
import edu.cmu.tartan.utils.LoggerUtil;
import edu.cmu.tartan.service.TartanParams;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Authenticate the User
 * <p>
 * Project: LG Exec Ed SDET Program
 * Copyright: 2017 Suraj Mal Singh
 * Versions:
 * 1.0 July 2017 - initial version
 */

public class AdminConsoleDialog extends JDialog {


    private JPanel contentPane;
    private JLabel lableTotalRevenue;
    // private JLabel lableFrequentUser;
    private JPanel lableCP;
    private JButton buttonSendVehicleArrivalStatus;


    private JLabel lableAverageWrongParked;
    private JLabel lableAverageInvalidPayments;
    private JLabel lableAverageResUpdates;
    private JLabel lableDeniedReservationsDuplidateReq;
    private JLabel lableAverageOccupancy;
    private JLabel lableDeniedReservationsParkingFull;
    private JLabel lableDeniedReservationsInvalidRequest;
    private JLabel lableDeniedReservationsUnknown;
    private JLabel lablePeakUsageHour;
    private JComboBox comboBoxStartDate;
    private JComboBox comboBoxStartTime;
    private JComboBox comboBoxEndDate;
    private JComboBox comboBoxEndTime;
    private JButton buttonRefresh;
    private JCheckBox checkBoxVehicleArrivalStatus;
    private JCheckBox checkBoxVehicleExitStatus;
    private JButton buttonSendVehicleExitStatus;
    private JButton buttonSendPOStatus;
    private JTextField textFieldPOStatus;
    private JTextField textFieldIP;
    private JButton buttonConnection;
    private JLabel lableConnected;
    private JLabel lablePOMessage;
    private JButton fromButton;
    private JButton toButton;
    TartanGarageConnection conn;


    public AdminConsoleDialog() {


        setContentPane(contentPane);
        //setModal(true);
        setTitle("Administive Console");
        setResizable(false);

        populateDate(comboBoxStartDate);
        populateDate(comboBoxEndDate);
        populateTime(comboBoxStartTime);
        populateTime(comboBoxEndTime);

        // Set the default time range as last 24 hours
        int i = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        comboBoxStartTime.setSelectedIndex(i);

        if (i + 1 > comboBoxEndTime.getMaximumRowCount())
            comboBoxEndTime.setSelectedIndex(0);
        else
            comboBoxEndTime.setSelectedIndex(i + 1);
        
        comboBoxStartDate.setSelectedIndex(1);

        lableConnected.setVisible(false);
        lablePOMessage.setVisible(false);


        Calendar c = Calendar.getInstance();
        Date endTime = new Date(); // CURRENT TIME
        c.setTime(endTime);
        c.add(Calendar.HOUR_OF_DAY, -24);
        Date startTime = c.getTime(); // 24 hour before Time

        initialiseStatics(startTime, endTime);

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        buttonSendVehicleArrivalStatus.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (checkBoxVehicleArrivalStatus.isSelected() == true)
                    conn.sendMessageToGarage("NIR=1.");
                else
                    conn.sendMessageToGarage("NIR=0.");
            }
        });

        buttonSendVehicleArrivalStatus.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });
        buttonSendVehicleExitStatus.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (checkBoxVehicleExitStatus.isSelected() == true)
                    conn.sendMessageToGarage("XIR=1.");
                else
                    conn.sendMessageToGarage("XIR=0.");
            }
        });
        buttonSendPOStatus.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String str = textFieldPOStatus.getText().toString();
                if (str.length() >= 4)
                    conn.sendMessageToGarage("PO=[1=" + str.charAt(0) + ",2=" + str.charAt(1) + ",3=" + str.charAt(2) + ",4=" + str.charAt(3) + "].");

                if (lablePOMessage.isVisible())
                    lablePOMessage.setVisible(false);

                else {
                    lablePOMessage.setForeground(Color.RED);
                    lablePOMessage.setText("Enter 4 digits 0 or 1 !!!");
                    lablePOMessage.setVisible(true);
                }
            }
        });
        buttonConnection.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String IP = null;
                IP = textFieldIP.getText();
                if (IP != null && !IP.isEmpty() && validIP(IP)) {
                    System.out.println("IP : " + IP);
                    conn = TartanGarageConnection.getConnection(IP);
                    if (conn != null) {
                        buttonSendPOStatus.setEnabled(true);
                        buttonSendVehicleArrivalStatus.setEnabled(true);
                        buttonSendVehicleExitStatus.setEnabled(true);
                        textFieldPOStatus.setEnabled(true);
                        checkBoxVehicleExitStatus.setEnabled(true);
                        checkBoxVehicleArrivalStatus.setEnabled(true);
                        buttonConnection.setEnabled(false);
                        textFieldIP.setEnabled(false);

                        lableConnected.setForeground(Color.green);
                        lableConnected.setText("Connected.");
                        lableConnected.setVisible(true);
                    } else {
                        buttonSendPOStatus.setEnabled(false);
                        buttonSendVehicleArrivalStatus.setEnabled(false);
                        buttonSendVehicleExitStatus.setEnabled(false);
                        textFieldPOStatus.setEnabled(false);
                        checkBoxVehicleExitStatus.setEnabled(false);
                        checkBoxVehicleArrivalStatus.setEnabled(false);

                        lableConnected.setForeground(Color.red);
                        lableConnected.setText("Enter correct IP !!!");
                        lableConnected.setVisible(true);

                    }
                } else {
                    lableConnected.setForeground(Color.red);
                    lableConnected.setText("Enter correct IP !!!");
                    lableConnected.setVisible(true);
                }
            }
        });
        buttonRefresh.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SimpleDateFormat parser = new SimpleDateFormat("EEE,MMM dd yyyy hh a");

                String start = String.valueOf(comboBoxStartDate.getSelectedItem()) + " " + String.valueOf(comboBoxStartTime.getSelectedItem());
//                System.out.println(start);
                String end = String.valueOf(comboBoxEndDate.getSelectedItem()) + " " + String.valueOf(comboBoxEndTime.getSelectedItem());
                //              System.out.println(end);

                Date startDate = null;
                Date endDate = null;

                try {
                    startDate = parser.parse(start);
                    endDate = parser.parse(end);
                } catch (ParseException e1) {
                    e1.printStackTrace();
                }

                System.out.println(startDate);
                System.out.println(endDate);

                initialiseStatics(startDate, endDate);

            }
        });
    }

    private void onOK() {
        // add your code here
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }

    public void initialiseStatics(Date startTime, Date endTime) {


        //***************** Initialise Statistics START *******************************************************************

        // RESERVATIONS DENIED
        TreeMap<String, Integer> errorCountMap = LoggerUtil.getErrorTypeCount(startTime, endTime);

        if (errorCountMap != null && errorCountMap.size() > 0) {
            int countDuplicate = 0;
            if (errorCountMap.get(TartanParams.DENIED_REASON_DUPLICATE) == null)
                countDuplicate = 0;
            else
                countDuplicate = errorCountMap.get(TartanParams.DENIED_REASON_DUPLICATE);
            lableDeniedReservationsDuplidateReq.setText("" + countDuplicate);

            int countInvalid = 0;
            if (errorCountMap.get(TartanParams.DENIED_REASON_INVALID) == null)
                countInvalid = 0;
            else
                countInvalid = errorCountMap.get(TartanParams.DENIED_REASON_INVALID);
            lableDeniedReservationsInvalidRequest.setText("" + countInvalid);

            int countFull = 0;
            if (errorCountMap.get(TartanParams.DENIED_REASON_PARKING_FULL) == null)
                countFull = 0;
            else
                countFull = errorCountMap.get(TartanParams.DENIED_REASON_PARKING_FULL);

            lableDeniedReservationsParkingFull.setText("" + countFull);

            int countUnknown = 0;
            if (errorCountMap.get(TartanParams.REASON_UNKNOWN) == null)
                countUnknown = 0;
            else
                countUnknown = errorCountMap.get(TartanParams.REASON_UNKNOWN);

            int countCouldNotComplete = 0;
            if (errorCountMap.get(TartanParams.DENIED_REASON_NOT_COMPLETED) == null)
                countCouldNotComplete = 0;
            else
                countCouldNotComplete = errorCountMap.get(TartanParams.DENIED_REASON_NOT_COMPLETED);

            lableDeniedReservationsUnknown.setText("" + (countUnknown + countCouldNotComplete));
        } else {
            lableDeniedReservationsDuplidateReq.setText("0");
            lableDeniedReservationsInvalidRequest.setText("0");
            lableDeniedReservationsParkingFull.setText("0");
            lableDeniedReservationsUnknown.setText("0");
        }


        //RESERVATION UPDATE REQUESTS
        int noOfReservationUpdates = LoggerUtil.getMessageLogCount(startTime, endTime, TartanParams.MSG_UPDATE_RSVP);
        lableAverageResUpdates.setText(String.valueOf(noOfReservationUpdates));

        //INVALID PAYMENTS
        int noOfInvalidPayments = LoggerUtil.getMessageLogCount(startTime, endTime, TartanParams.MSG_PAYMENT_INVALID);
        lableAverageInvalidPayments.setText(String.valueOf(noOfInvalidPayments));

        //WRONG VEHICLE PARKED
        int noOfWrongParkings = LoggerUtil.getMessageLogCount(startTime, endTime, TartanParams.MSG_WRONG_SPOT);
        lableAverageWrongParked.setText(String.valueOf(noOfWrongParkings));

        //REVENUE
        int revenue = LoggerUtil.getTotalRevenue(startTime, endTime);
        lableTotalRevenue.setText(String.valueOf(revenue));

        float parkingPercentage = 0.0f;
        parkingPercentage = LoggerUtil.getParkingOccupancyPercentage(startTime, endTime);
        lableAverageOccupancy.setText("" + parkingPercentage + "%");
        //FrequentUser
        // lableFrequentUser.setText(LoggerUtil.getFrequentlyVisitingCustomeraAndVehicle(startTime, endTime));


        //***************** Initialise Statistics END *********************************************************************

    }

    /**
     * Set up the date combo box.
     *
     * @param dayCb The date combo box
     */
    private void populateDate(JComboBox dayCb) {
        try {
            Calendar c = Calendar.getInstance();
            for (int i = 0; i <= 31; i++) {
                Date date = c.getTime();
                SimpleDateFormat formatter = new SimpleDateFormat("EEE,MMM dd yyyy");
                dayCb.addItem(formatter.format(date));
                c.add(Calendar.DAY_OF_YEAR, -1);
            }
        } catch (Exception e) {
        }
    }

    /**
     * Set up the time combo box.
     *
     * @param timeCb The time combo box
     */
    private void populateTime(JComboBox timeCb) {

        for (int i = 0; i < 24; i++) {

            String t = String.valueOf((i % 12) == 0 ? 12 : (i % 12));
            t += (i > 11) ? " PM" : " AM";
            timeCb.addItem(t);
        }
    }


    /**
     * @noinspection ALL
     */
    private Font $$$getFont$$$(String fontName, int style, int size, Font currentFont) {
        if (currentFont == null) return null;
        String resultName;
        if (fontName == null) {
            resultName = currentFont.getName();
        } else {
            Font testFont = new Font(fontName, Font.PLAIN, 10);
            if (testFont.canDisplay('a') && testFont.canDisplay('1')) {
                resultName = fontName;
            } else {
                resultName = currentFont.getName();
            }
        }
        return new Font(resultName, style >= 0 ? style : currentFont.getStyle(), size >= 0 ? size : currentFont.getSize());
    }

    public boolean validIP(String ip) {
        try {
            if (ip == null || ip.isEmpty()) {
                return false;
            }

            String[] parts = ip.split("\\.");
            if (parts.length != 4) {
                return false;
            }

            for (String s : parts) {
                int i = Integer.parseInt(s);
                if ((i < 0) || (i > 255)) {
                    return false;
                }
            }
            if (ip.endsWith(".")) {
                return false;
            }

            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }


    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        contentPane = new JPanel();
        contentPane.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(1, 1, new Insets(10, 10, 10, 10), -1, -1));
        contentPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), null));
        lableCP = new JPanel();
        lableCP.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(22, 8, new Insets(0, 0, 0, 0), -1, -1));
        contentPane.add(lableCP, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JLabel label1 = new JLabel();
        Font label1Font = this.$$$getFont$$$(null, Font.BOLD, 14, label1.getFont());
        if (label1Font != null) label1.setFont(label1Font);
        label1.setForeground(new Color(-14605099));
        label1.setText("Manual Actions");
        lableCP.add(label1, new com.intellij.uiDesigner.core.GridConstraints(15, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label2 = new JLabel();
        label2.setText("Reservations denied due to parking full : ");
        lableCP.add(label2, new com.intellij.uiDesigner.core.GridConstraints(11, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label3 = new JLabel();
        label3.setText("Reservations denied due to invalid request :");
        lableCP.add(label3, new com.intellij.uiDesigner.core.GridConstraints(12, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label4 = new JLabel();
        label4.setText("Reservations denied due to unknown reasons :");
        lableCP.add(label4, new com.intellij.uiDesigner.core.GridConstraints(13, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        lableTotalRevenue = new JLabel();
        lableTotalRevenue.setForeground(new Color(-5231459));
        lableTotalRevenue.setText("---");
        lableCP.add(lableTotalRevenue, new com.intellij.uiDesigner.core.GridConstraints(4, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label5 = new JLabel();
        label5.setText("Total Revenue : ");
        lableCP.add(label5, new com.intellij.uiDesigner.core.GridConstraints(4, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        lableAverageWrongParked = new JLabel();
        lableAverageWrongParked.setForeground(new Color(-5231459));
        lableAverageWrongParked.setText("---");
        lableCP.add(lableAverageWrongParked, new com.intellij.uiDesigner.core.GridConstraints(5, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label6 = new JLabel();
        label6.setText("Number of wrong vehicle parked : ");
        lableCP.add(label6, new com.intellij.uiDesigner.core.GridConstraints(5, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        lableAverageInvalidPayments = new JLabel();
        lableAverageInvalidPayments.setForeground(new Color(-5231459));
        lableAverageInvalidPayments.setText("---");
        lableCP.add(lableAverageInvalidPayments, new com.intellij.uiDesigner.core.GridConstraints(6, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label7 = new JLabel();
        label7.setText("Number of invalid payments:");
        lableCP.add(label7, new com.intellij.uiDesigner.core.GridConstraints(6, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        lableAverageResUpdates = new JLabel();
        lableAverageResUpdates.setForeground(new Color(-5231459));
        lableAverageResUpdates.setText("---");
        lableCP.add(lableAverageResUpdates, new com.intellij.uiDesigner.core.GridConstraints(7, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label8 = new JLabel();
        label8.setText("Number of reservation updates : ");
        lableCP.add(label8, new com.intellij.uiDesigner.core.GridConstraints(7, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        lableAverageOccupancy = new JLabel();
        lableAverageOccupancy.setBackground(new Color(-5231459));
        lableAverageOccupancy.setForeground(new Color(-5231459));
        lableAverageOccupancy.setText("---");
        lableCP.add(lableAverageOccupancy, new com.intellij.uiDesigner.core.GridConstraints(8, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label9 = new JLabel();
        label9.setText("Occupancy (%) :  ");
        lableCP.add(label9, new com.intellij.uiDesigner.core.GridConstraints(8, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        lablePeakUsageHour = new JLabel();
        lablePeakUsageHour.setText("---");
        lableCP.add(lablePeakUsageHour, new com.intellij.uiDesigner.core.GridConstraints(9, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label10 = new JLabel();
        label10.setText("Peak Usage Hour :");
        lableCP.add(label10, new com.intellij.uiDesigner.core.GridConstraints(9, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        lableDeniedReservationsDuplidateReq = new JLabel();
        lableDeniedReservationsDuplidateReq.setForeground(new Color(-5231459));
        lableDeniedReservationsDuplidateReq.setText("---");
        lableCP.add(lableDeniedReservationsDuplidateReq, new com.intellij.uiDesigner.core.GridConstraints(10, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label11 = new JLabel();
        label11.setText("Reservations denied due to duplicate request : ");
        lableCP.add(label11, new com.intellij.uiDesigner.core.GridConstraints(10, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        lableDeniedReservationsParkingFull = new JLabel();
        lableDeniedReservationsParkingFull.setText("---");
        lableCP.add(lableDeniedReservationsParkingFull, new com.intellij.uiDesigner.core.GridConstraints(11, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        lableDeniedReservationsInvalidRequest = new JLabel();
        lableDeniedReservationsInvalidRequest.setText("---");
        lableCP.add(lableDeniedReservationsInvalidRequest, new com.intellij.uiDesigner.core.GridConstraints(12, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        lableDeniedReservationsUnknown = new JLabel();
        lableDeniedReservationsUnknown.setText("---");
        lableCP.add(lableDeniedReservationsUnknown, new com.intellij.uiDesigner.core.GridConstraints(13, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label12 = new JLabel();
        label12.setBackground(new Color(-1540551));
        Font label12Font = this.$$$getFont$$$(null, Font.BOLD, 11, label12.getFont());
        if (label12Font != null) label12.setFont(label12Font);
        label12.setForeground(new Color(-1540551));
        label12.setText("----------------------------------------------------------------------------------------------------------------------------------------------------------------------");
        lableCP.add(label12, new com.intellij.uiDesigner.core.GridConstraints(14, 0, 1, 8, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label13 = new JLabel();
        Font label13Font = this.$$$getFont$$$(null, Font.BOLD, 14, label13.getFont());
        if (label13Font != null) label13.setFont(label13Font);
        label13.setForeground(new Color(-14605099));
        label13.setText("Statistics");
        lableCP.add(label13, new com.intellij.uiDesigner.core.GridConstraints(3, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        buttonSendPOStatus = new JButton();
        buttonSendPOStatus.setEnabled(false);
        buttonSendPOStatus.setText("Send");
        lableCP.add(buttonSendPOStatus, new com.intellij.uiDesigner.core.GridConstraints(19, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        textFieldPOStatus = new JTextField();
        textFieldPOStatus.setEnabled(false);
        textFieldPOStatus.setText("0000");
        textFieldPOStatus.setToolTipText("0000");
        lableCP.add(textFieldPOStatus, new com.intellij.uiDesigner.core.GridConstraints(19, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        final JLabel label14 = new JLabel();
        Font label14Font = this.$$$getFont$$$(null, Font.BOLD, 12, label14.getFont());
        if (label14Font != null) label14.setFont(label14Font);
        label14.setText("Start date");
        lableCP.add(label14, new com.intellij.uiDesigner.core.GridConstraints(0, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label15 = new JLabel();
        Font label15Font = this.$$$getFont$$$(null, Font.BOLD, 12, label15.getFont());
        if (label15Font != null) label15.setFont(label15Font);
        label15.setText("Start time");
        lableCP.add(label15, new com.intellij.uiDesigner.core.GridConstraints(0, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label16 = new JLabel();
        Font label16Font = this.$$$getFont$$$(null, Font.BOLD, 12, label16.getFont());
        if (label16Font != null) label16.setFont(label16Font);
        label16.setText("End date");
        lableCP.add(label16, new com.intellij.uiDesigner.core.GridConstraints(0, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label17 = new JLabel();
        Font label17Font = this.$$$getFont$$$(null, Font.BOLD, 12, label17.getFont());
        if (label17Font != null) label17.setFont(label17Font);
        label17.setText("End time");
        lableCP.add(label17, new com.intellij.uiDesigner.core.GridConstraints(0, 3, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        comboBoxStartDate = new JComboBox();
        lableCP.add(comboBoxStartDate, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_EAST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        comboBoxEndTime = new JComboBox();
        lableCP.add(comboBoxEndTime, new com.intellij.uiDesigner.core.GridConstraints(1, 3, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        buttonRefresh = new JButton();
        buttonRefresh.setText("Refresh Statics");
        lableCP.add(buttonRefresh, new com.intellij.uiDesigner.core.GridConstraints(1, 4, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label18 = new JLabel();
        label18.setBackground(new Color(-1540551));
        Font label18Font = this.$$$getFont$$$(null, Font.BOLD, 11, label18.getFont());
        if (label18Font != null) label18.setFont(label18Font);
        label18.setForeground(new Color(-1540551));
        label18.setText("-----------------------------------------------------------------------------------------------------------------------------------------------------------------------");
        lableCP.add(label18, new com.intellij.uiDesigner.core.GridConstraints(2, 0, 1, 8, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label19 = new JLabel();
        label19.setText("Parking Status :");
        lableCP.add(label19, new com.intellij.uiDesigner.core.GridConstraints(18, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        textFieldIP = new JTextField();
        textFieldIP.setText("127.0.0.1");
        textFieldIP.setToolTipText("127.0.0.1");
        lableCP.add(textFieldIP, new com.intellij.uiDesigner.core.GridConstraints(17, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        buttonConnection = new JButton();
        buttonConnection.setText("Connection");
        lableCP.add(buttonConnection, new com.intellij.uiDesigner.core.GridConstraints(17, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label20 = new JLabel();
        label20.setText("Enter IP : ");
        lableCP.add(label20, new com.intellij.uiDesigner.core.GridConstraints(16, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        checkBoxVehicleArrivalStatus = new JCheckBox();
        checkBoxVehicleArrivalStatus.setEnabled(false);
        checkBoxVehicleArrivalStatus.setSelected(true);
        checkBoxVehicleArrivalStatus.setText("Vehicle arrival status");
        lableCP.add(checkBoxVehicleArrivalStatus, new com.intellij.uiDesigner.core.GridConstraints(20, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        buttonSendVehicleArrivalStatus = new JButton();
        buttonSendVehicleArrivalStatus.setEnabled(false);
        buttonSendVehicleArrivalStatus.setText("Send");
        lableCP.add(buttonSendVehicleArrivalStatus, new com.intellij.uiDesigner.core.GridConstraints(20, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        checkBoxVehicleExitStatus = new JCheckBox();
        checkBoxVehicleExitStatus.setEnabled(false);
        checkBoxVehicleExitStatus.setSelected(true);
        checkBoxVehicleExitStatus.setText("Vehicle exit status");
        lableCP.add(checkBoxVehicleExitStatus, new com.intellij.uiDesigner.core.GridConstraints(21, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        buttonSendVehicleExitStatus = new JButton();
        buttonSendVehicleExitStatus.setEnabled(false);
        buttonSendVehicleExitStatus.setText("Send");
        lableCP.add(buttonSendVehicleExitStatus, new com.intellij.uiDesigner.core.GridConstraints(21, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        comboBoxStartTime = new JComboBox();
        lableCP.add(comboBoxStartTime, new com.intellij.uiDesigner.core.GridConstraints(1, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        comboBoxEndDate = new JComboBox();
        lableCP.add(comboBoxEndDate, new com.intellij.uiDesigner.core.GridConstraints(1, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        lableConnected = new JLabel();
        Font lableConnectedFont = this.$$$getFont$$$(null, Font.BOLD, -1, lableConnected.getFont());
        if (lableConnectedFont != null) lableConnected.setFont(lableConnectedFont);
        lableConnected.setForeground(new Color(-15139063));
        lableConnected.setText("Connected");
        lableCP.add(lableConnected, new com.intellij.uiDesigner.core.GridConstraints(17, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        lablePOMessage = new JLabel();
        Font lablePOMessageFont = this.$$$getFont$$$(null, Font.BOLD, -1, lablePOMessage.getFont());
        if (lablePOMessageFont != null) lablePOMessage.setFont(lablePOMessageFont);
        lablePOMessage.setForeground(new Color(-4973786));
        lablePOMessage.setText("Enter 4 characters 0 or 1 ");
        lableCP.add(lablePOMessage, new com.intellij.uiDesigner.core.GridConstraints(19, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return contentPane;
    }
}
