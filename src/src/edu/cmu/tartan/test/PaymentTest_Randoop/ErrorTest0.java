package edu.cmu.tartan.test.PaymentTest_Randoop;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ErrorTest0 {

    public static boolean debug = false;

    @Test
    public void test001() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test001");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) (-1L));
        String str2 = payment1.getCcExpDate();
    }

    @Test
    public void test002() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test002");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcName();
    }

    @Test
    public void test003() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test003");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) (-1L));
        payment1.setCcName("");
        String str4 = payment1.getCcNum();
    }

    @Test
    public void test004() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test004");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        payment1.setCcNum("");
        String str4 = payment1.getCcExpDate();
    }

    @Test
    public void test005() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test005");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        Boolean b2 = payment0.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = payment0.getReservation();
        Long long4 = payment0.getFee();
        String str5 = payment0.getCcName();
    }

    @Test
    public void test006() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test006");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        String str3 = payment1.getCcName();
    }

    @Test
    public void test007() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test007");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        String str4 = payment1.getCcNum();
    }

    @Test
    public void test008() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test008");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        Boolean b2 = payment0.isValid();
        String str3 = payment0.getCcNum();
    }

    @Test
    public void test009() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test009");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment0.getReservation();
        Long long8 = payment0.getFee();
        String str9 = payment0.getCcExpDate();
    }

    @Test
    public void test010() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test010");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        Boolean b2 = payment0.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = payment0.getReservation();
        Long long4 = payment0.getFee();
        String str5 = payment0.getCcNum();
    }

    @Test
    public void test011() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test011");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment0.getReservation();
        String str3 = payment0.getCcName();
    }

    @Test
    public void test012() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test012");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcName("");
        String str7 = payment1.getCcNum();
    }

    @Test
    public void test013() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test013");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        String str2 = payment1.getCcNum();
    }

    @Test
    public void test014() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test014");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        String str4 = payment0.getCcName();
    }

    @Test
    public void test015() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test015");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        Long long2 = payment1.getFee();
        String str3 = payment1.getCcNum();
    }

    @Test
    public void test016() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test016");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        Boolean b7 = payment1.isValid();
        String str8 = payment1.getCcName();
    }

    @Test
    public void test017() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test017");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        String str3 = payment1.getCcExpDate();
    }

    @Test
    public void test018() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test018");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment0.getReservation();
        Long long8 = payment0.getFee();
        String str9 = payment0.getCcName();
    }

    @Test
    public void test019() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test019");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        payment1.setCcNum("hi!");
        payment1.setCcNum("");
        String str8 = payment1.getCcExpDate();
    }

    @Test
    public void test020() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test020");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        String str4 = payment1.getCcName();
    }

    @Test
    public void test021() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test021");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        payment1.setCcNum("hi!");
        String str6 = payment1.getCcExpDate();
    }

    @Test
    public void test022() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test022");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        Boolean b2 = payment0.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = payment0.getReservation();
        String str5 = payment0.getCcNum();
    }

    @Test
    public void test023() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test023");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment0.getReservation();
        String str8 = payment0.getCcExpDate();
    }

    @Test
    public void test024() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test024");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        Long long4 = payment1.getFee();
        String str5 = payment1.getCcNum();
    }

    @Test
    public void test025() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test025");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Boolean b3 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = payment1.getReservation();
        payment1.setCcName("hi!");
        Long long7 = payment1.getFee();
        String str8 = payment1.getCcNum();
    }

    @Test
    public void test026() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test026");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        String str1 = payment0.getCcExpDate();
    }

    @Test
    public void test027() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test027");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        Boolean b7 = payment0.isValid();
        String str8 = payment0.getCcExpDate();
    }

    @Test
    public void test028() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test028");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        String str4 = payment1.getCcNum();
    }

    @Test
    public void test029() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test029");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        String str1 = payment0.getCcName();
    }

    @Test
    public void test030() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test030");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        Long long4 = payment1.getFee();
        Long long5 = payment1.getFee();
        String str6 = payment1.getCcExpDate();
    }

    @Test
    public void test031() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test031");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        Boolean b5 = payment1.isValid();
        payment1.setFee((Long) 0L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment1.getReservation();
        String str9 = payment1.getCcNum();
    }

    @Test
    public void test032() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test032");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Boolean b3 = payment0.isValid();
        String str4 = payment0.getCcExpDate();
    }

    @Test
    public void test033() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test033");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        payment0.setCcName("");
        payment0.setCcName("");
        String str6 = payment0.getCcExpDate();
    }

    @Test
    public void test034() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test034");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        payment0.setCcNum("hi!");
        String str5 = payment0.getCcExpDate();
    }

    @Test
    public void test035() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test035");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Boolean b3 = payment1.isValid();
        Boolean b4 = payment1.isValid();
        payment1.setCcName("");
        String str7 = payment1.getCcExpDate();
    }

    @Test
    public void test036() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test036");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment0.getReservation();
        String str9 = payment0.getCcName();
    }

    @Test
    public void test037() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test037");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        Long long4 = payment1.getFee();
        payment1.setCcName("");
        String str7 = payment1.getCcExpDate();
    }

    @Test
    public void test038() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test038");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        Boolean b2 = payment0.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = payment0.getReservation();
        Long long4 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = payment0.getReservation();
        payment0.setCcExpDate("");
        String str8 = payment0.getCcNum();
    }

    @Test
    public void test039() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test039");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        String str8 = payment0.getCcNum();
        String str9 = payment0.getCcExpDate();
        String str10 = payment0.getCcName();
    }

    @Test
    public void test040() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test040");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcExpDate("");
        String str4 = payment1.getCcName();
    }

    @Test
    public void test041() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test041");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        payment1.setFee((Long) 10L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = payment1.getReservation();
        Long long7 = payment1.getFee();
        String str8 = payment1.getCcExpDate();
    }

    @Test
    public void test042() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test042");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        Boolean b7 = payment1.isValid();
        Boolean b8 = payment1.isValid();
        payment1.setCcNum("");
        Long long11 = payment1.getFee();
        String str12 = payment1.getCcName();
    }

    @Test
    public void test043() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test043");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setFee((Long) 1L);
        String str6 = payment0.getCcExpDate();
    }

    @Test
    public void test044() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test044");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        payment1.setFee((Long) 10L);
        payment1.setCcNum("hi!");
        String str8 = payment1.getCcNum();
        String str9 = payment1.getCcName();
    }

    @Test
    public void test045() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test045");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Boolean b3 = payment1.isValid();
        Boolean b4 = payment1.isValid();
        String str5 = payment1.getCcExpDate();
    }

    @Test
    public void test046() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test046");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        String str5 = payment1.getCcExpDate();
        payment1.setFee((Long) (-1L));
        String str8 = payment1.getCcNum();
    }

    @Test
    public void test047() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test047");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        payment1.setFee((Long) 10L);
        String str6 = payment1.getCcName();
    }

    @Test
    public void test048() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test048");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Boolean b3 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = payment1.getReservation();
        payment1.setCcName("hi!");
        Boolean b7 = payment1.isValid();
        String str8 = payment1.getCcNum();
    }

    @Test
    public void test049() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test049");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) (-1L));
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        String str3 = payment1.getCcExpDate();
    }

    @Test
    public void test050() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test050");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        Boolean b5 = payment1.isValid();
        String str6 = payment1.getCcName();
    }

    @Test
    public void test051() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test051");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        Boolean b2 = payment0.isValid();
        Boolean b3 = payment0.isValid();
        String str4 = payment0.getCcExpDate();
    }

    @Test
    public void test052() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test052");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        String str5 = payment1.getCcExpDate();
        payment1.setFee((Long) (-1L));
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment1.getReservation();
        String str9 = payment1.getCcNum();
    }

    @Test
    public void test053() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test053");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) (-1L));
        payment1.setCcName("");
        payment1.setCcExpDate("");
        String str6 = payment1.getCcNum();
    }

    @Test
    public void test054() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test054");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        Boolean b2 = payment0.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = payment0.getReservation();
        Long long4 = payment0.getFee();
        Boolean b5 = payment0.isValid();
        String str6 = payment0.getCcName();
    }

    @Test
    public void test055() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test055");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        Boolean b4 = payment0.isValid();
        Long long5 = payment0.getFee();
        String str6 = payment0.getCcName();
    }

    @Test
    public void test056() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test056");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        Boolean b5 = payment1.isValid();
        payment1.setFee((Long) 0L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment1.getReservation();
        Boolean b9 = payment1.isValid();
        String str10 = payment1.getCcName();
    }

    @Test
    public void test057() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test057");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        payment1.setFee((Long) 10L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = payment1.getReservation();
        Boolean b7 = payment1.isValid();
        String str8 = payment1.getCcExpDate();
    }

    @Test
    public void test058() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test058");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        Boolean b5 = payment1.isValid();
        payment1.setFee((Long) 0L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment1.getReservation();
        payment1.setFee((Long) (-1L));
        String str11 = payment1.getCcName();
    }

    @Test
    public void test059() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test059");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        Boolean b2 = payment0.isValid();
        payment0.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = payment0.getReservation();
        String str6 = payment0.getCcExpDate();
    }

    @Test
    public void test060() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test060");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setCcNum("hi!");
        String str9 = payment1.getCcName();
    }

    @Test
    public void test061() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test061");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        Boolean b5 = payment1.isValid();
        payment1.setFee((Long) (-1L));
        String str8 = payment1.getCcExpDate();
        Boolean b9 = payment1.isValid();
        String str10 = payment1.getCcNum();
    }

    @Test
    public void test062() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test062");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcNum("hi!");
        payment1.setCcName("hi!");
        String str7 = payment1.getCcExpDate();
    }

    @Test
    public void test063() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test063");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        Boolean b2 = payment0.isValid();
        Boolean b3 = payment0.isValid();
        Long long4 = payment0.getFee();
        String str5 = payment0.getCcName();
    }

    @Test
    public void test064() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test064");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 0L);
        String str2 = payment1.getCcName();
    }

    @Test
    public void test065() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test065");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        Boolean b2 = payment0.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = payment0.getReservation();
        Long long4 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = payment0.getReservation();
        String str7 = payment0.getCcName();
    }

    @Test
    public void test066() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test066");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        Long long4 = payment1.getFee();
        payment1.setCcName("");
        payment1.setFee((Long) 0L);
        payment1.setCcNum("");
        String str11 = payment1.getCcExpDate();
    }

    @Test
    public void test067() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test067");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        Boolean b5 = payment1.isValid();
        payment1.setFee((Long) 0L);
        String str8 = payment1.getCcName();
    }

    @Test
    public void test068() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test068");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        payment1.setCcNum("");
        payment1.setCcExpDate("hi!");
        Boolean b6 = payment1.isValid();
        String str7 = payment1.getCcName();
    }

    @Test
    public void test069() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test069");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 100L);
        payment1.setCcName("");
        payment1.setFee((Long) 10L);
        String str6 = payment1.getCcNum();
    }

    @Test
    public void test070() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test070");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        Long long4 = payment1.getFee();
        Long long5 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = payment1.getReservation();
        String str7 = payment1.getCcExpDate();
    }

    @Test
    public void test071() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test071");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 0L);
        String str2 = payment1.getCcExpDate();
    }

    @Test
    public void test072() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test072");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        Boolean b2 = payment0.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = payment0.getReservation();
        String str5 = payment0.getCcExpDate();
    }

    @Test
    public void test073() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test073");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        Long long4 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = payment1.getReservation();
        payment1.setCcName("");
        Long long8 = payment1.getFee();
        payment1.setCcExpDate("hi!");
        String str11 = payment1.getCcNum();
    }

    @Test
    public void test074() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test074");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        Boolean b2 = payment0.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = payment0.getReservation();
        Long long4 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = payment0.getReservation();
        payment0.setCcExpDate("");
        payment0.setCcName("");
        Long long10 = payment0.getFee();
        String str11 = payment0.getCcNum();
    }

    @Test
    public void test075() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test075");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        Boolean b4 = payment0.isValid();
        Long long5 = payment0.getFee();
        payment0.setCcName("hi!");
        String str8 = payment0.getCcExpDate();
    }

    @Test
    public void test076() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test076");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        payment1.setFee((Long) 10L);
        payment1.setCcNum("hi!");
        payment1.setCcNum("hi!");
        Boolean b10 = payment1.isValid();
        String str11 = payment1.getCcExpDate();
    }

    @Test
    public void test077() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test077");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        Long long4 = payment1.getFee();
        Long long5 = payment1.getFee();
        payment1.setFee((Long) 10L);
        payment1.setFee((Long) 1L);
        String str10 = payment1.getCcExpDate();
    }

    @Test
    public void test078() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test078");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        String str5 = payment1.getCcExpDate();
        Long long6 = payment1.getFee();
        Long long7 = payment1.getFee();
        String str8 = payment1.getCcName();
    }

    @Test
    public void test079() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test079");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        payment0.setCcNum("hi!");
        payment0.setCcNum("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment0.getReservation();
        String str8 = payment0.getCcExpDate();
    }

    @Test
    public void test080() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test080");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        Long long4 = payment1.getFee();
        payment1.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment1.getReservation();
        String str8 = payment1.getCcExpDate();
    }

    @Test
    public void test081() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test081");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcExpDate("hi!");
        payment1.setCcExpDate("");
        String str9 = payment1.getCcNum();
    }

    @Test
    public void test082() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test082");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setCcNum("hi!");
        String str9 = payment1.getCcNum();
        payment1.setCcNum("hi!");
        String str12 = payment1.getCcName();
    }

    @Test
    public void test083() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test083");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment0.getReservation();
        payment0.setCcExpDate("hi!");
        Long long5 = payment0.getFee();
        String str6 = payment0.getCcName();
    }

    @Test
    public void test084() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test084");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) (-1L));
        payment1.setCcExpDate("hi!");
        payment1.setCcNum("hi!");
        String str6 = payment1.getCcName();
    }

    @Test
    public void test085() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test085");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment0.getReservation();
        Boolean b8 = payment0.isValid();
        String str9 = payment0.getCcExpDate();
    }

    @Test
    public void test086() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test086");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        payment0.setCcNum("hi!");
        Boolean b5 = payment0.isValid();
        payment0.setFee((Long) 0L);
        String str8 = payment0.getCcName();
    }

    @Test
    public void test087() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test087");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment0.getReservation();
        Long long8 = payment0.getFee();
        Long long9 = payment0.getFee();
        payment0.setCcNum("hi!");
        String str12 = payment0.getCcName();
    }

    @Test
    public void test088() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test088");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment0.getReservation();
        Boolean b9 = payment0.isValid();
        String str10 = payment0.getCcName();
    }

    @Test
    public void test089() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test089");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        String str5 = payment1.getCcExpDate();
        Long long6 = payment1.getFee();
        payment1.setFee((Long) (-1L));
        Long long9 = payment1.getFee();
        String str10 = payment1.getCcNum();
    }

    @Test
    public void test090() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test090");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        String str3 = payment0.getCcName();
    }

    @Test
    public void test091() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test091");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        Boolean b5 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = payment1.getReservation();
        payment1.setCcExpDate("hi!");
        String str9 = payment1.getCcName();
    }

    @Test
    public void test092() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test092");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Long long2 = payment1.getFee();
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        String str7 = payment1.getCcNum();
    }

    @Test
    public void test093() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test093");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        Long long7 = payment1.getFee();
        payment1.setCcNum("");
        Long long10 = payment1.getFee();
        String str11 = payment1.getCcExpDate();
        String str12 = payment1.getCcName();
    }

    @Test
    public void test094() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test094");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Boolean b3 = payment1.isValid();
        String str4 = payment1.getCcNum();
    }

    @Test
    public void test095() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test095");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment0.getReservation();
        payment0.setCcExpDate("hi!");
        String str11 = payment0.getCcExpDate();
        String str12 = payment0.getCcName();
    }

    @Test
    public void test096() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test096");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Boolean b3 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = payment1.getReservation();
        payment1.setCcName("hi!");
        Boolean b7 = payment1.isValid();
        payment1.setFee((Long) 10L);
        String str10 = payment1.getCcExpDate();
    }

    @Test
    public void test097() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test097");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        payment1.setFee((Long) 1L);
        payment1.setCcExpDate("");
        String str8 = payment1.getCcNum();
    }

    @Test
    public void test098() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test098");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        payment1.setFee((Long) 10L);
        payment1.setCcNum("hi!");
        String str8 = payment1.getCcExpDate();
    }

    @Test
    public void test099() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test099");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcName("");
        Boolean b7 = payment1.isValid();
        String str8 = payment1.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = payment1.getReservation();
        Long long10 = payment1.getFee();
        payment1.setFee((Long) (-1L));
        String str13 = payment1.getCcNum();
    }

    @Test
    public void test100() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test100");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcName("");
        Boolean b7 = payment1.isValid();
        payment1.setCcName("hi!");
        String str10 = payment1.getCcNum();
    }

    @Test
    public void test101() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test101");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcExpDate("");
        String str6 = payment0.getCcExpDate();
        String str7 = payment0.getCcName();
    }

    @Test
    public void test102() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test102");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        payment1.setFee((Long) 10L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = payment1.getReservation();
        Boolean b7 = payment1.isValid();
        payment1.setCcName("");
        payment1.setCcNum("hi!");
        String str12 = payment1.getCcName();
        payment1.setCcNum("hi!");
        String str15 = payment1.getCcExpDate();
    }

    @Test
    public void test103() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test103");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcName("");
        Boolean b7 = payment1.isValid();
        String str8 = payment1.getCcExpDate();
        String str9 = payment1.getCcNum();
    }

    @Test
    public void test104() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test104");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        payment1.setFee((Long) 10L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = payment1.getReservation();
        Long long7 = payment1.getFee();
        String str8 = payment1.getCcName();
    }

    @Test
    public void test105() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test105");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        String str2 = payment0.getCcName();
    }

    @Test
    public void test106() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test106");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) (-1L));
        payment1.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = payment1.getReservation();
        String str5 = payment1.getCcName();
        String str6 = payment1.getCcExpDate();
    }

    @Test
    public void test107() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test107");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        payment1.setCcNum("");
        payment1.setCcExpDate("hi!");
        payment1.setCcNum("hi!");
        String str8 = payment1.getCcNum();
        String str9 = payment1.getCcName();
    }

    @Test
    public void test108() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test108");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        Long long4 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = payment1.getReservation();
        payment1.setCcName("");
        Long long8 = payment1.getFee();
        payment1.setFee((Long) (-1L));
        payment1.setCcName("hi!");
        Long long13 = payment1.getFee();
        String str14 = payment1.getCcExpDate();
    }

    @Test
    public void test109() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test109");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        Boolean b4 = payment1.isValid();
        String str5 = payment1.getCcNum();
    }

    @Test
    public void test110() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test110");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        payment1.setCcNum("");
        payment1.setCcExpDate("hi!");
        Boolean b6 = payment1.isValid();
        String str7 = payment1.getCcNum();
        String str8 = payment1.getCcName();
    }

    @Test
    public void test111() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test111");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        Boolean b5 = payment1.isValid();
        payment1.setFee((Long) (-1L));
        String str8 = payment1.getCcExpDate();
        String str9 = payment1.getCcName();
    }

    @Test
    public void test112() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test112");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        Boolean b2 = payment0.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = payment0.getReservation();
        payment0.setCcExpDate("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment0.getReservation();
        String str8 = payment0.getCcName();
    }

    @Test
    public void test113() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test113");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment0.getReservation();
        payment0.setCcExpDate("hi!");
        Long long5 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = payment0.getReservation();
        String str7 = payment0.getCcName();
    }

    @Test
    public void test114() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test114");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        payment0.setCcExpDate("");
        String str9 = payment0.getCcName();
    }

    @Test
    public void test115() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test115");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment0.getReservation();
        payment0.setCcNum("hi!");
        String str11 = payment0.getCcExpDate();
    }

    @Test
    public void test116() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test116");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setCcNum("hi!");
        String str9 = payment1.getCcNum();
        Long long10 = payment1.getFee();
        payment1.setCcNum("");
        String str13 = payment1.getCcName();
    }

    @Test
    public void test117() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test117");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        String str8 = payment0.getCcNum();
        String str9 = payment0.getCcName();
    }

    @Test
    public void test118() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test118");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) (-1L));
        payment1.setFee((Long) 0L);
        payment1.setCcExpDate("hi!");
        payment1.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment1.getReservation();
        String str9 = payment1.getCcName();
    }

    @Test
    public void test119() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test119");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        Long long7 = payment1.getFee();
        payment1.setFee((Long) (-1L));
        String str10 = payment1.getCcName();
    }

    @Test
    public void test120() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test120");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        Boolean b2 = payment0.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = payment0.getReservation();
        Long long4 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = payment0.getReservation();
        payment0.setCcExpDate("");
        Long long8 = payment0.getFee();
        Long long9 = payment0.getFee();
        payment0.setCcNum("hi!");
        String str12 = payment0.getCcName();
    }

    @Test
    public void test121() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test121");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment0.getReservation();
        Boolean b9 = payment0.isValid();
        payment0.setCcNum("hi!");
        String str12 = payment0.getCcExpDate();
    }

    @Test
    public void test122() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test122");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        payment0.setCcName("");
        payment0.setCcName("");
        payment0.setCcExpDate("hi!");
        String str8 = payment0.getCcName();
        payment0.setCcExpDate("");
        String str11 = payment0.getCcNum();
    }

    @Test
    public void test123() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test123");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment0.getReservation();
        payment0.setCcNum("hi!");
        String str11 = payment0.getCcNum();
        String str12 = payment0.getCcName();
    }

    @Test
    public void test124() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test124");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        Boolean b2 = payment0.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = payment0.getReservation();
        String str6 = payment0.getCcExpDate();
    }

    @Test
    public void test125() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test125");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        payment0.setCcExpDate("");
        payment0.setCcExpDate("hi!");
        String str6 = payment0.getCcName();
    }

    @Test
    public void test126() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test126");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        String str2 = payment1.getCcExpDate();
    }

    @Test
    public void test127() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test127");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) (-1L));
        payment1.setFee((Long) 0L);
        payment1.setCcExpDate("hi!");
        payment1.setCcExpDate("");
        String str8 = payment1.getCcName();
    }

    @Test
    public void test128() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test128");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        payment1.setFee((Long) 10L);
        payment1.setCcExpDate("hi!");
        Boolean b8 = payment1.isValid();
        String str9 = payment1.getCcNum();
    }

    @Test
    public void test129() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test129");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        String str5 = payment1.getCcExpDate();
        payment1.setFee((Long) (-1L));
        Boolean b8 = payment1.isValid();
        String str9 = payment1.getCcExpDate();
        String str10 = payment1.getCcName();
    }

    @Test
    public void test130() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test130");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        Long long4 = payment1.getFee();
        Long long5 = payment1.getFee();
        payment1.setFee((Long) 10L);
        String str8 = payment1.getCcName();
    }

    @Test
    public void test131() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test131");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        payment1.setCcNum("");
        Boolean b4 = payment1.isValid();
        String str5 = payment1.getCcName();
    }

    @Test
    public void test132() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test132");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        String str8 = payment0.getCcNum();
        String str9 = payment0.getCcExpDate();
        payment0.setFee((Long) 0L);
        String str12 = payment0.getCcNum();
        String str13 = payment0.getCcExpDate();
        String str14 = payment0.getCcName();
    }

    @Test
    public void test133() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test133");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcNum("hi!");
        payment1.setCcName("hi!");
        String str7 = payment1.getCcNum();
        String str8 = payment1.getCcExpDate();
    }

    @Test
    public void test134() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test134");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) (-1L));
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setFee((Long) 0L);
        payment1.setCcNum("hi!");
        String str7 = payment1.getCcExpDate();
    }

    @Test
    public void test135() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test135");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment0.getReservation();
        payment0.setCcExpDate("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation11 = payment0.getReservation();
        payment0.setFee((Long) 1L);
        String str14 = payment0.getCcName();
    }

    @Test
    public void test136() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test136");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) (-1L));
        payment1.setFee((Long) 0L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = payment1.getReservation();
        payment1.setFee((Long) 100L);
        String str7 = payment1.getCcName();
    }

    @Test
    public void test137() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test137");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment0.getReservation();
        payment0.setCcExpDate("hi!");
        Long long5 = payment0.getFee();
        payment0.setCcExpDate("");
        String str8 = payment0.getCcName();
    }

    @Test
    public void test138() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test138");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Boolean b3 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = payment1.getReservation();
        payment1.setCcName("hi!");
        Boolean b7 = payment1.isValid();
        payment1.setFee((Long) 100L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation10 = payment1.getReservation();
        String str11 = payment1.getCcExpDate();
    }

    @Test
    public void test139() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test139");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        payment1.setCcNum("hi!");
        Boolean b6 = payment1.isValid();
        Long long7 = payment1.getFee();
        String str8 = payment1.getCcExpDate();
    }

    @Test
    public void test140() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test140");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setCcNum("hi!");
        String str9 = payment1.getCcNum();
        Long long10 = payment1.getFee();
        payment1.setCcNum("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = payment1.getReservation();
        payment1.setCcNum("hi!");
        String str16 = payment1.getCcName();
    }

    @Test
    public void test141() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test141");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        Boolean b2 = payment0.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = payment0.getReservation();
        Long long4 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = payment0.getReservation();
        payment0.setCcExpDate("");
        Long long8 = payment0.getFee();
        String str9 = payment0.getCcNum();
    }

    @Test
    public void test142() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test142");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        Long long7 = payment1.getFee();
        payment1.setCcExpDate("hi!");
        String str10 = payment1.getCcName();
    }

    @Test
    public void test143() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test143");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        payment1.setFee((Long) 10L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = payment1.getReservation();
        Boolean b7 = payment1.isValid();
        payment1.setCcName("");
        String str10 = payment1.getCcExpDate();
    }

    @Test
    public void test144() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test144");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) (-1L));
        payment1.setFee((Long) (-1L));
        payment1.setFee((Long) 100L);
        String str6 = payment1.getCcExpDate();
    }

    @Test
    public void test145() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test145");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        Boolean b5 = payment1.isValid();
        payment1.setFee((Long) 0L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment1.getReservation();
        payment1.setCcExpDate("hi!");
        payment1.setFee((Long) 100L);
        String str13 = payment1.getCcNum();
    }

    @Test
    public void test146() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test146");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        Boolean b8 = payment0.isValid();
        String str9 = payment0.getCcExpDate();
        String str10 = payment0.getCcName();
    }

    @Test
    public void test147() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test147");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        Long long7 = payment1.getFee();
        Long long8 = payment1.getFee();
        payment1.setCcNum("");
        Boolean b11 = payment1.isValid();
        String str12 = payment1.getCcName();
    }

    @Test
    public void test148() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test148");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Boolean b3 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = payment1.getReservation();
        payment1.setCcName("hi!");
        payment1.setCcName("");
        Boolean b9 = payment1.isValid();
        Boolean b10 = payment1.isValid();
        payment1.setCcNum("");
        payment1.setCcNum("hi!");
        String str15 = payment1.getCcExpDate();
    }

    @Test
    public void test149() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test149");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        payment1.setFee((Long) 10L);
        payment1.setCcNum("hi!");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        String str12 = payment1.getCcName();
    }

    @Test
    public void test150() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test150");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        payment1.setFee((Long) 10L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = payment1.getReservation();
        Boolean b7 = payment1.isValid();
        payment1.setCcName("");
        payment1.setCcName("");
        Boolean b12 = payment1.isValid();
        String str13 = payment1.getCcExpDate();
    }

    @Test
    public void test151() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test151");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        Long long3 = payment1.getFee();
        String str4 = payment1.getCcName();
    }

    @Test
    public void test152() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test152");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        String str8 = payment0.getCcNum();
        String str9 = payment0.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation10 = payment0.getReservation();
        String str11 = payment0.getCcExpDate();
        String str12 = payment0.getCcName();
    }

    @Test
    public void test153() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test153");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        Boolean b5 = payment1.isValid();
        payment1.setFee((Long) (-1L));
        String str8 = payment1.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = payment1.getReservation();
        String str10 = payment1.getCcName();
    }

    @Test
    public void test154() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test154");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        payment0.setCcExpDate("");
        payment0.setFee((Long) 10L);
        String str6 = payment0.getCcName();
    }

    @Test
    public void test155() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test155");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) (-1L));
        payment1.setFee((Long) 0L);
        payment1.setCcExpDate("hi!");
        payment1.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment1.getReservation();
        payment1.setFee((Long) 1L);
        String str11 = payment1.getCcExpDate();
        payment1.setCcNum("hi!");
        String str14 = payment1.getCcName();
    }

    @Test
    public void test156() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test156");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        String str2 = payment1.getCcExpDate();
    }

    @Test
    public void test157() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test157");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment0.getReservation();
        payment0.setCcExpDate("hi!");
        Long long5 = payment0.getFee();
        payment0.setCcExpDate("");
        payment0.setFee((Long) 100L);
        String str10 = payment0.getCcNum();
    }

    @Test
    public void test158() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test158");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        Boolean b7 = payment1.isValid();
        Boolean b8 = payment1.isValid();
        payment1.setCcNum("");
        Long long11 = payment1.getFee();
        Boolean b12 = payment1.isValid();
        String str13 = payment1.getCcName();
    }

    @Test
    public void test159() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test159");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        Boolean b5 = payment1.isValid();
        payment1.setFee((Long) (-1L));
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        String str14 = payment1.getCcName();
    }

    @Test
    public void test160() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test160");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setCcNum("hi!");
        String str9 = payment1.getCcNum();
        payment1.setFee((Long) 10L);
        Long long12 = payment1.getFee();
        String str13 = payment1.getCcName();
    }

    @Test
    public void test161() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test161");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcName("");
        String str6 = payment0.getCcExpDate();
    }

    @Test
    public void test162() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test162");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        Boolean b5 = payment1.isValid();
        payment1.setFee((Long) 0L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment1.getReservation();
        Boolean b9 = payment1.isValid();
        Boolean b10 = payment1.isValid();
        String str11 = payment1.getCcNum();
    }

    @Test
    public void test163() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test163");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        Boolean b4 = payment1.isValid();
        Boolean b5 = payment1.isValid();
        String str6 = payment1.getCcExpDate();
    }

    @Test
    public void test164() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test164");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        Long long4 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = payment1.getReservation();
        payment1.setCcName("");
        Boolean b8 = payment1.isValid();
        String str9 = payment1.getCcName();
        String str10 = payment1.getCcExpDate();
    }

    @Test
    public void test165() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test165");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        Long long4 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = payment1.getReservation();
        payment1.setCcName("");
        Long long8 = payment1.getFee();
        payment1.setCcExpDate("hi!");
        payment1.setCcName("hi!");
        String str13 = payment1.getCcExpDate();
        String str14 = payment1.getCcNum();
    }

    @Test
    public void test166() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test166");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcNum("hi!");
        payment1.setCcName("hi!");
        String str7 = payment1.getCcNum();
        String str8 = payment1.getCcNum();
        Long long9 = payment1.getFee();
        String str10 = payment1.getCcExpDate();
    }

    @Test
    public void test167() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test167");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        Long long4 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = payment1.getReservation();
        String str6 = payment1.getCcName();
    }

    @Test
    public void test168() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test168");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Boolean b3 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = payment1.getReservation();
        payment1.setCcName("hi!");
        Boolean b7 = payment1.isValid();
        payment1.setFee((Long) 100L);
        Boolean b10 = payment1.isValid();
        String str11 = payment1.getCcNum();
    }

    @Test
    public void test169() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test169");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        Boolean b2 = payment0.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = payment0.getReservation();
        Long long4 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = payment0.getReservation();
        String str6 = payment0.getCcExpDate();
    }

    @Test
    public void test170() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test170");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) (-1L));
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = payment1.getReservation();
        String str4 = payment1.getCcExpDate();
    }

    @Test
    public void test171() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test171");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        payment0.setCcExpDate("");
        payment0.setCcNum("hi!");
        payment0.setFee((Long) 0L);
        payment0.setFee((Long) 100L);
        payment0.setCcNum("hi!");
        payment0.setCcExpDate("");
        String str19 = payment0.getCcName();
    }

    @Test
    public void test172() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test172");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        payment0.setCcNum("hi!");
        payment0.setCcNum("");
        String str7 = payment0.getCcName();
    }

    @Test
    public void test173() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test173");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment0.getReservation();
        payment0.setCcNum("hi!");
        Long long11 = payment0.getFee();
        Long long12 = payment0.getFee();
        payment0.setCcNum("");
        String str15 = payment0.getCcName();
    }

    @Test
    public void test174() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test174");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        Long long4 = payment1.getFee();
        payment1.setCcName("");
        payment1.setFee((Long) 0L);
        String str9 = payment1.getCcExpDate();
    }

    @Test
    public void test175() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test175");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        payment1.setFee((Long) 1L);
        Long long6 = payment1.getFee();
        String str7 = payment1.getCcName();
    }

    @Test
    public void test176() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "ErrorTest0.test176");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        Boolean b4 = payment0.isValid();
        Long long5 = payment0.getFee();
        payment0.setFee((Long) 0L);
        String str8 = payment0.getCcName();
    }
}

