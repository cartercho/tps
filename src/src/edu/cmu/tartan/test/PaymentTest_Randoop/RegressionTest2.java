package edu.cmu.tartan.test.PaymentTest_Randoop;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RegressionTest2 {

    public static boolean debug = false;

    @Test
    public void test01() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test01");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcName("");
        payment0.setCcExpDate("hi!");
        String str8 = payment0.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = payment0.getReservation();
        payment0.setCcNum("hi!");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "hi!" + "'", str8.equals("hi!"));
        org.junit.Assert.assertNull(reservation9);
    }

    @Test
    public void test02() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test02");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcNum("hi!");
        payment1.setCcName("hi!");
        Long long7 = payment1.getFee();
        payment1.setCcName("hi!");
        String str10 = payment1.getCcName();
        payment1.setFee((Long) (-1L));
        String str13 = payment1.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation14 = null;
        payment1.setReservation(reservation14);
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 1L + "'", long7.equals(1L));
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "hi!" + "'", str10.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "hi!" + "'", str13.equals("hi!"));
    }

    @Test
    public void test03() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test03");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        payment1.setCcNum("hi!");
        Long long13 = payment1.getFee();
        payment1.setCcExpDate("");
        Long long16 = payment1.getFee();
        payment1.setFee((Long) (-1L));
        payment1.setFee((Long) 0L);
        Boolean b21 = payment1.isValid();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long13 + "' != '" + 100L + "'", long13.equals(100L));
        org.junit.Assert.assertTrue("'" + long16 + "' != '" + 100L + "'", long16.equals(100L));
        org.junit.Assert.assertTrue("'" + b21 + "' != '" + true + "'", b21.equals(true));
    }

    @Test
    public void test04() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test04");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        payment0.setCcName("hi!");
        Boolean b14 = payment0.isValid();
        Boolean b15 = payment0.isValid();
        Long long16 = payment0.getFee();
        String str17 = payment0.getCcExpDate();
        Boolean b18 = payment0.isValid();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + b14 + "' != '" + true + "'", b14.equals(true));
        org.junit.Assert.assertTrue("'" + b15 + "' != '" + true + "'", b15.equals(true));
        org.junit.Assert.assertNull(long16);
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "" + "'", str17.equals(""));
        org.junit.Assert.assertTrue("'" + b18 + "' != '" + true + "'", b18.equals(true));
    }

    @Test
    public void test05() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test05");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        payment1.setCcNum("hi!");
        Long long13 = payment1.getFee();
        payment1.setCcExpDate("");
        String str16 = payment1.getCcNum();
        Boolean b17 = payment1.isValid();
        String str18 = payment1.getCcName();
        Boolean b19 = payment1.isValid();
        String str20 = payment1.getCcNum();
        payment1.setCcExpDate("");
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long13 + "' != '" + 100L + "'", long13.equals(100L));
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "hi!" + "'", str16.equals("hi!"));
        org.junit.Assert.assertTrue("'" + b17 + "' != '" + true + "'", b17.equals(true));
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "hi!" + "'", str18.equals("hi!"));
        org.junit.Assert.assertTrue("'" + b19 + "' != '" + true + "'", b19.equals(true));
        org.junit.Assert.assertTrue("'" + str20 + "' != '" + "hi!" + "'", str20.equals("hi!"));
    }

    @Test
    public void test06() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test06");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        String str8 = payment0.getCcNum();
        String str9 = payment0.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation10 = payment0.getReservation();
        Boolean b11 = payment0.isValid();
        payment0.setCcExpDate("");
        Long long14 = payment0.getFee();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "" + "'", str9.equals(""));
        org.junit.Assert.assertNull(reservation10);
        org.junit.Assert.assertTrue("'" + b11 + "' != '" + false + "'", b11.equals(false));
        org.junit.Assert.assertNull(long14);
    }

    @Test
    public void test07() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test07");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation11 = payment1.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation12 = payment1.getReservation();
        Boolean b13 = payment1.isValid();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertNull(reservation11);
        org.junit.Assert.assertNull(reservation12);
        org.junit.Assert.assertTrue("'" + b13 + "' != '" + true + "'", b13.equals(true));
    }

    @Test
    public void test08() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test08");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = null;
        payment1.setReservation(reservation4);
        payment1.setFee((Long) (-1L));
        Boolean b8 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = payment1.getReservation();
        payment1.setCcExpDate("");
        org.junit.Assert.assertTrue("'" + b8 + "' != '" + false + "'", b8.equals(false));
        org.junit.Assert.assertNull(reservation9);
    }

    @Test
    public void test09() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test09");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        Boolean b5 = payment1.isValid();
        payment1.setFee((Long) 0L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment1.getReservation();
        Boolean b9 = payment1.isValid();
        Boolean b10 = payment1.isValid();
        String str11 = payment1.getCcExpDate();
        payment1.setCcName("hi!");
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b5 + "' != '" + false + "'", b5.equals(false));
        org.junit.Assert.assertNull(reservation8);
        org.junit.Assert.assertTrue("'" + b9 + "' != '" + false + "'", b9.equals(false));
        org.junit.Assert.assertTrue("'" + b10 + "' != '" + false + "'", b10.equals(false));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "" + "'", str11.equals(""));
    }

    @Test
    public void test10() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test10");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        payment1.setCcNum("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = null;
        payment1.setReservation(reservation13);
        payment1.setCcName("hi!");
        payment1.setFee((Long) (-1L));
        org.junit.Assert.assertNull(reservation2);
    }

    @Test
    public void test11() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test11");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        payment0.setCcExpDate("");
        payment0.setCcNum("hi!");
        payment0.setFee((Long) 0L);
        payment0.setCcExpDate("hi!");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "" + "'", str6.equals(""));
    }

    @Test
    public void test12() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test12");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        String str10 = payment0.getCcExpDate();
        payment0.setCcExpDate("");
        payment0.setCcNum("hi!");
        String str15 = payment0.getCcExpDate();
        payment0.setFee((Long) (-1L));
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "" + "'", str15.equals(""));
    }

    @Test
    public void test13() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test13");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment0.getReservation();
        payment0.setCcExpDate("hi!");
        String str11 = payment0.getCcExpDate();
        String str12 = payment0.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = null;
        payment0.setReservation(reservation13);
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "" + "'", str6.equals(""));
        org.junit.Assert.assertNull(reservation7);
        org.junit.Assert.assertNull(reservation8);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "hi!" + "'", str11.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "hi!" + "'", str12.equals("hi!"));
    }

    @Test
    public void test14() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test14");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        payment0.setCcExpDate("");
        payment0.setCcExpDate("hi!");
        payment0.setCcNum("");
        org.junit.Assert.assertNull(long1);
    }

    @Test
    public void test15() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test15");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        Boolean b2 = payment0.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = payment0.getReservation();
        Long long4 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = payment0.getReservation();
        payment0.setCcName("hi!");
        String str8 = payment0.getCcName();
        String str9 = payment0.getCcName();
        Boolean b10 = payment0.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation11 = null;
        payment0.setReservation(reservation11);
        org.junit.Assert.assertNull(long1);
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertNull(reservation3);
        org.junit.Assert.assertNull(long4);
        org.junit.Assert.assertNull(reservation5);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "hi!" + "'", str8.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "hi!" + "'", str9.equals("hi!"));
        org.junit.Assert.assertTrue("'" + b10 + "' != '" + false + "'", b10.equals(false));
    }

    @Test
    public void test16() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test16");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation10 = null;
        payment0.setReservation(reservation10);
        Long long12 = payment0.getFee();
        Boolean b13 = payment0.isValid();
        Boolean b14 = payment0.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation15 = null;
        payment0.setReservation(reservation15);
        Long long17 = payment0.getFee();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertNull(long12);
        org.junit.Assert.assertTrue("'" + b13 + "' != '" + true + "'", b13.equals(true));
        org.junit.Assert.assertTrue("'" + b14 + "' != '" + true + "'", b14.equals(true));
        org.junit.Assert.assertNull(long17);
    }

    @Test
    public void test17() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test17");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = null;
        payment1.setReservation(reservation3);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = null;
        payment1.setReservation(reservation5);
        payment1.setCcNum("hi!");
        payment1.setCcExpDate("");
        payment1.setCcExpDate("");
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
    }

    @Test
    public void test18() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test18");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) (-1L));
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = null;
        payment1.setReservation(reservation2);
        payment1.setCcName("hi!");
        Long long6 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment1.getReservation();
        payment1.setCcExpDate("");
        String str10 = payment1.getCcExpDate();
        String str11 = payment1.getCcName();
        String str12 = payment1.getCcName();
        org.junit.Assert.assertTrue("'" + long6 + "' != '" + (-1L) + "'", long6.equals((-1L)));
        org.junit.Assert.assertNull(reservation7);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "hi!" + "'", str11.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "hi!" + "'", str12.equals("hi!"));
    }

    @Test
    public void test19() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test19");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment0.getReservation();
        payment0.setCcExpDate("");
        String str5 = payment0.getCcExpDate();
        payment0.setCcNum("");
        payment0.setCcName("hi!");
        org.junit.Assert.assertNull(long1);
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "" + "'", str5.equals(""));
    }

    @Test
    public void test20() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test20");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        payment1.setCcNum("hi!");
        payment1.setCcNum("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation15 = payment1.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation16 = payment1.getReservation();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertNull(reservation15);
        org.junit.Assert.assertNull(reservation16);
    }

    @Test
    public void test21() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test21");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) (-1L));
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = null;
        payment1.setReservation(reservation2);
        payment1.setCcName("");
    }

    @Test
    public void test22() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test22");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        String str8 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment0.setReservation(reservation9);
        payment0.setCcName("hi!");
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        payment0.setCcName("");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
    }

    @Test
    public void test23() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test23");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = null;
        payment1.setReservation(reservation3);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = null;
        payment1.setReservation(reservation5);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment1.getReservation();
        Long long8 = payment1.getFee();
        payment1.setCcName("");
        Long long11 = payment1.getFee();
        Long long12 = payment1.getFee();
        payment1.setCcExpDate("");
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertNull(reservation7);
        org.junit.Assert.assertTrue("'" + long8 + "' != '" + 1L + "'", long8.equals(1L));
        org.junit.Assert.assertTrue("'" + long11 + "' != '" + 1L + "'", long11.equals(1L));
        org.junit.Assert.assertTrue("'" + long12 + "' != '" + 1L + "'", long12.equals(1L));
    }

    @Test
    public void test24() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test24");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment0.getReservation();
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        Boolean b13 = payment0.isValid();
        String str14 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation15 = null;
        payment0.setReservation(reservation15);
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertNull(reservation8);
        org.junit.Assert.assertTrue("'" + b13 + "' != '" + true + "'", b13.equals(true));
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "" + "'", str14.equals(""));
    }

    @Test
    public void test25() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test25");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        String str12 = payment0.getCcName();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = null;
        payment0.setReservation(reservation13);
        String str15 = payment0.getCcNum();
        Long long16 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation17 = payment0.getReservation();
        Boolean b18 = payment0.isValid();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "hi!" + "'", str12.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "" + "'", str15.equals(""));
        org.junit.Assert.assertNull(long16);
        org.junit.Assert.assertNull(reservation17);
        org.junit.Assert.assertTrue("'" + b18 + "' != '" + true + "'", b18.equals(true));
    }

    @Test
    public void test26() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test26");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        payment1.setCcNum("");
        payment1.setCcExpDate("hi!");
        payment1.setCcNum("hi!");
        payment1.setCcName("");
        payment1.setCcName("");
        Long long12 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = null;
        payment1.setReservation(reservation13);
        payment1.setFee((Long) 10L);
        org.junit.Assert.assertTrue("'" + long12 + "' != '" + 1L + "'", long12.equals(1L));
    }

    @Test
    public void test27() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test27");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        Boolean b5 = payment1.isValid();
        payment1.setFee((Long) 0L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment1.getReservation();
        Boolean b9 = payment1.isValid();
        Boolean b10 = payment1.isValid();
        payment1.setCcName("hi!");
        payment1.setCcNum("");
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b5 + "' != '" + false + "'", b5.equals(false));
        org.junit.Assert.assertNull(reservation8);
        org.junit.Assert.assertTrue("'" + b9 + "' != '" + false + "'", b9.equals(false));
        org.junit.Assert.assertTrue("'" + b10 + "' != '" + false + "'", b10.equals(false));
    }

    @Test
    public void test28() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test28");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        String str10 = payment0.getCcExpDate();
        payment0.setCcExpDate("");
        payment0.setFee((Long) 0L);
        payment0.setCcExpDate("");
        String str17 = payment0.getCcName();
        payment0.setCcExpDate("hi!");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "hi!" + "'", str17.equals("hi!"));
    }

    @Test
    public void test29() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test29");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcExpDate("hi!");
        Boolean b11 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation12 = null;
        payment1.setReservation(reservation12);
        payment1.setFee((Long) (-1L));
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b11 + "' != '" + false + "'", b11.equals(false));
    }

    @Test
    public void test30() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test30");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        payment1.setFee((Long) 1L);
        payment1.setCcExpDate("");
        String str8 = payment1.getCcExpDate();
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + long3 + "' != '" + 1L + "'", long3.equals(1L));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
    }

    @Test
    public void test31() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test31");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        String str12 = payment0.getCcName();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = null;
        payment0.setReservation(reservation13);
        payment0.setCcNum("");
        String str17 = payment0.getCcNum();
        String str18 = payment0.getCcNum();
        String str19 = payment0.getCcName();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "hi!" + "'", str12.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "" + "'", str17.equals(""));
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "" + "'", str18.equals(""));
        org.junit.Assert.assertTrue("'" + str19 + "' != '" + "hi!" + "'", str19.equals("hi!"));
    }

    @Test
    public void test32() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test32");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        payment1.setFee((Long) 10L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = payment1.getReservation();
        Boolean b7 = payment1.isValid();
        payment1.setCcExpDate("");
        payment1.setFee((Long) (-1L));
        Boolean b12 = payment1.isValid();
        Long long13 = payment1.getFee();
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + long3 + "' != '" + 1L + "'", long3.equals(1L));
        org.junit.Assert.assertNull(reservation6);
        org.junit.Assert.assertTrue("'" + b7 + "' != '" + false + "'", b7.equals(false));
        org.junit.Assert.assertTrue("'" + b12 + "' != '" + false + "'", b12.equals(false));
        org.junit.Assert.assertTrue("'" + long13 + "' != '" + (-1L) + "'", long13.equals((-1L)));
    }

    @Test
    public void test33() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test33");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        String str10 = payment0.getCcExpDate();
        String str11 = payment0.getCcNum();
        payment0.setFee((Long) 100L);
        payment0.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation16 = payment0.getReservation();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "" + "'", str11.equals(""));
        org.junit.Assert.assertNull(reservation16);
    }

    @Test
    public void test34() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test34");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        payment1.setFee((Long) 10L);
        payment1.setCcNum("hi!");
        Boolean b8 = payment1.isValid();
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + long3 + "' != '" + 1L + "'", long3.equals(1L));
        org.junit.Assert.assertTrue("'" + b8 + "' != '" + false + "'", b8.equals(false));
    }

    @Test
    public void test35() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test35");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        Boolean b4 = payment0.isValid();
        Long long5 = payment0.getFee();
        payment0.setCcName("hi!");
        Boolean b8 = payment0.isValid();
        payment0.setCcNum("hi!");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + b4 + "' != '" + false + "'", b4.equals(false));
        org.junit.Assert.assertNull(long5);
        org.junit.Assert.assertTrue("'" + b8 + "' != '" + false + "'", b8.equals(false));
    }

    @Test
    public void test36() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test36");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = null;
        payment1.setReservation(reservation4);
        payment1.setFee((Long) 0L);
        String str8 = payment1.getCcName();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = payment1.getReservation();
        payment1.setCcName("");
        payment1.setCcName("");
        payment1.setCcNum("");
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertNull(reservation9);
    }

    @Test
    public void test37() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test37");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        payment1.setCcNum("hi!");
        Long long13 = payment1.getFee();
        payment1.setCcExpDate("");
        String str16 = payment1.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation17 = null;
        payment1.setReservation(reservation17);
        payment1.setFee((Long) 1L);
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long13 + "' != '" + 100L + "'", long13.equals(100L));
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "hi!" + "'", str16.equals("hi!"));
    }

    @Test
    public void test38() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test38");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = null;
        payment1.setReservation(reservation3);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = null;
        payment1.setReservation(reservation5);
        Long long7 = payment1.getFee();
        payment1.setCcName("hi!");
        payment1.setCcNum("");
        Boolean b12 = payment1.isValid();
        try {
            String str13 = payment1.getCcExpDate();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException");
        } catch (NullPointerException e) {
        }
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 1L + "'", long7.equals(1L));
        org.junit.Assert.assertTrue("'" + b12 + "' != '" + false + "'", b12.equals(false));
    }

    @Test
    public void test39() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test39");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        Boolean b2 = payment0.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = payment0.getReservation();
        payment0.setCcExpDate("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment0.getReservation();
        Long long8 = payment0.getFee();
        payment0.setCcNum("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation11 = null;
        payment0.setReservation(reservation11);
        org.junit.Assert.assertNull(long1);
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertNull(reservation3);
        org.junit.Assert.assertNull(reservation4);
        org.junit.Assert.assertNull(reservation7);
        org.junit.Assert.assertNull(long8);
    }

    @Test
    public void test40() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test40");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        Boolean b2 = payment0.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = payment0.getReservation();
        Long long4 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = payment0.getReservation();
        Long long6 = payment0.getFee();
        payment0.setCcNum("");
        Long long9 = payment0.getFee();
        payment0.setCcNum("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation12 = payment0.getReservation();
        org.junit.Assert.assertNull(long1);
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertNull(reservation3);
        org.junit.Assert.assertNull(long4);
        org.junit.Assert.assertNull(reservation5);
        org.junit.Assert.assertNull(long6);
        org.junit.Assert.assertNull(long9);
        org.junit.Assert.assertNull(reservation12);
    }

    @Test
    public void test41() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test41");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        String str8 = payment0.getCcNum();
        String str9 = payment0.getCcExpDate();
        payment0.setFee((Long) 0L);
        String str12 = payment0.getCcNum();
        Boolean b13 = payment0.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation14 = null;
        payment0.setReservation(reservation14);
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "" + "'", str9.equals(""));
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "" + "'", str12.equals(""));
        org.junit.Assert.assertTrue("'" + b13 + "' != '" + false + "'", b13.equals(false));
    }
}

