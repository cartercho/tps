package edu.cmu.tartan.test.PaymentTest_Randoop;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RegressionTest1 {

    public static boolean debug = false;

    @Test
    public void test001() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test001");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        payment0.setCcNum("hi!");
        payment0.setCcNum("");
        payment0.setCcName("");
    }

    @Test
    public void test002() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test002");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        payment1.setCcNum("hi!");
        Long long13 = payment1.getFee();
        payment1.setCcExpDate("");
        String str16 = payment1.getCcNum();
        Long long17 = payment1.getFee();
        Long long18 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation19 = null;
        payment1.setReservation(reservation19);
        Boolean b21 = payment1.isValid();
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        payment1.setCcName("hi!");
        payment1.setFee((Long) 0L);
        Boolean b30 = payment1.isValid();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long13 + "' != '" + 100L + "'", long13.equals(100L));
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "hi!" + "'", str16.equals("hi!"));
        org.junit.Assert.assertTrue("'" + long17 + "' != '" + 100L + "'", long17.equals(100L));
        org.junit.Assert.assertTrue("'" + long18 + "' != '" + 100L + "'", long18.equals(100L));
        org.junit.Assert.assertTrue("'" + b21 + "' != '" + true + "'", b21.equals(true));
        org.junit.Assert.assertTrue("'" + b30 + "' != '" + true + "'", b30.equals(true));
    }

    @Test
    public void test003() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test003");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        payment0.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment0.setReservation(reservation9);
        String str11 = payment0.getCcNum();
        payment0.setFee((Long) 1L);
        payment0.setFee((Long) 0L);
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "" + "'", str6.equals(""));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "" + "'", str11.equals(""));
    }

    @Test
    public void test004() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test004");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcNum("hi!");
        payment1.setCcName("hi!");
        Long long7 = payment1.getFee();
        String str8 = payment1.getCcNum();
        payment1.setFee((Long) 100L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation11 = null;
        payment1.setReservation(reservation11);
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 1L + "'", long7.equals(1L));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "hi!" + "'", str8.equals("hi!"));
    }

    @Test
    public void test005() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test005");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        String str8 = payment0.getCcNum();
        String str9 = payment0.getCcExpDate();
        payment0.setFee((Long) 0L);
        String str12 = payment0.getCcNum();
        String str13 = payment0.getCcExpDate();
        Long long14 = payment0.getFee();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "" + "'", str9.equals(""));
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "" + "'", str12.equals(""));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "" + "'", str13.equals(""));
        org.junit.Assert.assertTrue("'" + long14 + "' != '" + 0L + "'", long14.equals(0L));
    }

    @Test
    public void test006() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test006");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        String str8 = payment0.getCcNum();
        String str9 = payment0.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation10 = payment0.getReservation();
        String str11 = payment0.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation12 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = null;
        payment0.setReservation(reservation13);
        payment0.setCcName("");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "" + "'", str9.equals(""));
        org.junit.Assert.assertNull(reservation10);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "" + "'", str11.equals(""));
        org.junit.Assert.assertNull(reservation12);
    }

    @Test
    public void test007() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test007");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcName("");
        Boolean b7 = payment1.isValid();
        String str8 = payment1.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = payment1.getReservation();
        payment1.setFee((Long) 0L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation12 = null;
        payment1.setReservation(reservation12);
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b7 + "' != '" + false + "'", b7.equals(false));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertNull(reservation9);
    }

    @Test
    public void test008() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test008");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        Boolean b5 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = null;
        payment1.setReservation(reservation6);
        payment1.setCcExpDate("");
        String str10 = payment1.getCcExpDate();
        payment1.setCcExpDate("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = payment1.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation14 = null;
        payment1.setReservation(reservation14);
        payment1.setFee((Long) (-1L));
        payment1.setCcName("hi!");
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b5 + "' != '" + false + "'", b5.equals(false));
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertNull(reservation13);
    }

    @Test
    public void test009() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test009");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcName("");
        payment0.setCcExpDate("hi!");
        payment0.setCcExpDate("hi!");
        Boolean b10 = payment0.isValid();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + b10 + "' != '" + true + "'", b10.equals(true));
    }

    @Test
    public void test010() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test010");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        payment0.setCcName("");
        Long long4 = payment0.getFee();
        org.junit.Assert.assertNull(long1);
        org.junit.Assert.assertNull(long4);
    }

    @Test
    public void test011() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test011");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = null;
        payment1.setReservation(reservation3);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = null;
        payment1.setReservation(reservation5);
        payment1.setCcNum("hi!");
        Boolean b9 = payment1.isValid();
        Long long10 = payment1.getFee();
        payment1.setCcName("");
        try {
            String str13 = payment1.getCcExpDate();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException");
        } catch (NullPointerException e) {
        }
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + b9 + "' != '" + false + "'", b9.equals(false));
        org.junit.Assert.assertTrue("'" + long10 + "' != '" + 1L + "'", long10.equals(1L));
    }

    @Test
    public void test012() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test012");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment0.getReservation();
        Boolean b8 = payment0.isValid();
        Long long9 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation10 = null;
        payment0.setReservation(reservation10);
        payment0.setCcName("");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertNull(reservation6);
        org.junit.Assert.assertNull(reservation7);
        org.junit.Assert.assertTrue("'" + b8 + "' != '" + false + "'", b8.equals(false));
        org.junit.Assert.assertNull(long9);
    }

    @Test
    public void test013() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test013");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = null;
        payment1.setReservation(reservation3);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = null;
        payment1.setReservation(reservation5);
        payment1.setCcNum("hi!");
        payment1.setCcExpDate("");
        Long long11 = payment1.getFee();
        payment1.setCcName("");
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + long11 + "' != '" + 1L + "'", long11.equals(1L));
    }

    @Test
    public void test014() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test014");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Boolean b3 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = payment1.getReservation();
        payment1.setCcName("hi!");
        Boolean b7 = payment1.isValid();
        payment1.setFee((Long) 10L);
        payment1.setCcName("");
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + b3 + "' != '" + false + "'", b3.equals(false));
        org.junit.Assert.assertNull(reservation4);
        org.junit.Assert.assertTrue("'" + b7 + "' != '" + false + "'", b7.equals(false));
    }

    @Test
    public void test015() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test015");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        String str8 = payment0.getCcNum();
        String str9 = payment0.getCcNum();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "" + "'", str9.equals(""));
    }

    @Test
    public void test016() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test016");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        String str5 = payment1.getCcExpDate();
        Boolean b6 = payment1.isValid();
        String str7 = payment1.getCcExpDate();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "" + "'", str5.equals(""));
        org.junit.Assert.assertTrue("'" + b6 + "' != '" + false + "'", b6.equals(false));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "" + "'", str7.equals(""));
    }

    @Test
    public void test017() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test017");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        Boolean b5 = payment1.isValid();
        payment1.setFee((Long) (-1L));
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation14 = payment1.getReservation();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b5 + "' != '" + false + "'", b5.equals(false));
        org.junit.Assert.assertNull(reservation14);
    }

    @Test
    public void test018() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test018");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        Boolean b5 = payment1.isValid();
        payment1.setCcNum("");
        Long long8 = payment1.getFee();
        Boolean b9 = payment1.isValid();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b5 + "' != '" + false + "'", b5.equals(false));
        org.junit.Assert.assertTrue("'" + long8 + "' != '" + 1L + "'", long8.equals(1L));
        org.junit.Assert.assertTrue("'" + b9 + "' != '" + false + "'", b9.equals(false));
    }

    @Test
    public void test019() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test019");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation10 = null;
        payment0.setReservation(reservation10);
        Long long12 = payment0.getFee();
        String str13 = payment0.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation14 = null;
        payment0.setReservation(reservation14);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation16 = null;
        payment0.setReservation(reservation16);
        String str18 = payment0.getCcNum();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertNull(long12);
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "" + "'", str13.equals(""));
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "" + "'", str18.equals(""));
    }

    @Test
    public void test020() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test020");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = null;
        payment1.setReservation(reservation2);
        Boolean b4 = payment1.isValid();
        Boolean b5 = payment1.isValid();
        org.junit.Assert.assertTrue("'" + b4 + "' != '" + false + "'", b4.equals(false));
        org.junit.Assert.assertTrue("'" + b5 + "' != '" + false + "'", b5.equals(false));
    }

    @Test
    public void test021() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test021");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setCcNum("hi!");
        String str9 = payment1.getCcNum();
        Long long10 = payment1.getFee();
        payment1.setCcNum("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = payment1.getReservation();
        payment1.setCcName("");
        payment1.setCcName("");
        Long long18 = payment1.getFee();
        String str19 = payment1.getCcExpDate();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "hi!" + "'", str9.equals("hi!"));
        org.junit.Assert.assertTrue("'" + long10 + "' != '" + 1L + "'", long10.equals(1L));
        org.junit.Assert.assertNull(reservation13);
        org.junit.Assert.assertTrue("'" + long18 + "' != '" + 1L + "'", long18.equals(1L));
        org.junit.Assert.assertTrue("'" + str19 + "' != '" + "" + "'", str19.equals(""));
    }

    @Test
    public void test022() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test022");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        payment1.setCcNum("");
        Boolean b4 = payment1.isValid();
        Long long5 = payment1.getFee();
        payment1.setCcName("");
        String str8 = payment1.getCcName();
        org.junit.Assert.assertTrue("'" + b4 + "' != '" + false + "'", b4.equals(false));
        org.junit.Assert.assertTrue("'" + long5 + "' != '" + 1L + "'", long5.equals(1L));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
    }

    @Test
    public void test023() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test023");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        payment0.setCcExpDate("");
        payment0.setCcNum("hi!");
        payment0.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = payment0.getReservation();
        payment0.setCcNum("");
        String str16 = payment0.getCcName();
        Long long17 = payment0.getFee();
        payment0.setCcNum("");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "" + "'", str6.equals(""));
        org.junit.Assert.assertNull(reservation13);
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "" + "'", str16.equals(""));
        org.junit.Assert.assertNull(long17);
    }

    @Test
    public void test024() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test024");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        payment0.setCcName("");
        payment0.setCcName("");
        payment0.setCcExpDate("hi!");
        String str8 = payment0.getCcName();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment0.setReservation(reservation9);
        Boolean b11 = payment0.isValid();
        String str12 = payment0.getCcExpDate();
        org.junit.Assert.assertNull(long1);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertTrue("'" + b11 + "' != '" + false + "'", b11.equals(false));
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "hi!" + "'", str12.equals("hi!"));
    }

    @Test
    public void test025() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test025");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        String str8 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment0.setReservation(reservation9);
        payment0.setCcName("hi!");
        payment0.setCcName("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation15 = payment0.getReservation();
        String str16 = payment0.getCcExpDate();
        Boolean b17 = payment0.isValid();
        payment0.setFee((Long) 1L);
        String str20 = payment0.getCcName();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertNull(reservation15);
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "" + "'", str16.equals(""));
        org.junit.Assert.assertTrue("'" + b17 + "' != '" + true + "'", b17.equals(true));
        org.junit.Assert.assertTrue("'" + str20 + "' != '" + "hi!" + "'", str20.equals("hi!"));
    }

    @Test
    public void test026() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test026");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        payment1.setFee((Long) 10L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = payment1.getReservation();
        Boolean b7 = payment1.isValid();
        payment1.setCcName("");
        payment1.setCcNum("hi!");
        Boolean b12 = payment1.isValid();
        Boolean b13 = payment1.isValid();
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + long3 + "' != '" + 1L + "'", long3.equals(1L));
        org.junit.Assert.assertNull(reservation6);
        org.junit.Assert.assertTrue("'" + b7 + "' != '" + false + "'", b7.equals(false));
        org.junit.Assert.assertTrue("'" + b12 + "' != '" + false + "'", b12.equals(false));
        org.junit.Assert.assertTrue("'" + b13 + "' != '" + false + "'", b13.equals(false));
    }

    @Test
    public void test027() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test027");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) (-1L));
        payment1.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = payment1.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = null;
        payment1.setReservation(reservation5);
        org.junit.Assert.assertNull(reservation4);
    }

    @Test
    public void test028() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test028");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation10 = null;
        payment0.setReservation(reservation10);
        Long long12 = payment0.getFee();
        String str13 = payment0.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation14 = null;
        payment0.setReservation(reservation14);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation16 = null;
        payment0.setReservation(reservation16);
        Long long18 = payment0.getFee();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertNull(long12);
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "" + "'", str13.equals(""));
        org.junit.Assert.assertNull(long18);
    }

    @Test
    public void test029() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test029");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) (-1L));
        payment1.setFee((Long) 0L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = null;
        payment1.setReservation(reservation4);
        payment1.setCcName("");
        payment1.setCcNum("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation10 = payment1.getReservation();
        org.junit.Assert.assertNull(reservation10);
    }

    @Test
    public void test030() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test030");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcExpDate("hi!");
        Boolean b11 = payment1.isValid();
        String str12 = payment1.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = null;
        payment1.setReservation(reservation13);
        String str15 = payment1.getCcExpDate();
        Boolean b16 = payment1.isValid();
        payment1.setCcExpDate("");
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b11 + "' != '" + false + "'", b11.equals(false));
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "hi!" + "'", str12.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "hi!" + "'", str15.equals("hi!"));
        org.junit.Assert.assertTrue("'" + b16 + "' != '" + false + "'", b16.equals(false));
    }

    @Test
    public void test031() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test031");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcExpDate("hi!");
        payment1.setCcNum("hi!");
        org.junit.Assert.assertNull(reservation2);
    }

    @Test
    public void test032() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test032");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        Boolean b2 = payment0.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = payment0.getReservation();
        Long long4 = payment0.getFee();
        payment0.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = null;
        payment0.setReservation(reservation7);
        payment0.setCcExpDate("hi!");
        org.junit.Assert.assertNull(long1);
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertNull(reservation3);
        org.junit.Assert.assertNull(long4);
    }

    @Test
    public void test033() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test033");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcExpDate("");
        String str4 = payment1.getCcExpDate();
        org.junit.Assert.assertTrue("'" + str4 + "' != '" + "" + "'", str4.equals(""));
    }

    @Test
    public void test034() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test034");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        Long long8 = payment0.getFee();
        Boolean b9 = payment0.isValid();
        payment0.setFee((Long) 100L);
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertNull(long8);
        org.junit.Assert.assertTrue("'" + b9 + "' != '" + false + "'", b9.equals(false));
    }

    @Test
    public void test035() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test035");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        payment1.setCcNum("hi!");
        Long long13 = payment1.getFee();
        payment1.setCcExpDate("");
        String str16 = payment1.getCcNum();
        Long long17 = payment1.getFee();
        payment1.setFee((Long) 100L);
        Long long20 = payment1.getFee();
        payment1.setFee((Long) 1L);
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long13 + "' != '" + 100L + "'", long13.equals(100L));
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "hi!" + "'", str16.equals("hi!"));
        org.junit.Assert.assertTrue("'" + long17 + "' != '" + 100L + "'", long17.equals(100L));
        org.junit.Assert.assertTrue("'" + long20 + "' != '" + 100L + "'", long20.equals(100L));
    }

    @Test
    public void test036() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test036");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        Long long2 = payment1.getFee();
        Boolean b3 = payment1.isValid();
        org.junit.Assert.assertTrue("'" + long2 + "' != '" + 10L + "'", long2.equals(10L));
        org.junit.Assert.assertTrue("'" + b3 + "' != '" + false + "'", b3.equals(false));
    }

    @Test
    public void test037() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test037");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        payment1.setCcNum("hi!");
        Long long13 = payment1.getFee();
        payment1.setCcExpDate("");
        Long long16 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation17 = null;
        payment1.setReservation(reservation17);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation19 = payment1.getReservation();
        String str20 = payment1.getCcNum();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long13 + "' != '" + 100L + "'", long13.equals(100L));
        org.junit.Assert.assertTrue("'" + long16 + "' != '" + 100L + "'", long16.equals(100L));
        org.junit.Assert.assertNull(reservation19);
        org.junit.Assert.assertTrue("'" + str20 + "' != '" + "hi!" + "'", str20.equals("hi!"));
    }

    @Test
    public void test038() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test038");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = null;
        payment1.setReservation(reservation5);
        Long long7 = payment1.getFee();
        Long long8 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = payment1.getReservation();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 1L + "'", long7.equals(1L));
        org.junit.Assert.assertTrue("'" + long8 + "' != '" + 1L + "'", long8.equals(1L));
        org.junit.Assert.assertNull(reservation9);
    }

    @Test
    public void test039() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test039");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        payment1.setCcNum("hi!");
        Long long13 = payment1.getFee();
        payment1.setCcExpDate("");
        String str16 = payment1.getCcNum();
        Boolean b17 = payment1.isValid();
        String str18 = payment1.getCcName();
        payment1.setFee((Long) 0L);
        String str21 = payment1.getCcName();
        payment1.setCcNum("");
        payment1.setCcExpDate("");
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long13 + "' != '" + 100L + "'", long13.equals(100L));
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "hi!" + "'", str16.equals("hi!"));
        org.junit.Assert.assertTrue("'" + b17 + "' != '" + true + "'", b17.equals(true));
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "hi!" + "'", str18.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str21 + "' != '" + "hi!" + "'", str21.equals("hi!"));
    }

    @Test
    public void test040() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test040");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcName("");
        payment0.setCcExpDate("");
        Boolean b8 = payment0.isValid();
        Long long9 = payment0.getFee();
        payment0.setCcExpDate("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation12 = payment0.getReservation();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + b8 + "' != '" + true + "'", b8.equals(true));
        org.junit.Assert.assertNull(long9);
        org.junit.Assert.assertNull(reservation12);
    }

    @Test
    public void test041() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test041");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment0.getReservation();
        payment0.setCcExpDate("hi!");
        Long long5 = payment0.getFee();
        Long long6 = payment0.getFee();
        payment0.setCcName("");
        payment0.setCcExpDate("hi!");
        Boolean b11 = payment0.isValid();
        org.junit.Assert.assertNull(long1);
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertNull(long5);
        org.junit.Assert.assertNull(long6);
        org.junit.Assert.assertTrue("'" + b11 + "' != '" + false + "'", b11.equals(false));
    }

    @Test
    public void test042() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test042");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        String str5 = payment1.getCcExpDate();
        payment1.setCcExpDate("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = null;
        payment1.setReservation(reservation8);
        String str10 = payment1.getCcExpDate();
        payment1.setFee((Long) 100L);
        String str13 = payment1.getCcExpDate();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "" + "'", str5.equals(""));
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "hi!" + "'", str10.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "hi!" + "'", str13.equals("hi!"));
    }

    @Test
    public void test043() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test043");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = null;
        payment1.setReservation(reservation3);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = null;
        payment1.setReservation(reservation5);
        Long long7 = payment1.getFee();
        payment1.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation10 = null;
        payment1.setReservation(reservation10);
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 1L + "'", long7.equals(1L));
    }

    @Test
    public void test044() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test044");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcNum("hi!");
        payment1.setCcName("hi!");
        Long long7 = payment1.getFee();
        payment1.setCcName("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation10 = payment1.getReservation();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 1L + "'", long7.equals(1L));
        org.junit.Assert.assertNull(reservation10);
    }

    @Test
    public void test045() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test045");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        Boolean b5 = payment1.isValid();
        payment1.setFee((Long) 0L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment1.getReservation();
        payment1.setFee((Long) (-1L));
        payment1.setCcName("hi!");
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b5 + "' != '" + false + "'", b5.equals(false));
        org.junit.Assert.assertNull(reservation8);
    }

    @Test
    public void test046() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test046");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        String str5 = payment1.getCcExpDate();
        payment1.setFee((Long) (-1L));
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment1.getReservation();
        payment1.setCcName("");
        String str11 = payment1.getCcExpDate();
        String str12 = payment1.getCcExpDate();
        Boolean b13 = payment1.isValid();
        payment1.setCcName("");
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "" + "'", str5.equals(""));
        org.junit.Assert.assertNull(reservation8);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "" + "'", str11.equals(""));
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "" + "'", str12.equals(""));
        org.junit.Assert.assertTrue("'" + b13 + "' != '" + false + "'", b13.equals(false));
    }

    @Test
    public void test047() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test047");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        Long long7 = payment1.getFee();
        payment1.setFee((Long) (-1L));
        String str10 = payment1.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation11 = payment1.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation12 = payment1.getReservation();
        Boolean b13 = payment1.isValid();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 1L + "'", long7.equals(1L));
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertNull(reservation11);
        org.junit.Assert.assertNull(reservation12);
        org.junit.Assert.assertTrue("'" + b13 + "' != '" + false + "'", b13.equals(false));
    }

    @Test
    public void test048() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test048");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        String str8 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment0.setReservation(reservation9);
        payment0.setCcName("hi!");
        String str13 = payment0.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation14 = null;
        payment0.setReservation(reservation14);
        String str16 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation17 = payment0.getReservation();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "" + "'", str13.equals(""));
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "" + "'", str16.equals(""));
        org.junit.Assert.assertNull(reservation17);
    }

    @Test
    public void test049() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test049");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        Boolean b4 = payment1.isValid();
        payment1.setCcExpDate("hi!");
        Long long7 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = null;
        payment1.setReservation(reservation8);
        String str10 = payment1.getCcExpDate();
        Boolean b11 = payment1.isValid();
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + long3 + "' != '" + 1L + "'", long3.equals(1L));
        org.junit.Assert.assertTrue("'" + b4 + "' != '" + false + "'", b4.equals(false));
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 1L + "'", long7.equals(1L));
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "hi!" + "'", str10.equals("hi!"));
        org.junit.Assert.assertTrue("'" + b11 + "' != '" + false + "'", b11.equals(false));
    }

    @Test
    public void test050() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test050");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcExpDate("hi!");
        Boolean b11 = payment1.isValid();
        String str12 = payment1.getCcNum();
        Long long13 = payment1.getFee();
        Boolean b14 = payment1.isValid();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b11 + "' != '" + false + "'", b11.equals(false));
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "hi!" + "'", str12.equals("hi!"));
        org.junit.Assert.assertTrue("'" + long13 + "' != '" + 100L + "'", long13.equals(100L));
        org.junit.Assert.assertTrue("'" + b14 + "' != '" + false + "'", b14.equals(false));
    }

    @Test
    public void test051() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test051");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = null;
        payment1.setReservation(reservation7);
        String str9 = payment1.getCcExpDate();
        payment1.setFee((Long) 100L);
        Long long12 = payment1.getFee();
        Long long13 = payment1.getFee();
        payment1.setCcExpDate("hi!");
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "" + "'", str9.equals(""));
        org.junit.Assert.assertTrue("'" + long12 + "' != '" + 100L + "'", long12.equals(100L));
        org.junit.Assert.assertTrue("'" + long13 + "' != '" + 100L + "'", long13.equals(100L));
    }

    @Test
    public void test052() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test052");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcNum("hi!");
        payment1.setCcName("hi!");
        String str7 = payment1.getCcNum();
        String str8 = payment1.getCcNum();
        Long long9 = payment1.getFee();
        payment1.setCcNum("hi!");
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "hi!" + "'", str7.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "hi!" + "'", str8.equals("hi!"));
        org.junit.Assert.assertTrue("'" + long9 + "' != '" + 1L + "'", long9.equals(1L));
    }

    @Test
    public void test053() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test053");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        String str5 = payment1.getCcExpDate();
        payment1.setCcExpDate("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = null;
        payment1.setReservation(reservation8);
        String str10 = payment1.getCcExpDate();
        String str11 = payment1.getCcExpDate();
        payment1.setCcName("hi!");
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "" + "'", str5.equals(""));
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "hi!" + "'", str10.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "hi!" + "'", str11.equals("hi!"));
    }

    @Test
    public void test054() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test054");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        Boolean b2 = payment0.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = payment0.getReservation();
        payment0.setCcExpDate("hi!");
        Boolean b7 = payment0.isValid();
        String str8 = payment0.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment0.setReservation(reservation9);
        org.junit.Assert.assertNull(long1);
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertNull(reservation3);
        org.junit.Assert.assertNull(reservation4);
        org.junit.Assert.assertTrue("'" + b7 + "' != '" + false + "'", b7.equals(false));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "hi!" + "'", str8.equals("hi!"));
    }

    @Test
    public void test055() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test055");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        String str8 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment0.setReservation(reservation9);
        payment0.setCcName("hi!");
        payment0.setCcName("hi!");
        Long long15 = payment0.getFee();
        payment0.setCcExpDate("hi!");
        payment0.setFee((Long) 0L);
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertNull(long15);
    }

    @Test
    public void test056() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test056");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = null;
        payment1.setReservation(reservation5);
        Long long7 = payment1.getFee();
        payment1.setCcName("hi!");
        String str10 = payment1.getCcExpDate();
        Long long11 = payment1.getFee();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 1L + "'", long7.equals(1L));
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertTrue("'" + long11 + "' != '" + 1L + "'", long11.equals(1L));
    }

    @Test
    public void test057() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test057");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        payment1.setCcNum("");
        Boolean b4 = payment1.isValid();
        Long long5 = payment1.getFee();
        payment1.setCcExpDate("");
        org.junit.Assert.assertTrue("'" + b4 + "' != '" + false + "'", b4.equals(false));
        org.junit.Assert.assertTrue("'" + long5 + "' != '" + 1L + "'", long5.equals(1L));
    }

    @Test
    public void test058() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test058");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        String str8 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment0.setReservation(reservation9);
        payment0.setCcName("hi!");
        payment0.setCcName("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation15 = payment0.getReservation();
        String str16 = payment0.getCcExpDate();
        Boolean b17 = payment0.isValid();
        String str18 = payment0.getCcNum();
        String str19 = payment0.getCcNum();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertNull(reservation15);
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "" + "'", str16.equals(""));
        org.junit.Assert.assertTrue("'" + b17 + "' != '" + true + "'", b17.equals(true));
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "" + "'", str18.equals(""));
        org.junit.Assert.assertTrue("'" + str19 + "' != '" + "" + "'", str19.equals(""));
    }

    @Test
    public void test059() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test059");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        Boolean b2 = payment0.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = payment0.getReservation();
        Long long4 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = payment0.getReservation();
        payment0.setCcExpDate("");
        payment0.setCcName("");
        Long long10 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation11 = null;
        payment0.setReservation(reservation11);
        org.junit.Assert.assertNull(long1);
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertNull(reservation3);
        org.junit.Assert.assertNull(long4);
        org.junit.Assert.assertNull(reservation5);
        org.junit.Assert.assertNull(long10);
    }

    @Test
    public void test060() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test060");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        payment1.setCcNum("");
        payment1.setCcExpDate("hi!");
        payment1.setCcNum("hi!");
        Long long8 = payment1.getFee();
        org.junit.Assert.assertTrue("'" + long8 + "' != '" + 1L + "'", long8.equals(1L));
    }

    @Test
    public void test061() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test061");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcExpDate("hi!");
        Boolean b11 = payment1.isValid();
        String str12 = payment1.getCcExpDate();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b11 + "' != '" + false + "'", b11.equals(false));
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "hi!" + "'", str12.equals("hi!"));
    }

    @Test
    public void test062() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test062");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        String str12 = payment0.getCcName();
        String str13 = payment0.getCcNum();
        String str14 = payment0.getCcNum();
        String str15 = payment0.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation16 = null;
        payment0.setReservation(reservation16);
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "hi!" + "'", str12.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "" + "'", str13.equals(""));
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "" + "'", str14.equals(""));
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "" + "'", str15.equals(""));
    }

    @Test
    public void test063() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test063");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = null;
        payment1.setReservation(reservation7);
        String str9 = payment1.getCcExpDate();
        payment1.setFee((Long) 100L);
        Long long12 = payment1.getFee();
        String str13 = payment1.getCcExpDate();
        payment1.setCcName("hi!");
        Long long16 = payment1.getFee();
        Boolean b17 = payment1.isValid();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "" + "'", str9.equals(""));
        org.junit.Assert.assertTrue("'" + long12 + "' != '" + 100L + "'", long12.equals(100L));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "" + "'", str13.equals(""));
        org.junit.Assert.assertTrue("'" + long16 + "' != '" + 100L + "'", long16.equals(100L));
        org.junit.Assert.assertTrue("'" + b17 + "' != '" + true + "'", b17.equals(true));
    }

    @Test
    public void test064() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test064");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation1 = null;
        payment0.setReservation(reservation1);
        Long long3 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = payment0.getReservation();
        payment0.setCcExpDate("hi!");
        try {
            String str7 = payment0.getCcNum();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException");
        } catch (NullPointerException e) {
        }
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertNull(reservation4);
    }

    @Test
    public void test065() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test065");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        Boolean b5 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = null;
        payment1.setReservation(reservation6);
        payment1.setCcExpDate("");
        payment1.setCcNum("");
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b5 + "' != '" + false + "'", b5.equals(false));
    }

    @Test
    public void test066() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test066");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        Boolean b5 = payment1.isValid();
        payment1.setFee((Long) 0L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment1.getReservation();
        Boolean b9 = payment1.isValid();
        Boolean b10 = payment1.isValid();
        String str11 = payment1.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation12 = payment1.getReservation();
        Long long13 = payment1.getFee();
        payment1.setCcNum("hi!");
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b5 + "' != '" + false + "'", b5.equals(false));
        org.junit.Assert.assertNull(reservation8);
        org.junit.Assert.assertTrue("'" + b9 + "' != '" + false + "'", b9.equals(false));
        org.junit.Assert.assertTrue("'" + b10 + "' != '" + false + "'", b10.equals(false));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "" + "'", str11.equals(""));
        org.junit.Assert.assertNull(reservation12);
        org.junit.Assert.assertTrue("'" + long13 + "' != '" + 0L + "'", long13.equals(0L));
    }

    @Test
    public void test067() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test067");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        Boolean b2 = payment0.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = payment0.getReservation();
        payment0.setCcExpDate("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment0.getReservation();
        payment0.setCcName("");
        org.junit.Assert.assertNull(long1);
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertNull(reservation3);
        org.junit.Assert.assertNull(reservation4);
        org.junit.Assert.assertNull(reservation7);
    }

    @Test
    public void test068() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test068");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcNum("hi!");
        payment1.setCcName("hi!");
        Long long7 = payment1.getFee();
        String str8 = payment1.getCcNum();
        String str9 = payment1.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation10 = null;
        payment1.setReservation(reservation10);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation12 = null;
        payment1.setReservation(reservation12);
        String str14 = payment1.getCcName();
        String str15 = payment1.getCcNum();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 1L + "'", long7.equals(1L));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "hi!" + "'", str8.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "hi!" + "'", str9.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "hi!" + "'", str14.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "hi!" + "'", str15.equals("hi!"));
    }

    @Test
    public void test069() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test069");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        Long long2 = payment1.getFee();
        Long long3 = payment1.getFee();
        org.junit.Assert.assertTrue("'" + long2 + "' != '" + 10L + "'", long2.equals(10L));
        org.junit.Assert.assertTrue("'" + long3 + "' != '" + 10L + "'", long3.equals(10L));
    }

    @Test
    public void test070() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test070");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcName("");
        Boolean b7 = payment1.isValid();
        Long long8 = payment1.getFee();
        Long long9 = payment1.getFee();
        payment1.setFee((Long) 0L);
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b7 + "' != '" + false + "'", b7.equals(false));
        org.junit.Assert.assertTrue("'" + long8 + "' != '" + 1L + "'", long8.equals(1L));
        org.junit.Assert.assertTrue("'" + long9 + "' != '" + 1L + "'", long9.equals(1L));
    }

    @Test
    public void test071() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test071");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        payment1.setCcNum("hi!");
        Long long13 = payment1.getFee();
        payment1.setCcExpDate("");
        Long long16 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation17 = null;
        payment1.setReservation(reservation17);
        String str19 = payment1.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation20 = null;
        payment1.setReservation(reservation20);
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long13 + "' != '" + 100L + "'", long13.equals(100L));
        org.junit.Assert.assertTrue("'" + long16 + "' != '" + 100L + "'", long16.equals(100L));
        org.junit.Assert.assertTrue("'" + str19 + "' != '" + "hi!" + "'", str19.equals("hi!"));
    }

    @Test
    public void test072() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test072");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment0.getReservation();
        payment0.setCcExpDate("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = null;
        payment0.setReservation(reservation5);
        payment0.setCcNum("");
        try {
            String str9 = payment0.getCcName();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException");
        } catch (NullPointerException e) {
        }
        org.junit.Assert.assertNull(long1);
        org.junit.Assert.assertNull(reservation2);
    }

    @Test
    public void test073() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test073");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Boolean b3 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = payment1.getReservation();
        payment1.setCcName("hi!");
        Long long7 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = null;
        payment1.setReservation(reservation8);
        Long long10 = payment1.getFee();
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + b3 + "' != '" + false + "'", b3.equals(false));
        org.junit.Assert.assertNull(reservation4);
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 1L + "'", long7.equals(1L));
        org.junit.Assert.assertTrue("'" + long10 + "' != '" + 1L + "'", long10.equals(1L));
    }

    @Test
    public void test074() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test074");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = null;
        payment1.setReservation(reservation4);
        payment1.setFee((Long) 0L);
        payment1.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation10 = payment1.getReservation();
        org.junit.Assert.assertNull(reservation10);
    }

    @Test
    public void test075() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test075");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) (-1L));
        payment1.setFee((Long) 0L);
        payment1.setCcExpDate("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = payment1.getReservation();
        payment1.setCcNum("");
        org.junit.Assert.assertNull(reservation6);
    }

    @Test
    public void test076() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test076");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        payment1.setCcNum("hi!");
        Long long13 = payment1.getFee();
        payment1.setCcExpDate("");
        Long long16 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation17 = null;
        payment1.setReservation(reservation17);
        String str19 = payment1.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation20 = payment1.getReservation();
        Boolean b21 = payment1.isValid();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long13 + "' != '" + 100L + "'", long13.equals(100L));
        org.junit.Assert.assertTrue("'" + long16 + "' != '" + 100L + "'", long16.equals(100L));
        org.junit.Assert.assertTrue("'" + str19 + "' != '" + "hi!" + "'", str19.equals("hi!"));
        org.junit.Assert.assertNull(reservation20);
        org.junit.Assert.assertTrue("'" + b21 + "' != '" + true + "'", b21.equals(true));
    }

    @Test
    public void test077() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test077");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        String str10 = payment0.getCcExpDate();
        payment0.setCcExpDate("");
        payment0.setFee((Long) 0L);
        payment0.setCcExpDate("");
        payment0.setFee((Long) 10L);
        payment0.setFee((Long) 100L);
        String str21 = payment0.getCcExpDate();
        String str22 = payment0.getCcNum();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertTrue("'" + str21 + "' != '" + "" + "'", str21.equals(""));
        org.junit.Assert.assertTrue("'" + str22 + "' != '" + "" + "'", str22.equals(""));
    }

    @Test
    public void test078() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test078");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        String str10 = payment0.getCcExpDate();
        String str11 = payment0.getCcNum();
        String str12 = payment0.getCcName();
        String str13 = payment0.getCcName();
        payment0.setCcExpDate("");
        Long long16 = payment0.getFee();
        payment0.setFee((Long) 1L);
        Long long19 = payment0.getFee();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "" + "'", str11.equals(""));
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "hi!" + "'", str12.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "hi!" + "'", str13.equals("hi!"));
        org.junit.Assert.assertNull(long16);
        org.junit.Assert.assertTrue("'" + long19 + "' != '" + 1L + "'", long19.equals(1L));
    }

    @Test
    public void test079() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test079");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcNum("hi!");
        payment1.setCcName("hi!");
        Long long7 = payment1.getFee();
        String str8 = payment1.getCcNum();
        String str9 = payment1.getCcNum();
        String str10 = payment1.getCcName();
        String str11 = payment1.getCcNum();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 1L + "'", long7.equals(1L));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "hi!" + "'", str8.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "hi!" + "'", str9.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "hi!" + "'", str10.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "hi!" + "'", str11.equals("hi!"));
    }

    @Test
    public void test080() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test080");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        payment1.setFee((Long) 10L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = payment1.getReservation();
        Boolean b7 = payment1.isValid();
        payment1.setCcExpDate("");
        payment1.setFee((Long) (-1L));
        Boolean b12 = payment1.isValid();
        payment1.setFee((Long) 0L);
        Boolean b15 = payment1.isValid();
        Boolean b16 = payment1.isValid();
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + long3 + "' != '" + 1L + "'", long3.equals(1L));
        org.junit.Assert.assertNull(reservation6);
        org.junit.Assert.assertTrue("'" + b7 + "' != '" + false + "'", b7.equals(false));
        org.junit.Assert.assertTrue("'" + b12 + "' != '" + false + "'", b12.equals(false));
        org.junit.Assert.assertTrue("'" + b15 + "' != '" + false + "'", b15.equals(false));
        org.junit.Assert.assertTrue("'" + b16 + "' != '" + false + "'", b16.equals(false));
    }

    @Test
    public void test081() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test081");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        payment0.setCcExpDate("");
        payment0.setCcNum("hi!");
        Boolean b11 = payment0.isValid();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "" + "'", str6.equals(""));
        org.junit.Assert.assertTrue("'" + b11 + "' != '" + false + "'", b11.equals(false));
    }

    @Test
    public void test082() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test082");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment0.getReservation();
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        payment0.setCcNum("");
        Boolean b15 = payment0.isValid();
        payment0.setCcNum("hi!");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertNull(reservation8);
        org.junit.Assert.assertTrue("'" + b15 + "' != '" + true + "'", b15.equals(true));
    }

    @Test
    public void test083() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test083");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        String str8 = payment0.getCcNum();
        String str9 = payment0.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation10 = payment0.getReservation();
        String str11 = payment0.getCcExpDate();
        Boolean b12 = payment0.isValid();
        payment0.setCcName("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation15 = null;
        payment0.setReservation(reservation15);
        Boolean b17 = payment0.isValid();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "" + "'", str9.equals(""));
        org.junit.Assert.assertNull(reservation10);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "" + "'", str11.equals(""));
        org.junit.Assert.assertTrue("'" + b12 + "' != '" + false + "'", b12.equals(false));
        org.junit.Assert.assertTrue("'" + b17 + "' != '" + true + "'", b17.equals(true));
    }

    @Test
    public void test084() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test084");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) (-1L));
        payment1.setFee((Long) 0L);
        Boolean b4 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = null;
        payment1.setReservation(reservation5);
        payment1.setCcExpDate("hi!");
        org.junit.Assert.assertTrue("'" + b4 + "' != '" + false + "'", b4.equals(false));
    }

    @Test
    public void test085() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test085");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        String str8 = payment0.getCcNum();
        String str9 = payment0.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation10 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation11 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation12 = null;
        payment0.setReservation(reservation12);
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "" + "'", str9.equals(""));
        org.junit.Assert.assertNull(reservation10);
        org.junit.Assert.assertNull(reservation11);
    }

    @Test
    public void test086() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test086");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        payment1.setCcNum("");
        payment1.setCcExpDate("hi!");
        payment1.setCcNum("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment1.getReservation();
        payment1.setFee((Long) 1L);
        payment1.setCcName("");
        Long long13 = payment1.getFee();
        payment1.setCcNum("");
        org.junit.Assert.assertNull(reservation8);
        org.junit.Assert.assertTrue("'" + long13 + "' != '" + 1L + "'", long13.equals(1L));
    }

    @Test
    public void test087() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test087");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        payment0.setCcNum("hi!");
        payment0.setCcNum("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment0.getReservation();
        payment0.setFee((Long) 100L);
        org.junit.Assert.assertNull(reservation7);
        org.junit.Assert.assertNull(reservation8);
    }

    @Test
    public void test088() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test088");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcExpDate("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment1.getReservation();
        payment1.setCcExpDate("");
        String str10 = payment1.getCcExpDate();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertNull(reservation7);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
    }

    @Test
    public void test089() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test089");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        String str8 = payment0.getCcNum();
        String str9 = payment0.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation10 = payment0.getReservation();
        String str11 = payment0.getCcExpDate();
        Boolean b12 = payment0.isValid();
        payment0.setCcName("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation15 = null;
        payment0.setReservation(reservation15);
        payment0.setCcExpDate("hi!");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "" + "'", str9.equals(""));
        org.junit.Assert.assertNull(reservation10);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "" + "'", str11.equals(""));
        org.junit.Assert.assertTrue("'" + b12 + "' != '" + false + "'", b12.equals(false));
    }

    @Test
    public void test090() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test090");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = payment1.getReservation();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertNull(reservation5);
    }

    @Test
    public void test091() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test091");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        payment0.setCcName("hi!");
        Boolean b14 = payment0.isValid();
        Boolean b15 = payment0.isValid();
        Long long16 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation17 = null;
        payment0.setReservation(reservation17);
        payment0.setCcName("");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + b14 + "' != '" + true + "'", b14.equals(true));
        org.junit.Assert.assertTrue("'" + b15 + "' != '" + true + "'", b15.equals(true));
        org.junit.Assert.assertNull(long16);
    }

    @Test
    public void test092() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test092");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setCcNum("hi!");
        String str9 = payment1.getCcNum();
        Long long10 = payment1.getFee();
        payment1.setCcNum("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = payment1.getReservation();
        payment1.setCcNum("hi!");
        payment1.setFee((Long) (-1L));
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "hi!" + "'", str9.equals("hi!"));
        org.junit.Assert.assertTrue("'" + long10 + "' != '" + 1L + "'", long10.equals(1L));
        org.junit.Assert.assertNull(reservation13);
    }

    @Test
    public void test093() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test093");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        Long long7 = payment1.getFee();
        payment1.setFee((Long) (-1L));
        String str10 = payment1.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation11 = payment1.getReservation();
        String str12 = payment1.getCcExpDate();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 1L + "'", long7.equals(1L));
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertNull(reservation11);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "" + "'", str12.equals(""));
    }

    @Test
    public void test094() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test094");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment0.getReservation();
        payment0.setCcExpDate("hi!");
        String str11 = payment0.getCcExpDate();
        payment0.setCcNum("hi!");
        payment0.setCcName("");
        String str16 = payment0.getCcNum();
        Boolean b17 = payment0.isValid();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "" + "'", str6.equals(""));
        org.junit.Assert.assertNull(reservation7);
        org.junit.Assert.assertNull(reservation8);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "hi!" + "'", str11.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "hi!" + "'", str16.equals("hi!"));
        org.junit.Assert.assertTrue("'" + b17 + "' != '" + true + "'", b17.equals(true));
    }

    @Test
    public void test095() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test095");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        String str10 = payment0.getCcExpDate();
        payment0.setCcExpDate("");
        payment0.setFee((Long) 0L);
        payment0.setCcExpDate("");
        String str17 = payment0.getCcName();
        payment0.setCcNum("");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "hi!" + "'", str17.equals("hi!"));
    }

    @Test
    public void test096() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test096");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        Boolean b5 = payment1.isValid();
        payment1.setFee((Long) 0L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment1.getReservation();
        payment1.setFee((Long) (-1L));
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation11 = null;
        payment1.setReservation(reservation11);
        String str13 = payment1.getCcExpDate();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b5 + "' != '" + false + "'", b5.equals(false));
        org.junit.Assert.assertNull(reservation8);
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "" + "'", str13.equals(""));
    }

    @Test
    public void test097() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test097");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        String str10 = payment0.getCcExpDate();
        payment0.setCcExpDate("");
        String str13 = payment0.getCcNum();
        String str14 = payment0.getCcName();
        String str15 = payment0.getCcExpDate();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "" + "'", str13.equals(""));
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "hi!" + "'", str14.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "" + "'", str15.equals(""));
    }

    @Test
    public void test098() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test098");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcNum("hi!");
        payment1.setCcName("hi!");
        String str7 = payment1.getCcNum();
        payment1.setCcNum("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation10 = payment1.getReservation();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "hi!" + "'", str7.equals("hi!"));
        org.junit.Assert.assertNull(reservation10);
    }

    @Test
    public void test099() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test099");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        String str8 = payment0.getCcNum();
        String str9 = payment0.getCcExpDate();
        payment0.setFee((Long) 0L);
        String str12 = payment0.getCcNum();
        Long long13 = payment0.getFee();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "" + "'", str9.equals(""));
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "" + "'", str12.equals(""));
        org.junit.Assert.assertTrue("'" + long13 + "' != '" + 0L + "'", long13.equals(0L));
    }

    @Test
    public void test100() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test100");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcNum("hi!");
        payment1.setCcName("hi!");
        Long long7 = payment1.getFee();
        payment1.setCcName("hi!");
        String str10 = payment1.getCcName();
        payment1.setFee((Long) (-1L));
        payment1.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation15 = payment1.getReservation();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 1L + "'", long7.equals(1L));
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "hi!" + "'", str10.equals("hi!"));
        org.junit.Assert.assertNull(reservation15);
    }

    @Test
    public void test101() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test101");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        Long long4 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = payment1.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = null;
        payment1.setReservation(reservation6);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = null;
        payment1.setReservation(reservation8);
        try {
            String str10 = payment1.getCcExpDate();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException");
        } catch (NullPointerException e) {
        }
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + long3 + "' != '" + 1L + "'", long3.equals(1L));
        org.junit.Assert.assertTrue("'" + long4 + "' != '" + 1L + "'", long4.equals(1L));
        org.junit.Assert.assertNull(reservation5);
    }

    @Test
    public void test102() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test102");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = null;
        payment1.setReservation(reservation5);
        payment1.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment1.setReservation(reservation9);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation11 = payment1.getReservation();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertNull(reservation11);
    }

    @Test
    public void test103() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test103");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        payment1.setFee((Long) 10L);
        payment1.setCcNum("hi!");
        payment1.setCcNum("hi!");
        payment1.setCcExpDate("hi!");
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + long3 + "' != '" + 1L + "'", long3.equals(1L));
    }

    @Test
    public void test104() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test104");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcName("");
        payment1.setCcExpDate("");
        org.junit.Assert.assertNull(reservation2);
    }

    @Test
    public void test105() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test105");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Boolean b3 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = payment1.getReservation();
        payment1.setCcName("hi!");
        Boolean b7 = payment1.isValid();
        String str8 = payment1.getCcName();
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + b3 + "' != '" + false + "'", b3.equals(false));
        org.junit.Assert.assertNull(reservation4);
        org.junit.Assert.assertTrue("'" + b7 + "' != '" + false + "'", b7.equals(false));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "hi!" + "'", str8.equals("hi!"));
    }

    @Test
    public void test106() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test106");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        Boolean b2 = payment0.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = payment0.getReservation();
        Long long4 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = payment0.getReservation();
        payment0.setCcExpDate("");
        payment0.setCcName("");
        Boolean b10 = payment0.isValid();
        payment0.setCcExpDate("hi!");
        payment0.setCcName("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation15 = null;
        payment0.setReservation(reservation15);
        String str17 = payment0.getCcExpDate();
        org.junit.Assert.assertNull(long1);
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertNull(reservation3);
        org.junit.Assert.assertNull(long4);
        org.junit.Assert.assertNull(reservation5);
        org.junit.Assert.assertTrue("'" + b10 + "' != '" + false + "'", b10.equals(false));
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "hi!" + "'", str17.equals("hi!"));
    }

    @Test
    public void test107() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test107");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setCcNum("hi!");
        String str9 = payment1.getCcNum();
        Long long10 = payment1.getFee();
        payment1.setCcNum("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = null;
        payment1.setReservation(reservation13);
        payment1.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation17 = payment1.getReservation();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "hi!" + "'", str9.equals("hi!"));
        org.junit.Assert.assertTrue("'" + long10 + "' != '" + 1L + "'", long10.equals(1L));
        org.junit.Assert.assertNull(reservation17);
    }

    @Test
    public void test108() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test108");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = null;
        payment1.setReservation(reservation4);
        payment1.setFee((Long) 0L);
        String str8 = payment1.getCcName();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = payment1.getReservation();
        payment1.setCcName("");
        payment1.setCcName("");
        String str14 = payment1.getCcName();
        try {
            String str15 = payment1.getCcNum();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException");
        } catch (NullPointerException e) {
        }
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertNull(reservation9);
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "" + "'", str14.equals(""));
    }

    @Test
    public void test109() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test109");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        Boolean b2 = payment0.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = payment0.getReservation();
        Long long4 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = payment0.getReservation();
        payment0.setCcExpDate("");
        payment0.setCcName("");
        Long long10 = payment0.getFee();
        String str11 = payment0.getCcExpDate();
        org.junit.Assert.assertNull(long1);
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertNull(reservation3);
        org.junit.Assert.assertNull(long4);
        org.junit.Assert.assertNull(reservation5);
        org.junit.Assert.assertNull(long10);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "" + "'", str11.equals(""));
    }

    @Test
    public void test110() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test110");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        Boolean b5 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = null;
        payment1.setReservation(reservation6);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment1.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = payment1.getReservation();
        Long long10 = payment1.getFee();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b5 + "' != '" + false + "'", b5.equals(false));
        org.junit.Assert.assertNull(reservation8);
        org.junit.Assert.assertNull(reservation9);
        org.junit.Assert.assertTrue("'" + long10 + "' != '" + 1L + "'", long10.equals(1L));
    }

    @Test
    public void test111() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test111");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        String str5 = payment1.getCcExpDate();
        payment1.setCcExpDate("hi!");
        payment1.setCcExpDate("");
        Long long10 = payment1.getFee();
        payment1.setFee((Long) 100L);
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "" + "'", str5.equals(""));
        org.junit.Assert.assertTrue("'" + long10 + "' != '" + 1L + "'", long10.equals(1L));
    }

    @Test
    public void test112() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test112");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = null;
        payment1.setReservation(reservation3);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = null;
        payment1.setReservation(reservation5);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment1.getReservation();
        Long long8 = payment1.getFee();
        payment1.setCcName("");
        Long long11 = payment1.getFee();
        String str12 = payment1.getCcName();
        payment1.setFee((Long) 1L);
        try {
            String str15 = payment1.getCcNum();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException");
        } catch (NullPointerException e) {
        }
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertNull(reservation7);
        org.junit.Assert.assertTrue("'" + long8 + "' != '" + 1L + "'", long8.equals(1L));
        org.junit.Assert.assertTrue("'" + long11 + "' != '" + 1L + "'", long11.equals(1L));
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "" + "'", str12.equals(""));
    }

    @Test
    public void test113() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test113");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcNum("hi!");
        payment1.setCcName("hi!");
        String str7 = payment1.getCcNum();
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 0L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation12 = payment1.getReservation();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "hi!" + "'", str7.equals("hi!"));
        org.junit.Assert.assertNull(reservation12);
    }

    @Test
    public void test114() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test114");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcName("");
        payment0.setCcExpDate("");
        payment0.setCcExpDate("");
        String str10 = payment0.getCcExpDate();
        String str11 = payment0.getCcExpDate();
        String str12 = payment0.getCcExpDate();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "" + "'", str11.equals(""));
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "" + "'", str12.equals(""));
    }

    @Test
    public void test115() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test115");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        payment0.setCcExpDate("");
        payment0.setCcExpDate("hi!");
        payment0.setFee((Long) 0L);
        org.junit.Assert.assertNull(long1);
    }

    @Test
    public void test116() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test116");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        Long long7 = payment1.getFee();
        payment1.setCcNum("");
        Long long10 = payment1.getFee();
        String str11 = payment1.getCcExpDate();
        payment1.setCcNum("");
        payment1.setCcName("hi!");
        String str16 = payment1.getCcNum();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 1L + "'", long7.equals(1L));
        org.junit.Assert.assertTrue("'" + long10 + "' != '" + 1L + "'", long10.equals(1L));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "" + "'", str11.equals(""));
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "" + "'", str16.equals(""));
    }

    @Test
    public void test117() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test117");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment0.getReservation();
        Long long8 = payment0.getFee();
        Long long9 = payment0.getFee();
        payment0.setCcNum("hi!");
        payment0.setCcNum("");
        String str14 = payment0.getCcNum();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "" + "'", str6.equals(""));
        org.junit.Assert.assertNull(reservation7);
        org.junit.Assert.assertNull(long8);
        org.junit.Assert.assertNull(long9);
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "" + "'", str14.equals(""));
    }

    @Test
    public void test118() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test118");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        String str8 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment0.setReservation(reservation9);
        Long long11 = payment0.getFee();
        String str12 = payment0.getCcExpDate();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertNull(long11);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "" + "'", str12.equals(""));
    }

    @Test
    public void test119() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test119");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        payment1.setCcNum("");
        payment1.setCcExpDate("hi!");
        payment1.setCcNum("hi!");
        String str8 = payment1.getCcExpDate();
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "hi!" + "'", str8.equals("hi!"));
    }

    @Test
    public void test120() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test120");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment1.setReservation(reservation9);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation11 = payment1.getReservation();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertNull(reservation11);
    }

    @Test
    public void test121() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test121");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcNum("hi!");
        payment1.setCcName("hi!");
        Long long7 = payment1.getFee();
        String str8 = payment1.getCcNum();
        String str9 = payment1.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation10 = null;
        payment1.setReservation(reservation10);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation12 = null;
        payment1.setReservation(reservation12);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation14 = null;
        payment1.setReservation(reservation14);
        Long long16 = payment1.getFee();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 1L + "'", long7.equals(1L));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "hi!" + "'", str8.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "hi!" + "'", str9.equals("hi!"));
        org.junit.Assert.assertTrue("'" + long16 + "' != '" + 1L + "'", long16.equals(1L));
    }

    @Test
    public void test122() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test122");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = null;
        payment1.setReservation(reservation4);
        payment1.setFee((Long) (-1L));
        Boolean b8 = payment1.isValid();
        payment1.setCcNum("hi!");
        payment1.setCcNum("");
        org.junit.Assert.assertTrue("'" + b8 + "' != '" + false + "'", b8.equals(false));
    }

    @Test
    public void test123() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test123");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        String str12 = payment0.getCcName();
        String str13 = payment0.getCcNum();
        String str14 = payment0.getCcNum();
        payment0.setCcName("");
        String str17 = payment0.getCcNum();
        String str18 = payment0.getCcNum();
        payment0.setFee((Long) 100L);
        payment0.setCcNum("hi!");
        Long long23 = payment0.getFee();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "hi!" + "'", str12.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "" + "'", str13.equals(""));
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "" + "'", str14.equals(""));
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "" + "'", str17.equals(""));
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "" + "'", str18.equals(""));
        org.junit.Assert.assertTrue("'" + long23 + "' != '" + 100L + "'", long23.equals(100L));
    }

    @Test
    public void test124() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test124");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        Boolean b2 = payment0.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = payment0.getReservation();
        Long long4 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = payment0.getReservation();
        payment0.setCcExpDate("");
        Long long8 = payment0.getFee();
        Long long9 = payment0.getFee();
        payment0.setFee((Long) 0L);
        org.junit.Assert.assertNull(long1);
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertNull(reservation3);
        org.junit.Assert.assertNull(long4);
        org.junit.Assert.assertNull(reservation5);
        org.junit.Assert.assertNull(long8);
        org.junit.Assert.assertNull(long9);
    }

    @Test
    public void test125() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test125");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        String str12 = payment0.getCcName();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = null;
        payment0.setReservation(reservation13);
        payment0.setCcNum("");
        String str17 = payment0.getCcNum();
        String str18 = payment0.getCcExpDate();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "hi!" + "'", str12.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "" + "'", str17.equals(""));
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "" + "'", str18.equals(""));
    }

    @Test
    public void test126() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test126");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) (-1L));
        payment1.setFee((Long) 0L);
        Boolean b4 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = null;
        payment1.setReservation(reservation5);
        payment1.setCcName("hi!");
        org.junit.Assert.assertTrue("'" + b4 + "' != '" + false + "'", b4.equals(false));
    }

    @Test
    public void test127() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test127");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setCcNum("hi!");
        String str9 = payment1.getCcNum();
        payment1.setCcNum("hi!");
        payment1.setCcNum("");
        String str14 = payment1.getCcExpDate();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "hi!" + "'", str9.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "" + "'", str14.equals(""));
    }

    @Test
    public void test128() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test128");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        payment1.setCcNum("");
        Boolean b4 = payment1.isValid();
        Long long5 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = null;
        payment1.setReservation(reservation6);
        org.junit.Assert.assertTrue("'" + b4 + "' != '" + false + "'", b4.equals(false));
        org.junit.Assert.assertTrue("'" + long5 + "' != '" + 1L + "'", long5.equals(1L));
    }

    @Test
    public void test129() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test129");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        Boolean b5 = payment0.isValid();
        payment0.setCcNum("hi!");
        org.junit.Assert.assertTrue("'" + b5 + "' != '" + false + "'", b5.equals(false));
    }

    @Test
    public void test130() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test130");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        Boolean b2 = payment0.isValid();
        Boolean b3 = payment0.isValid();
        payment0.setCcNum("hi!");
        payment0.setCcExpDate("");
        String str8 = payment0.getCcExpDate();
        org.junit.Assert.assertNull(long1);
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + b3 + "' != '" + false + "'", b3.equals(false));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
    }

    @Test
    public void test131() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test131");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        String str10 = payment0.getCcExpDate();
        payment0.setCcExpDate("");
        Long long13 = payment0.getFee();
        String str14 = payment0.getCcName();
        payment0.setCcName("");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertNull(long13);
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "hi!" + "'", str14.equals("hi!"));
    }

    @Test
    public void test132() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test132");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        payment1.setCcNum("hi!");
        Long long13 = payment1.getFee();
        payment1.setCcExpDate("");
        String str16 = payment1.getCcNum();
        Boolean b17 = payment1.isValid();
        String str18 = payment1.getCcName();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation19 = payment1.getReservation();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long13 + "' != '" + 100L + "'", long13.equals(100L));
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "hi!" + "'", str16.equals("hi!"));
        org.junit.Assert.assertTrue("'" + b17 + "' != '" + true + "'", b17.equals(true));
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "hi!" + "'", str18.equals("hi!"));
        org.junit.Assert.assertNull(reservation19);
    }

    @Test
    public void test133() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test133");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = null;
        payment1.setReservation(reservation5);
        Long long7 = payment1.getFee();
        Long long8 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment1.setReservation(reservation9);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation11 = null;
        payment1.setReservation(reservation11);
        payment1.setCcName("");
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 1L + "'", long7.equals(1L));
        org.junit.Assert.assertTrue("'" + long8 + "' != '" + 1L + "'", long8.equals(1L));
    }

    @Test
    public void test134() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test134");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        payment0.setCcName("");
        payment0.setCcName("");
        payment0.setCcExpDate("hi!");
        payment0.setCcNum("");
        payment0.setFee((Long) 10L);
        org.junit.Assert.assertNull(long1);
    }

    @Test
    public void test135() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test135");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        Long long4 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = payment1.getReservation();
        payment1.setCcName("");
        Long long8 = payment1.getFee();
        payment1.setCcName("hi!");
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + long3 + "' != '" + 1L + "'", long3.equals(1L));
        org.junit.Assert.assertTrue("'" + long4 + "' != '" + 1L + "'", long4.equals(1L));
        org.junit.Assert.assertNull(reservation5);
        org.junit.Assert.assertTrue("'" + long8 + "' != '" + 1L + "'", long8.equals(1L));
    }

    @Test
    public void test136() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test136");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        payment0.setCcNum("");
        payment0.setCcNum("hi!");
        org.junit.Assert.assertNull(long3);
    }

    @Test
    public void test137() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test137");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = null;
        payment1.setReservation(reservation5);
        Long long7 = payment1.getFee();
        payment1.setCcName("hi!");
        payment1.setCcNum("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation12 = payment1.getReservation();
        Boolean b13 = payment1.isValid();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 1L + "'", long7.equals(1L));
        org.junit.Assert.assertNull(reservation12);
        org.junit.Assert.assertTrue("'" + b13 + "' != '" + true + "'", b13.equals(true));
    }

    @Test
    public void test138() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test138");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        payment1.setCcNum("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = null;
        payment1.setReservation(reservation6);
        String str8 = payment1.getCcName();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = payment1.getReservation();
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertNull(reservation9);
    }

    @Test
    public void test139() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test139");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        payment1.setFee((Long) 10L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = payment1.getReservation();
        Boolean b7 = payment1.isValid();
        payment1.setCcName("");
        payment1.setCcNum("hi!");
        String str12 = payment1.getCcName();
        payment1.setCcNum("hi!");
        payment1.setCcName("hi!");
        payment1.setCcNum("");
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + long3 + "' != '" + 1L + "'", long3.equals(1L));
        org.junit.Assert.assertNull(reservation6);
        org.junit.Assert.assertTrue("'" + b7 + "' != '" + false + "'", b7.equals(false));
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "" + "'", str12.equals(""));
    }

    @Test
    public void test140() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test140");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        payment1.setCcNum("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = null;
        payment1.setReservation(reservation13);
        payment1.setCcName("hi!");
        payment1.setFee((Long) 0L);
        payment1.setCcNum("");
        payment1.setCcNum("");
        org.junit.Assert.assertNull(reservation2);
    }

    @Test
    public void test141() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test141");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        String str8 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment0.setReservation(reservation9);
        payment0.setCcName("hi!");
        payment0.setCcName("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation15 = payment0.getReservation();
        String str16 = payment0.getCcExpDate();
        Boolean b17 = payment0.isValid();
        String str18 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation19 = null;
        payment0.setReservation(reservation19);
        String str21 = payment0.getCcNum();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertNull(reservation15);
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "" + "'", str16.equals(""));
        org.junit.Assert.assertTrue("'" + b17 + "' != '" + true + "'", b17.equals(true));
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "" + "'", str18.equals(""));
        org.junit.Assert.assertTrue("'" + str21 + "' != '" + "" + "'", str21.equals(""));
    }

    @Test
    public void test142() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test142");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        Long long4 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = payment1.getReservation();
        payment1.setCcName("");
        Long long8 = payment1.getFee();
        payment1.setFee((Long) (-1L));
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation11 = payment1.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation12 = null;
        payment1.setReservation(reservation12);
        payment1.setCcExpDate("");
        String str16 = payment1.getCcExpDate();
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + long3 + "' != '" + 1L + "'", long3.equals(1L));
        org.junit.Assert.assertTrue("'" + long4 + "' != '" + 1L + "'", long4.equals(1L));
        org.junit.Assert.assertNull(reservation5);
        org.junit.Assert.assertTrue("'" + long8 + "' != '" + 1L + "'", long8.equals(1L));
        org.junit.Assert.assertNull(reservation11);
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "" + "'", str16.equals(""));
    }

    @Test
    public void test143() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test143");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = null;
        payment1.setReservation(reservation4);
        payment1.setFee((Long) (-1L));
        payment1.setFee((Long) 1L);
        payment1.setCcExpDate("hi!");
        payment1.setCcNum("");
    }

    @Test
    public void test144() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test144");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = null;
        payment1.setReservation(reservation4);
        payment1.setFee((Long) 0L);
        String str8 = payment1.getCcName();
        payment1.setCcName("hi!");
        payment1.setCcExpDate("hi!");
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
    }

    @Test
    public void test145() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test145");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment0.getReservation();
        payment0.setCcExpDate("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = null;
        payment0.setReservation(reservation5);
        Boolean b7 = payment0.isValid();
        payment0.setCcNum("hi!");
        org.junit.Assert.assertNull(long1);
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b7 + "' != '" + false + "'", b7.equals(false));
    }

    @Test
    public void test146() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test146");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        payment0.setCcExpDate("");
        payment0.setFee((Long) 10L);
        String str6 = payment0.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = null;
        payment0.setReservation(reservation7);
        org.junit.Assert.assertNull(long1);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "" + "'", str6.equals(""));
    }

    @Test
    public void test147() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test147");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        String str8 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment0.setReservation(reservation9);
        payment0.setCcName("hi!");
        String str13 = payment0.getCcExpDate();
        payment0.setCcExpDate("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation16 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation17 = payment0.getReservation();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "" + "'", str13.equals(""));
        org.junit.Assert.assertNull(reservation16);
        org.junit.Assert.assertNull(reservation17);
    }

    @Test
    public void test148() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test148");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        payment1.setCcNum("");
        payment1.setCcExpDate("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = null;
        payment1.setReservation(reservation6);
        Boolean b8 = payment1.isValid();
        String str9 = payment1.getCcExpDate();
        org.junit.Assert.assertTrue("'" + b8 + "' != '" + false + "'", b8.equals(false));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "hi!" + "'", str9.equals("hi!"));
    }

    @Test
    public void test149() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test149");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcNum("hi!");
        payment1.setCcName("hi!");
        Long long7 = payment1.getFee();
        String str8 = payment1.getCcNum();
        String str9 = payment1.getCcNum();
        String str10 = payment1.getCcName();
        payment1.setCcNum("");
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 1L + "'", long7.equals(1L));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "hi!" + "'", str8.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "hi!" + "'", str9.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "hi!" + "'", str10.equals("hi!"));
    }

    @Test
    public void test150() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test150");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        String str10 = payment0.getCcExpDate();
        payment0.setCcExpDate("");
        payment0.setFee((Long) 0L);
        payment0.setCcExpDate("");
        payment0.setFee((Long) 10L);
        String str19 = payment0.getCcNum();
        String str20 = payment0.getCcExpDate();
        payment0.setCcNum("hi!");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertTrue("'" + str19 + "' != '" + "" + "'", str19.equals(""));
        org.junit.Assert.assertTrue("'" + str20 + "' != '" + "" + "'", str20.equals(""));
    }

    @Test
    public void test151() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test151");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        payment1.setCcNum("hi!");
        Long long13 = payment1.getFee();
        payment1.setCcExpDate("");
        Long long16 = payment1.getFee();
        payment1.setFee((Long) (-1L));
        payment1.setCcName("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation21 = null;
        payment1.setReservation(reservation21);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation23 = null;
        payment1.setReservation(reservation23);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation25 = payment1.getReservation();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long13 + "' != '" + 100L + "'", long13.equals(100L));
        org.junit.Assert.assertTrue("'" + long16 + "' != '" + 100L + "'", long16.equals(100L));
        org.junit.Assert.assertNull(reservation25);
    }

    @Test
    public void test152() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test152");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        String str12 = payment0.getCcName();
        String str13 = payment0.getCcNum();
        Boolean b14 = payment0.isValid();
        payment0.setCcName("");
        String str17 = payment0.getCcName();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "hi!" + "'", str12.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "" + "'", str13.equals(""));
        org.junit.Assert.assertTrue("'" + b14 + "' != '" + true + "'", b14.equals(true));
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "" + "'", str17.equals(""));
    }

    @Test
    public void test153() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test153");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Long long2 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = null;
        payment1.setReservation(reservation3);
        try {
            String str5 = payment1.getCcExpDate();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException");
        } catch (NullPointerException e) {
        }
        org.junit.Assert.assertTrue("'" + long2 + "' != '" + 1L + "'", long2.equals(1L));
    }

    @Test
    public void test154() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test154");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        String str10 = payment0.getCcExpDate();
        payment0.setCcExpDate("");
        payment0.setFee((Long) 0L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation15 = null;
        payment0.setReservation(reservation15);
        payment0.setCcNum("");
        payment0.setCcNum("");
        payment0.setFee((Long) 100L);
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
    }

    @Test
    public void test155() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test155");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        payment0.setCcNum("");
        payment0.setCcNum("");
        org.junit.Assert.assertNull(long3);
    }

    @Test
    public void test156() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test156");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        payment1.setCcNum("hi!");
        payment1.setCcName("hi!");
        String str15 = payment1.getCcExpDate();
        String str16 = payment1.getCcName();
        String str17 = payment1.getCcExpDate();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "" + "'", str15.equals(""));
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "hi!" + "'", str16.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "" + "'", str17.equals(""));
    }

    @Test
    public void test157() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test157");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = null;
        payment1.setReservation(reservation5);
        Long long7 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = null;
        payment1.setReservation(reservation8);
        payment1.setFee((Long) 0L);
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 1L + "'", long7.equals(1L));
    }

    @Test
    public void test158() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test158");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        String str10 = payment0.getCcExpDate();
        payment0.setCcExpDate("");
        Long long13 = payment0.getFee();
        payment0.setFee((Long) 1L);
        payment0.setCcName("");
        payment0.setCcName("hi!");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertNull(long13);
    }

    @Test
    public void test159() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test159");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        String str12 = payment0.getCcName();
        Boolean b13 = payment0.isValid();
        payment0.setCcName("");
        String str16 = payment0.getCcExpDate();
        String str17 = payment0.getCcNum();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "hi!" + "'", str12.equals("hi!"));
        org.junit.Assert.assertTrue("'" + b13 + "' != '" + true + "'", b13.equals(true));
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "" + "'", str16.equals(""));
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "" + "'", str17.equals(""));
    }

    @Test
    public void test160() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test160");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        String str12 = payment0.getCcName();
        String str13 = payment0.getCcNum();
        String str14 = payment0.getCcNum();
        payment0.setCcExpDate("");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "hi!" + "'", str12.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "" + "'", str13.equals(""));
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "" + "'", str14.equals(""));
    }

    @Test
    public void test161() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test161");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        Boolean b7 = payment1.isValid();
        Boolean b8 = payment1.isValid();
        payment1.setCcName("hi!");
        payment1.setCcNum("");
        payment1.setCcNum("hi!");
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b7 + "' != '" + false + "'", b7.equals(false));
        org.junit.Assert.assertTrue("'" + b8 + "' != '" + false + "'", b8.equals(false));
    }

    @Test
    public void test162() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test162");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment0.getReservation();
        payment0.setCcExpDate("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = null;
        payment0.setReservation(reservation5);
        Boolean b7 = payment0.isValid();
        payment0.setFee((Long) (-1L));
        payment0.setCcExpDate("hi!");
        org.junit.Assert.assertNull(long1);
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b7 + "' != '" + false + "'", b7.equals(false));
    }

    @Test
    public void test163() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test163");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 0L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = null;
        payment1.setReservation(reservation2);
    }

    @Test
    public void test164() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test164");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcNum("hi!");
        payment1.setCcName("hi!");
        Long long7 = payment1.getFee();
        String str8 = payment1.getCcNum();
        String str9 = payment1.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation10 = null;
        payment1.setReservation(reservation10);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation12 = null;
        payment1.setReservation(reservation12);
        payment1.setCcExpDate("hi!");
        String str16 = payment1.getCcExpDate();
        String str17 = payment1.getCcExpDate();
        String str18 = payment1.getCcExpDate();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 1L + "'", long7.equals(1L));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "hi!" + "'", str8.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "hi!" + "'", str9.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "hi!" + "'", str16.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "hi!" + "'", str17.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "hi!" + "'", str18.equals("hi!"));
    }

    @Test
    public void test165() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test165");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        payment0.setCcExpDate("");
        Boolean b9 = payment0.isValid();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "" + "'", str6.equals(""));
        org.junit.Assert.assertTrue("'" + b9 + "' != '" + false + "'", b9.equals(false));
    }

    @Test
    public void test166() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test166");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        Boolean b4 = payment1.isValid();
        payment1.setCcExpDate("hi!");
        Long long7 = payment1.getFee();
        Boolean b8 = payment1.isValid();
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + long3 + "' != '" + 1L + "'", long3.equals(1L));
        org.junit.Assert.assertTrue("'" + b4 + "' != '" + false + "'", b4.equals(false));
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 1L + "'", long7.equals(1L));
        org.junit.Assert.assertTrue("'" + b8 + "' != '" + false + "'", b8.equals(false));
    }

    @Test
    public void test167() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test167");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        payment1.setCcNum("");
        payment1.setCcExpDate("hi!");
        Boolean b6 = payment1.isValid();
        String str7 = payment1.getCcNum();
        payment1.setFee((Long) 10L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation10 = payment1.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation11 = payment1.getReservation();
        org.junit.Assert.assertTrue("'" + b6 + "' != '" + false + "'", b6.equals(false));
        org.junit.Assert.assertTrue("'" + str7 + "' != '" + "" + "'", str7.equals(""));
        org.junit.Assert.assertNull(reservation10);
        org.junit.Assert.assertNull(reservation11);
    }

    @Test
    public void test168() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test168");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        payment1.setCcNum("hi!");
        payment1.setCcNum("");
        String str8 = payment1.getCcName();
        payment1.setCcNum("hi!");
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
    }

    @Test
    public void test169() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test169");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        payment0.setCcNum("hi!");
        payment0.setCcNum("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = payment0.getReservation();
        org.junit.Assert.assertNull(reservation7);
        org.junit.Assert.assertNull(reservation8);
        org.junit.Assert.assertNull(reservation9);
    }

    @Test
    public void test170() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test170");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Boolean b3 = payment1.isValid();
        Boolean b4 = payment1.isValid();
        payment1.setCcName("");
        payment1.setCcNum("");
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + b3 + "' != '" + false + "'", b3.equals(false));
        org.junit.Assert.assertTrue("'" + b4 + "' != '" + false + "'", b4.equals(false));
    }

    @Test
    public void test171() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test171");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        payment1.setCcNum("hi!");
        Long long13 = payment1.getFee();
        payment1.setCcExpDate("");
        String str16 = payment1.getCcNum();
        Long long17 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation18 = null;
        payment1.setReservation(reservation18);
        Long long20 = payment1.getFee();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long13 + "' != '" + 100L + "'", long13.equals(100L));
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "hi!" + "'", str16.equals("hi!"));
        org.junit.Assert.assertTrue("'" + long17 + "' != '" + 100L + "'", long17.equals(100L));
        org.junit.Assert.assertTrue("'" + long20 + "' != '" + 100L + "'", long20.equals(100L));
    }

    @Test
    public void test172() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test172");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = null;
        payment1.setReservation(reservation3);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = payment1.getReservation();
        Boolean b6 = payment1.isValid();
        payment1.setCcExpDate("hi!");
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertNull(reservation5);
        org.junit.Assert.assertTrue("'" + b6 + "' != '" + false + "'", b6.equals(false));
    }

    @Test
    public void test173() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test173");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = null;
        payment1.setReservation(reservation4);
        payment1.setFee((Long) 0L);
        payment1.setCcName("");
        payment1.setCcName("");
        payment1.setCcNum("hi!");
    }

    @Test
    public void test174() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test174");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        Long long4 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = payment1.getReservation();
        payment1.setCcName("");
        Boolean b8 = payment1.isValid();
        String str9 = payment1.getCcName();
        String str10 = payment1.getCcName();
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + long3 + "' != '" + 1L + "'", long3.equals(1L));
        org.junit.Assert.assertTrue("'" + long4 + "' != '" + 1L + "'", long4.equals(1L));
        org.junit.Assert.assertNull(reservation5);
        org.junit.Assert.assertTrue("'" + b8 + "' != '" + false + "'", b8.equals(false));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "" + "'", str9.equals(""));
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
    }

    @Test
    public void test175() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test175");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Boolean b3 = payment0.isValid();
        payment0.setFee((Long) 100L);
        org.junit.Assert.assertTrue("'" + b3 + "' != '" + false + "'", b3.equals(false));
    }

    @Test
    public void test176() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test176");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = null;
        payment1.setReservation(reservation5);
        payment1.setFee((Long) 10L);
        payment1.setFee((Long) 0L);
        Long long11 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation12 = null;
        payment1.setReservation(reservation12);
        try {
            String str14 = payment1.getCcNum();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException");
        } catch (NullPointerException e) {
        }
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long11 + "' != '" + 0L + "'", long11.equals(0L));
    }

    @Test
    public void test177() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test177");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) (-1L));
        Boolean b15 = payment1.isValid();
        payment1.setCcNum("");
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b15 + "' != '" + true + "'", b15.equals(true));
    }

    @Test
    public void test178() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test178");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        payment0.setCcExpDate("");
        payment0.setCcNum("hi!");
        payment0.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = payment0.getReservation();
        payment0.setCcNum("");
        payment0.setCcName("");
        Long long18 = payment0.getFee();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "" + "'", str6.equals(""));
        org.junit.Assert.assertNull(reservation13);
        org.junit.Assert.assertNull(long18);
    }

    @Test
    public void test179() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test179");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        payment1.setCcNum("hi!");
        payment1.setCcNum("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment1.getReservation();
        String str9 = payment1.getCcNum();
        String str10 = payment1.getCcName();
        Long long11 = payment1.getFee();
        org.junit.Assert.assertNull(reservation8);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "" + "'", str9.equals(""));
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertTrue("'" + long11 + "' != '" + 10L + "'", long11.equals(10L));
    }

    @Test
    public void test180() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test180");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setFee((Long) 1L);
        Boolean b6 = payment0.isValid();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + b6 + "' != '" + false + "'", b6.equals(false));
    }

    @Test
    public void test181() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test181");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment0.getReservation();
        payment0.setCcExpDate("hi!");
        String str11 = payment0.getCcExpDate();
        String str12 = payment0.getCcNum();
        payment0.setCcExpDate("");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "" + "'", str6.equals(""));
        org.junit.Assert.assertNull(reservation7);
        org.junit.Assert.assertNull(reservation8);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "hi!" + "'", str11.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "" + "'", str12.equals(""));
    }

    @Test
    public void test182() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test182");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        payment1.setCcNum("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = null;
        payment1.setReservation(reservation13);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation15 = payment1.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation16 = payment1.getReservation();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertNull(reservation15);
        org.junit.Assert.assertNull(reservation16);
    }

    @Test
    public void test183() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test183");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        Long long4 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = payment1.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = null;
        payment1.setReservation(reservation6);
        try {
            String str8 = payment1.getCcNum();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException");
        } catch (NullPointerException e) {
        }
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + long3 + "' != '" + 1L + "'", long3.equals(1L));
        org.junit.Assert.assertTrue("'" + long4 + "' != '" + 1L + "'", long4.equals(1L));
        org.junit.Assert.assertNull(reservation5);
    }

    @Test
    public void test184() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test184");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        String str8 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment0.setReservation(reservation9);
        payment0.setCcName("hi!");
        payment0.setCcName("hi!");
        payment0.setFee((Long) 10L);
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
    }

    @Test
    public void test185() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test185");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcExpDate("hi!");
        Boolean b11 = payment1.isValid();
        payment1.setCcNum("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation14 = null;
        payment1.setReservation(reservation14);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation16 = payment1.getReservation();
        Long long17 = payment1.getFee();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b11 + "' != '" + false + "'", b11.equals(false));
        org.junit.Assert.assertNull(reservation16);
        org.junit.Assert.assertTrue("'" + long17 + "' != '" + 100L + "'", long17.equals(100L));
    }

    @Test
    public void test186() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test186");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        Long long4 = payment1.getFee();
        Long long5 = payment1.getFee();
        payment1.setFee((Long) 10L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment1.getReservation();
        payment1.setCcName("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation11 = null;
        payment1.setReservation(reservation11);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = payment1.getReservation();
        payment1.setCcExpDate("");
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + long3 + "' != '" + 1L + "'", long3.equals(1L));
        org.junit.Assert.assertTrue("'" + long4 + "' != '" + 1L + "'", long4.equals(1L));
        org.junit.Assert.assertTrue("'" + long5 + "' != '" + 1L + "'", long5.equals(1L));
        org.junit.Assert.assertNull(reservation8);
        org.junit.Assert.assertNull(reservation13);
    }

    @Test
    public void test187() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test187");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        String str10 = payment0.getCcExpDate();
        payment0.setCcExpDate("");
        Long long13 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation14 = null;
        payment0.setReservation(reservation14);
        Boolean b16 = payment0.isValid();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertNull(long13);
        org.junit.Assert.assertTrue("'" + b16 + "' != '" + true + "'", b16.equals(true));
    }

    @Test
    public void test188() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test188");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcExpDate("hi!");
        Boolean b11 = payment1.isValid();
        Boolean b12 = payment1.isValid();
        Long long13 = payment1.getFee();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b11 + "' != '" + false + "'", b11.equals(false));
        org.junit.Assert.assertTrue("'" + b12 + "' != '" + false + "'", b12.equals(false));
        org.junit.Assert.assertTrue("'" + long13 + "' != '" + 100L + "'", long13.equals(100L));
    }

    @Test
    public void test189() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test189");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        payment1.setCcNum("");
        payment1.setCcExpDate("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = null;
        payment1.setReservation(reservation6);
        Boolean b8 = payment1.isValid();
        try {
            String str9 = payment1.getCcName();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException");
        } catch (NullPointerException e) {
        }
        org.junit.Assert.assertTrue("'" + b8 + "' != '" + false + "'", b8.equals(false));
    }

    @Test
    public void test190() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test190");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        Boolean b2 = payment0.isValid();
        payment0.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = payment0.getReservation();
        payment0.setCcNum("hi!");
        payment0.setFee((Long) 10L);
        org.junit.Assert.assertNull(long1);
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertNull(reservation5);
    }

    @Test
    public void test191() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test191");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcExpDate("hi!");
        payment1.setFee((Long) 0L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment1.setReservation(reservation9);
        payment1.setCcExpDate("");
        org.junit.Assert.assertNull(reservation2);
    }

    @Test
    public void test192() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test192");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = null;
        payment1.setReservation(reservation7);
        String str9 = payment1.getCcExpDate();
        payment1.setFee((Long) 100L);
        Long long12 = payment1.getFee();
        String str13 = payment1.getCcExpDate();
        payment1.setCcName("hi!");
        Long long16 = payment1.getFee();
        String str17 = payment1.getCcName();
        payment1.setCcNum("");
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "" + "'", str9.equals(""));
        org.junit.Assert.assertTrue("'" + long12 + "' != '" + 100L + "'", long12.equals(100L));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "" + "'", str13.equals(""));
        org.junit.Assert.assertTrue("'" + long16 + "' != '" + 100L + "'", long16.equals(100L));
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "hi!" + "'", str17.equals("hi!"));
    }

    @Test
    public void test193() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test193");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment0.getReservation();
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        payment0.setCcNum("");
        payment0.setCcName("hi!");
        payment0.setCcNum("hi!");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertNull(reservation8);
    }

    @Test
    public void test194() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test194");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        payment0.setCcName("hi!");
        Boolean b14 = payment0.isValid();
        String str15 = payment0.getCcName();
        String str16 = payment0.getCcNum();
        payment0.setCcName("");
        payment0.setCcNum("hi!");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + b14 + "' != '" + true + "'", b14.equals(true));
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "hi!" + "'", str15.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "" + "'", str16.equals(""));
    }

    @Test
    public void test195() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test195");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        String str10 = payment0.getCcExpDate();
        payment0.setCcExpDate("");
        payment0.setCcNum("hi!");
        String str15 = payment0.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation16 = payment0.getReservation();
        Long long17 = payment0.getFee();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "" + "'", str15.equals(""));
        org.junit.Assert.assertNull(reservation16);
        org.junit.Assert.assertNull(long17);
    }

    @Test
    public void test196() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test196");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        Boolean b7 = payment1.isValid();
        Boolean b8 = payment1.isValid();
        payment1.setCcNum("");
        Long long11 = payment1.getFee();
        String str12 = payment1.getCcNum();
        String str13 = payment1.getCcNum();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b7 + "' != '" + false + "'", b7.equals(false));
        org.junit.Assert.assertTrue("'" + b8 + "' != '" + false + "'", b8.equals(false));
        org.junit.Assert.assertTrue("'" + long11 + "' != '" + 1L + "'", long11.equals(1L));
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "" + "'", str12.equals(""));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "" + "'", str13.equals(""));
    }

    @Test
    public void test197() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test197");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setCcNum("hi!");
        String str9 = payment1.getCcNum();
        Long long10 = payment1.getFee();
        payment1.setCcName("hi!");
        payment1.setCcName("");
        payment1.setCcExpDate("");
        payment1.setFee((Long) 10L);
        String str19 = payment1.getCcName();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "hi!" + "'", str9.equals("hi!"));
        org.junit.Assert.assertTrue("'" + long10 + "' != '" + 1L + "'", long10.equals(1L));
        org.junit.Assert.assertTrue("'" + str19 + "' != '" + "" + "'", str19.equals(""));
    }

    @Test
    public void test198() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test198");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        Boolean b2 = payment0.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = payment0.getReservation();
        Long long4 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = payment0.getReservation();
        payment0.setCcNum("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = payment0.getReservation();
        org.junit.Assert.assertNull(long1);
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertNull(reservation3);
        org.junit.Assert.assertNull(long4);
        org.junit.Assert.assertNull(reservation5);
        org.junit.Assert.assertNull(reservation6);
        org.junit.Assert.assertNull(reservation9);
    }

    @Test
    public void test199() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test199");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment0.getReservation();
        payment0.setCcExpDate("hi!");
        Long long5 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = payment0.getReservation();
        payment0.setCcExpDate("hi!");
        String str9 = payment0.getCcExpDate();
        org.junit.Assert.assertNull(long1);
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertNull(long5);
        org.junit.Assert.assertNull(reservation6);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "hi!" + "'", str9.equals("hi!"));
    }

    @Test
    public void test200() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test200");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = null;
        payment1.setReservation(reservation3);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = null;
        payment1.setReservation(reservation5);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment1.getReservation();
        Long long8 = payment1.getFee();
        payment1.setCcExpDate("");
        payment1.setCcExpDate("");
        Long long13 = payment1.getFee();
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertNull(reservation7);
        org.junit.Assert.assertTrue("'" + long8 + "' != '" + 1L + "'", long8.equals(1L));
        org.junit.Assert.assertTrue("'" + long13 + "' != '" + 1L + "'", long13.equals(1L));
    }

    @Test
    public void test201() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test201");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 0L);
        payment1.setCcNum("");
        payment1.setCcNum("hi!");
        org.junit.Assert.assertNull(reservation2);
    }

    @Test
    public void test202() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test202");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        payment1.setCcNum("");
        payment1.setCcExpDate("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = null;
        payment1.setReservation(reservation6);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment1.getReservation();
        Long long9 = payment1.getFee();
        org.junit.Assert.assertNull(reservation8);
        org.junit.Assert.assertTrue("'" + long9 + "' != '" + 1L + "'", long9.equals(1L));
    }

    @Test
    public void test203() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test203");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        payment1.setCcNum("hi!");
        Boolean b6 = payment1.isValid();
        Long long7 = payment1.getFee();
        Boolean b8 = payment1.isValid();
        org.junit.Assert.assertTrue("'" + b6 + "' != '" + false + "'", b6.equals(false));
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 10L + "'", long7.equals(10L));
        org.junit.Assert.assertTrue("'" + b8 + "' != '" + false + "'", b8.equals(false));
    }

    @Test
    public void test204() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test204");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        payment0.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment0.setReservation(reservation9);
        payment0.setCcExpDate("hi!");
        payment0.setCcName("");
        payment0.setCcNum("hi!");
        payment0.setCcExpDate("");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "" + "'", str6.equals(""));
    }

    @Test
    public void test205() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test205");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        payment1.setCcExpDate("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = payment1.getReservation();
        payment1.setCcNum("hi!");
        org.junit.Assert.assertNull(reservation6);
    }

    @Test
    public void test206() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test206");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = null;
        payment1.setReservation(reservation3);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = null;
        payment1.setReservation(reservation5);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment1.getReservation();
        Long long8 = payment1.getFee();
        payment1.setCcName("");
        Long long11 = payment1.getFee();
        String str12 = payment1.getCcName();
        payment1.setCcName("hi!");
        payment1.setCcExpDate("hi!");
        payment1.setCcNum("hi!");
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertNull(reservation7);
        org.junit.Assert.assertTrue("'" + long8 + "' != '" + 1L + "'", long8.equals(1L));
        org.junit.Assert.assertTrue("'" + long11 + "' != '" + 1L + "'", long11.equals(1L));
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "" + "'", str12.equals(""));
    }

    @Test
    public void test207() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test207");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = null;
        payment1.setReservation(reservation4);
        payment1.setFee((Long) 0L);
        String str8 = payment1.getCcName();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = payment1.getReservation();
        payment1.setCcName("");
        payment1.setCcName("");
        Long long14 = payment1.getFee();
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertNull(reservation9);
        org.junit.Assert.assertTrue("'" + long14 + "' != '" + 0L + "'", long14.equals(0L));
    }

    @Test
    public void test208() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test208");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        Long long7 = payment1.getFee();
        Boolean b8 = payment1.isValid();
        String str9 = payment1.getCcExpDate();
        String str10 = payment1.getCcExpDate();
        String str11 = payment1.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation12 = payment1.getReservation();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 1L + "'", long7.equals(1L));
        org.junit.Assert.assertTrue("'" + b8 + "' != '" + false + "'", b8.equals(false));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "" + "'", str9.equals(""));
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "" + "'", str11.equals(""));
        org.junit.Assert.assertNull(reservation12);
    }

    @Test
    public void test209() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test209");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        payment0.setCcName("");
        payment0.setCcName("");
        payment0.setCcExpDate("hi!");
        String str8 = payment0.getCcName();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment0.setReservation(reservation9);
        payment0.setCcExpDate("");
        org.junit.Assert.assertNull(long1);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
    }

    @Test
    public void test210() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test210");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        Long long7 = payment1.getFee();
        Boolean b8 = payment1.isValid();
        String str9 = payment1.getCcExpDate();
        Boolean b10 = payment1.isValid();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 1L + "'", long7.equals(1L));
        org.junit.Assert.assertTrue("'" + b8 + "' != '" + false + "'", b8.equals(false));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "" + "'", str9.equals(""));
        org.junit.Assert.assertTrue("'" + b10 + "' != '" + false + "'", b10.equals(false));
    }

    @Test
    public void test211() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test211");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        Boolean b2 = payment0.isValid();
        payment0.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = null;
        payment0.setReservation(reservation6);
        Boolean b8 = payment0.isValid();
        payment0.setCcName("hi!");
        org.junit.Assert.assertNull(long1);
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertNull(reservation5);
        org.junit.Assert.assertTrue("'" + b8 + "' != '" + false + "'", b8.equals(false));
    }

    @Test
    public void test212() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test212");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcNum("hi!");
        Boolean b5 = payment1.isValid();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b5 + "' != '" + false + "'", b5.equals(false));
    }

    @Test
    public void test213() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test213");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        payment1.setCcNum("hi!");
        Long long13 = payment1.getFee();
        payment1.setCcExpDate("");
        Long long16 = payment1.getFee();
        payment1.setFee((Long) (-1L));
        payment1.setCcName("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation21 = null;
        payment1.setReservation(reservation21);
        payment1.setFee((Long) (-1L));
        Boolean b25 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation26 = null;
        payment1.setReservation(reservation26);
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long13 + "' != '" + 100L + "'", long13.equals(100L));
        org.junit.Assert.assertTrue("'" + long16 + "' != '" + 100L + "'", long16.equals(100L));
        org.junit.Assert.assertTrue("'" + b25 + "' != '" + true + "'", b25.equals(true));
    }

    @Test
    public void test214() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test214");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        Boolean b7 = payment0.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = null;
        payment0.setReservation(reservation8);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation10 = payment0.getReservation();
        try {
            String str11 = payment0.getCcName();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException");
        } catch (NullPointerException e) {
        }
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "" + "'", str6.equals(""));
        org.junit.Assert.assertTrue("'" + b7 + "' != '" + false + "'", b7.equals(false));
        org.junit.Assert.assertNull(reservation10);
    }

    @Test
    public void test215() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test215");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        String str10 = payment0.getCcExpDate();
        String str11 = payment0.getCcName();
        payment0.setFee((Long) (-1L));
        payment0.setCcNum("");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "hi!" + "'", str11.equals("hi!"));
    }

    @Test
    public void test216() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test216");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        String str12 = payment0.getCcName();
        String str13 = payment0.getCcNum();
        String str14 = payment0.getCcNum();
        String str15 = payment0.getCcExpDate();
        payment0.setCcNum("");
        String str18 = payment0.getCcNum();
        String str19 = payment0.getCcNum();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "hi!" + "'", str12.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "" + "'", str13.equals(""));
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "" + "'", str14.equals(""));
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "" + "'", str15.equals(""));
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "" + "'", str18.equals(""));
        org.junit.Assert.assertTrue("'" + str19 + "' != '" + "" + "'", str19.equals(""));
    }

    @Test
    public void test217() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test217");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcExpDate("hi!");
        Boolean b11 = payment1.isValid();
        payment1.setCcNum("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation14 = null;
        payment1.setReservation(reservation14);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation16 = payment1.getReservation();
        try {
            String str17 = payment1.getCcName();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException");
        } catch (NullPointerException e) {
        }
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b11 + "' != '" + false + "'", b11.equals(false));
        org.junit.Assert.assertNull(reservation16);
    }

    @Test
    public void test218() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test218");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        payment1.setFee((Long) 1L);
        Long long6 = payment1.getFee();
        payment1.setFee((Long) 0L);
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + long3 + "' != '" + 1L + "'", long3.equals(1L));
        org.junit.Assert.assertTrue("'" + long6 + "' != '" + 1L + "'", long6.equals(1L));
    }

    @Test
    public void test219() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test219");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        String str5 = payment1.getCcExpDate();
        Long long6 = payment1.getFee();
        Long long7 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = null;
        payment1.setReservation(reservation8);
        payment1.setCcNum("hi!");
        Long long12 = payment1.getFee();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "" + "'", str5.equals(""));
        org.junit.Assert.assertTrue("'" + long6 + "' != '" + 1L + "'", long6.equals(1L));
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 1L + "'", long7.equals(1L));
        org.junit.Assert.assertTrue("'" + long12 + "' != '" + 1L + "'", long12.equals(1L));
    }

    @Test
    public void test220() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test220");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        String str10 = payment0.getCcExpDate();
        payment0.setCcExpDate("");
        payment0.setFee((Long) 0L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation15 = null;
        payment0.setReservation(reservation15);
        payment0.setCcNum("");
        payment0.setCcNum("");
        payment0.setCcNum("");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
    }

    @Test
    public void test221() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test221");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        Boolean b5 = payment1.isValid();
        payment1.setFee((Long) 0L);
        String str8 = payment1.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment1.setReservation(reservation9);
        Long long11 = payment1.getFee();
        try {
            String str12 = payment1.getCcNum();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException");
        } catch (NullPointerException e) {
        }
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b5 + "' != '" + false + "'", b5.equals(false));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertTrue("'" + long11 + "' != '" + 0L + "'", long11.equals(0L));
    }

    @Test
    public void test222() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test222");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        String str8 = payment0.getCcNum();
        String str9 = payment0.getCcExpDate();
        payment0.setCcName("hi!");
        payment0.setFee((Long) 1L);
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "" + "'", str9.equals(""));
    }

    @Test
    public void test223() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test223");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Boolean b3 = payment0.isValid();
        payment0.setCcNum("");
        payment0.setFee((Long) (-1L));
        payment0.setCcName("hi!");
        org.junit.Assert.assertTrue("'" + b3 + "' != '" + false + "'", b3.equals(false));
    }

    @Test
    public void test224() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test224");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) (-1L));
        payment1.setFee((Long) 0L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = null;
        payment1.setReservation(reservation4);
        payment1.setCcName("");
        payment1.setCcNum("");
        payment1.setFee((Long) 100L);
        payment1.setFee((Long) 0L);
    }

    @Test
    public void test225() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test225");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcName("");
        payment0.setCcExpDate("");
        payment0.setCcExpDate("");
        Boolean b10 = payment0.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation11 = payment0.getReservation();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + b10 + "' != '" + true + "'", b10.equals(true));
        org.junit.Assert.assertNull(reservation11);
    }

    @Test
    public void test226() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test226");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        String str8 = payment0.getCcNum();
        String str9 = payment0.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation10 = payment0.getReservation();
        String str11 = payment0.getCcExpDate();
        String str12 = payment0.getCcExpDate();
        payment0.setCcNum("hi!");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "" + "'", str9.equals(""));
        org.junit.Assert.assertNull(reservation10);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "" + "'", str11.equals(""));
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "" + "'", str12.equals(""));
    }

    @Test
    public void test227() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test227");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setCcNum("hi!");
        String str9 = payment1.getCcNum();
        Long long10 = payment1.getFee();
        payment1.setCcName("hi!");
        payment1.setCcName("");
        payment1.setCcName("hi!");
        payment1.setCcExpDate("");
        payment1.setCcName("");
        payment1.setCcName("");
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "hi!" + "'", str9.equals("hi!"));
        org.junit.Assert.assertTrue("'" + long10 + "' != '" + 1L + "'", long10.equals(1L));
    }

    @Test
    public void test228() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test228");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        payment0.setCcExpDate("");
        payment0.setCcNum("hi!");
        payment0.setFee((Long) 0L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = null;
        payment0.setReservation(reservation13);
        Long long15 = payment0.getFee();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "" + "'", str6.equals(""));
        org.junit.Assert.assertTrue("'" + long15 + "' != '" + 0L + "'", long15.equals(0L));
    }

    @Test
    public void test229() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test229");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        String str12 = payment0.getCcName();
        String str13 = payment0.getCcNum();
        String str14 = payment0.getCcNum();
        String str15 = payment0.getCcName();
        String str16 = payment0.getCcExpDate();
        String str17 = payment0.getCcNum();
        String str18 = payment0.getCcName();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation19 = payment0.getReservation();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "hi!" + "'", str12.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "" + "'", str13.equals(""));
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "" + "'", str14.equals(""));
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "hi!" + "'", str15.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "" + "'", str16.equals(""));
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "" + "'", str17.equals(""));
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "hi!" + "'", str18.equals("hi!"));
        org.junit.Assert.assertNull(reservation19);
    }

    @Test
    public void test230() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test230");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = null;
        payment1.setReservation(reservation3);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = null;
        payment1.setReservation(reservation5);
        Long long7 = payment1.getFee();
        payment1.setCcName("hi!");
        payment1.setCcNum("");
        Boolean b12 = payment1.isValid();
        String str13 = payment1.getCcNum();
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 1L + "'", long7.equals(1L));
        org.junit.Assert.assertTrue("'" + b12 + "' != '" + false + "'", b12.equals(false));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "" + "'", str13.equals(""));
    }

    @Test
    public void test231() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test231");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcNum("hi!");
        payment1.setCcName("hi!");
        Long long7 = payment1.getFee();
        payment1.setCcName("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation10 = null;
        payment1.setReservation(reservation10);
        payment1.setFee((Long) 100L);
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 1L + "'", long7.equals(1L));
    }

    @Test
    public void test232() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test232");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        String str12 = payment0.getCcName();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = null;
        payment0.setReservation(reservation13);
        String str15 = payment0.getCcName();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "hi!" + "'", str12.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "hi!" + "'", str15.equals("hi!"));
    }

    @Test
    public void test233() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test233");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcName("");
        Boolean b7 = payment1.isValid();
        String str8 = payment1.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = payment1.getReservation();
        payment1.setFee((Long) 0L);
        payment1.setCcExpDate("hi!");
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b7 + "' != '" + false + "'", b7.equals(false));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertNull(reservation9);
    }

    @Test
    public void test234() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test234");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        String str10 = payment0.getCcExpDate();
        String str11 = payment0.getCcName();
        String str12 = payment0.getCcNum();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "hi!" + "'", str11.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "" + "'", str12.equals(""));
    }

    @Test
    public void test235() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test235");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        String str8 = payment0.getCcNum();
        String str9 = payment0.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation10 = payment0.getReservation();
        Boolean b11 = payment0.isValid();
        payment0.setCcName("hi!");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "" + "'", str9.equals(""));
        org.junit.Assert.assertNull(reservation10);
        org.junit.Assert.assertTrue("'" + b11 + "' != '" + false + "'", b11.equals(false));
    }

    @Test
    public void test236() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test236");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = null;
        payment1.setReservation(reservation4);
        payment1.setFee((Long) 0L);
        String str8 = payment1.getCcName();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = payment1.getReservation();
        payment1.setCcName("");
        payment1.setCcName("");
        String str14 = payment1.getCcName();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation15 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setFee((Long) 0L);
        payment1.setCcExpDate("hi!");
        payment1.setCcExpDate("hi!");
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertNull(reservation9);
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "" + "'", str14.equals(""));
        org.junit.Assert.assertNull(reservation15);
    }

    @Test
    public void test237() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test237");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        Boolean b2 = payment0.isValid();
        payment0.setCcName("");
        payment0.setCcExpDate("");
        payment0.setCcExpDate("");
        org.junit.Assert.assertNull(long1);
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
    }

    @Test
    public void test238() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test238");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        payment1.setCcNum("hi!");
        Long long13 = payment1.getFee();
        payment1.setCcExpDate("");
        String str16 = payment1.getCcNum();
        Long long17 = payment1.getFee();
        Long long18 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation19 = null;
        payment1.setReservation(reservation19);
        Boolean b21 = payment1.isValid();
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        payment1.setCcName("hi!");
        String str28 = payment1.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation29 = null;
        payment1.setReservation(reservation29);
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long13 + "' != '" + 100L + "'", long13.equals(100L));
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "hi!" + "'", str16.equals("hi!"));
        org.junit.Assert.assertTrue("'" + long17 + "' != '" + 100L + "'", long17.equals(100L));
        org.junit.Assert.assertTrue("'" + long18 + "' != '" + 100L + "'", long18.equals(100L));
        org.junit.Assert.assertTrue("'" + b21 + "' != '" + true + "'", b21.equals(true));
        org.junit.Assert.assertTrue("'" + str28 + "' != '" + "hi!" + "'", str28.equals("hi!"));
    }

    @Test
    public void test239() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test239");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        Boolean b2 = payment0.isValid();
        Boolean b3 = payment0.isValid();
        payment0.setCcName("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = null;
        payment0.setReservation(reservation6);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = null;
        payment0.setReservation(reservation8);
        org.junit.Assert.assertNull(long1);
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + b3 + "' != '" + false + "'", b3.equals(false));
    }

    @Test
    public void test240() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test240");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        String str12 = payment0.getCcName();
        Boolean b13 = payment0.isValid();
        payment0.setCcName("");
        String str16 = payment0.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation17 = payment0.getReservation();
        String str18 = payment0.getCcNum();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "hi!" + "'", str12.equals("hi!"));
        org.junit.Assert.assertTrue("'" + b13 + "' != '" + true + "'", b13.equals(true));
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "" + "'", str16.equals(""));
        org.junit.Assert.assertNull(reservation17);
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "" + "'", str18.equals(""));
    }

    @Test
    public void test241() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test241");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment0.getReservation();
        payment0.setCcExpDate("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation11 = payment0.getReservation();
        payment0.setCcNum("");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "" + "'", str6.equals(""));
        org.junit.Assert.assertNull(reservation7);
        org.junit.Assert.assertNull(reservation8);
        org.junit.Assert.assertNull(reservation11);
    }

    @Test
    public void test242() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test242");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = null;
        payment1.setReservation(reservation7);
        String str9 = payment1.getCcExpDate();
        payment1.setFee((Long) 100L);
        payment1.setCcExpDate("");
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "" + "'", str9.equals(""));
    }

    @Test
    public void test243() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test243");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        payment1.setFee((Long) 10L);
        payment1.setCcExpDate("hi!");
        payment1.setCcName("");
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + long3 + "' != '" + 1L + "'", long3.equals(1L));
    }

    @Test
    public void test244() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test244");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = null;
        payment1.setReservation(reservation5);
        payment1.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment1.setReservation(reservation9);
        payment1.setCcNum("");
        org.junit.Assert.assertNull(reservation2);
    }

    @Test
    public void test245() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test245");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setCcNum("hi!");
        String str9 = payment1.getCcNum();
        Long long10 = payment1.getFee();
        payment1.setCcNum("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = payment1.getReservation();
        payment1.setCcName("");
        payment1.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation18 = null;
        payment1.setReservation(reservation18);
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "hi!" + "'", str9.equals("hi!"));
        org.junit.Assert.assertTrue("'" + long10 + "' != '" + 1L + "'", long10.equals(1L));
        org.junit.Assert.assertNull(reservation13);
    }

    @Test
    public void test246() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test246");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        String str10 = payment0.getCcExpDate();
        payment0.setCcExpDate("");
        Long long13 = payment0.getFee();
        String str14 = payment0.getCcName();
        Boolean b15 = payment0.isValid();
        Long long16 = payment0.getFee();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertNull(long13);
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "hi!" + "'", str14.equals("hi!"));
        org.junit.Assert.assertTrue("'" + b15 + "' != '" + true + "'", b15.equals(true));
        org.junit.Assert.assertNull(long16);
    }

    @Test
    public void test247() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test247");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        payment1.setCcNum("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = null;
        payment1.setReservation(reservation6);
        String str8 = payment1.getCcName();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment1.setReservation(reservation9);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
    }

    @Test
    public void test248() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test248");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        String str5 = payment1.getCcExpDate();
        payment1.setCcExpDate("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = null;
        payment1.setReservation(reservation8);
        String str10 = payment1.getCcExpDate();
        String str11 = payment1.getCcExpDate();
        payment1.setCcExpDate("hi!");
        String str14 = payment1.getCcExpDate();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "" + "'", str5.equals(""));
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "hi!" + "'", str10.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "hi!" + "'", str11.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "hi!" + "'", str14.equals("hi!"));
    }

    @Test
    public void test249() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test249");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcExpDate("hi!");
        payment1.setFee((Long) 0L);
        payment1.setCcName("hi!");
        org.junit.Assert.assertNull(reservation2);
    }

    @Test
    public void test250() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test250");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        payment1.setCcNum("hi!");
        String str6 = payment1.getCcName();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = null;
        payment1.setReservation(reservation7);
        payment1.setCcName("");
        payment1.setFee((Long) 100L);
        String str13 = payment1.getCcNum();
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "" + "'", str6.equals(""));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "hi!" + "'", str13.equals("hi!"));
    }

    @Test
    public void test251() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test251");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation10 = null;
        payment0.setReservation(reservation10);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation12 = payment0.getReservation();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertNull(reservation12);
    }

    @Test
    public void test252() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test252");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        payment1.setCcNum("hi!");
        Long long13 = payment1.getFee();
        payment1.setCcExpDate("");
        String str16 = payment1.getCcNum();
        Long long17 = payment1.getFee();
        Long long18 = payment1.getFee();
        Boolean b19 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation20 = payment1.getReservation();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long13 + "' != '" + 100L + "'", long13.equals(100L));
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "hi!" + "'", str16.equals("hi!"));
        org.junit.Assert.assertTrue("'" + long17 + "' != '" + 100L + "'", long17.equals(100L));
        org.junit.Assert.assertTrue("'" + long18 + "' != '" + 100L + "'", long18.equals(100L));
        org.junit.Assert.assertTrue("'" + b19 + "' != '" + true + "'", b19.equals(true));
        org.junit.Assert.assertNull(reservation20);
    }

    @Test
    public void test253() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test253");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = null;
        payment1.setReservation(reservation4);
        payment1.setFee((Long) 0L);
        payment1.setCcExpDate("");
        payment1.setCcNum("");
        Long long12 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = payment1.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation14 = null;
        payment1.setReservation(reservation14);
        org.junit.Assert.assertTrue("'" + long12 + "' != '" + 0L + "'", long12.equals(0L));
        org.junit.Assert.assertNull(reservation13);
    }

    @Test
    public void test254() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test254");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcName("");
        Boolean b7 = payment1.isValid();
        Long long8 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = payment1.getReservation();
        payment1.setFee((Long) (-1L));
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b7 + "' != '" + false + "'", b7.equals(false));
        org.junit.Assert.assertTrue("'" + long8 + "' != '" + 1L + "'", long8.equals(1L));
        org.junit.Assert.assertNull(reservation9);
    }

    @Test
    public void test255() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test255");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) (-1L));
        payment1.setFee((Long) 0L);
        payment1.setCcExpDate("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = payment1.getReservation();
        Long long7 = payment1.getFee();
        org.junit.Assert.assertNull(reservation6);
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 0L + "'", long7.equals(0L));
    }

    @Test
    public void test256() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test256");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcName("");
        payment0.setCcExpDate("hi!");
        String str8 = payment0.getCcExpDate();
        payment0.setCcExpDate("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation11 = payment0.getReservation();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "hi!" + "'", str8.equals("hi!"));
        org.junit.Assert.assertNull(reservation11);
    }

    @Test
    public void test257() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test257");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        Long long4 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = payment1.getReservation();
        payment1.setCcName("");
        Long long8 = payment1.getFee();
        payment1.setCcExpDate("hi!");
        payment1.setCcName("hi!");
        Boolean b13 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation14 = null;
        payment1.setReservation(reservation14);
        payment1.setFee((Long) 100L);
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + long3 + "' != '" + 1L + "'", long3.equals(1L));
        org.junit.Assert.assertTrue("'" + long4 + "' != '" + 1L + "'", long4.equals(1L));
        org.junit.Assert.assertNull(reservation5);
        org.junit.Assert.assertTrue("'" + long8 + "' != '" + 1L + "'", long8.equals(1L));
        org.junit.Assert.assertTrue("'" + b13 + "' != '" + false + "'", b13.equals(false));
    }

    @Test
    public void test258() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test258");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        payment1.setCcNum("");
        payment1.setCcExpDate("hi!");
        payment1.setCcNum("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment1.getReservation();
        payment1.setFee((Long) 1L);
        Boolean b11 = payment1.isValid();
        org.junit.Assert.assertNull(reservation8);
        org.junit.Assert.assertTrue("'" + b11 + "' != '" + false + "'", b11.equals(false));
    }

    @Test
    public void test259() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test259");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = null;
        payment1.setReservation(reservation3);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = null;
        payment1.setReservation(reservation5);
        payment1.setCcNum("");
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
    }

    @Test
    public void test260() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test260");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = null;
        payment1.setReservation(reservation4);
        payment1.setFee((Long) 0L);
        String str8 = payment1.getCcName();
        String str9 = payment1.getCcName();
        Boolean b10 = payment1.isValid();
        payment1.setCcExpDate("");
        payment1.setCcName("");
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "" + "'", str9.equals(""));
        org.junit.Assert.assertTrue("'" + b10 + "' != '" + false + "'", b10.equals(false));
    }

    @Test
    public void test261() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test261");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        String str8 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment0.setReservation(reservation9);
        payment0.setCcName("hi!");
        payment0.setCcName("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation15 = payment0.getReservation();
        String str16 = payment0.getCcExpDate();
        Boolean b17 = payment0.isValid();
        payment0.setFee((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation20 = null;
        payment0.setReservation(reservation20);
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertNull(reservation15);
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "" + "'", str16.equals(""));
        org.junit.Assert.assertTrue("'" + b17 + "' != '" + true + "'", b17.equals(true));
    }

    @Test
    public void test262() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test262");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        payment1.setCcNum("");
        payment1.setCcExpDate("hi!");
        payment1.setCcNum("hi!");
        payment1.setCcName("");
        payment1.setCcName("");
        payment1.setCcName("");
        payment1.setCcExpDate("hi!");
    }

    @Test
    public void test263() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test263");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setCcNum("hi!");
        String str9 = payment1.getCcNum();
        Long long10 = payment1.getFee();
        payment1.setCcName("hi!");
        payment1.setCcName("");
        payment1.setCcName("hi!");
        payment1.setCcExpDate("");
        payment1.setCcName("");
        String str21 = payment1.getCcNum();
        Long long22 = payment1.getFee();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "hi!" + "'", str9.equals("hi!"));
        org.junit.Assert.assertTrue("'" + long10 + "' != '" + 1L + "'", long10.equals(1L));
        org.junit.Assert.assertTrue("'" + str21 + "' != '" + "hi!" + "'", str21.equals("hi!"));
        org.junit.Assert.assertTrue("'" + long22 + "' != '" + 1L + "'", long22.equals(1L));
    }

    @Test
    public void test264() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test264");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        String str10 = payment0.getCcExpDate();
        payment0.setCcExpDate("");
        payment0.setCcNum("hi!");
        String str15 = payment0.getCcExpDate();
        payment0.setFee((Long) (-1L));
        String str18 = payment0.getCcName();
        String str19 = payment0.getCcNum();
        payment0.setCcExpDate("");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "" + "'", str15.equals(""));
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "hi!" + "'", str18.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str19 + "' != '" + "hi!" + "'", str19.equals("hi!"));
    }

    @Test
    public void test265() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test265");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment0.getReservation();
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        Boolean b13 = payment0.isValid();
        String str14 = payment0.getCcExpDate();
        payment0.setCcExpDate("hi!");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertNull(reservation8);
        org.junit.Assert.assertTrue("'" + b13 + "' != '" + true + "'", b13.equals(true));
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "" + "'", str14.equals(""));
    }

    @Test
    public void test266() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test266");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        payment0.setCcNum("hi!");
        payment0.setCcNum("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment0.getReservation();
        payment0.setCcNum("hi!");
        org.junit.Assert.assertNull(reservation7);
        org.junit.Assert.assertNull(reservation8);
    }

    @Test
    public void test267() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test267");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment0.getReservation();
        payment0.setCcExpDate("hi!");
        String str11 = payment0.getCcExpDate();
        payment0.setCcNum("hi!");
        payment0.setCcName("");
        Boolean b16 = payment0.isValid();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "" + "'", str6.equals(""));
        org.junit.Assert.assertNull(reservation7);
        org.junit.Assert.assertNull(reservation8);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "hi!" + "'", str11.equals("hi!"));
        org.junit.Assert.assertTrue("'" + b16 + "' != '" + true + "'", b16.equals(true));
    }

    @Test
    public void test268() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test268");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        payment0.setCcName("");
        payment0.setCcName("");
        payment0.setCcExpDate("hi!");
        String str8 = payment0.getCcName();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment0.setReservation(reservation9);
        try {
            String str11 = payment0.getCcNum();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException");
        } catch (NullPointerException e) {
        }
        org.junit.Assert.assertNull(long1);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
    }

    @Test
    public void test269() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test269");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = null;
        payment1.setReservation(reservation3);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = null;
        payment1.setReservation(reservation5);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment1.getReservation();
        Long long8 = payment1.getFee();
        payment1.setCcName("");
        Long long11 = payment1.getFee();
        String str12 = payment1.getCcName();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = payment1.getReservation();
        payment1.setCcExpDate("");
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertNull(reservation7);
        org.junit.Assert.assertTrue("'" + long8 + "' != '" + 1L + "'", long8.equals(1L));
        org.junit.Assert.assertTrue("'" + long11 + "' != '" + 1L + "'", long11.equals(1L));
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "" + "'", str12.equals(""));
        org.junit.Assert.assertNull(reservation13);
    }

    @Test
    public void test270() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test270");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        payment0.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment0.setReservation(reservation9);
        payment0.setCcExpDate("hi!");
        String str13 = payment0.getCcExpDate();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "" + "'", str6.equals(""));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "hi!" + "'", str13.equals("hi!"));
    }

    @Test
    public void test271() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test271");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = null;
        payment1.setReservation(reservation7);
        String str9 = payment1.getCcExpDate();
        payment1.setFee((Long) 100L);
        Long long12 = payment1.getFee();
        String str13 = payment1.getCcExpDate();
        payment1.setCcName("hi!");
        payment1.setCcNum("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation18 = null;
        payment1.setReservation(reservation18);
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "" + "'", str9.equals(""));
        org.junit.Assert.assertTrue("'" + long12 + "' != '" + 100L + "'", long12.equals(100L));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "" + "'", str13.equals(""));
    }

    @Test
    public void test272() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test272");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        String str12 = payment0.getCcName();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = null;
        payment0.setReservation(reservation13);
        String str15 = payment0.getCcNum();
        String str16 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation17 = null;
        payment0.setReservation(reservation17);
        payment0.setCcExpDate("hi!");
        Boolean b21 = payment0.isValid();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "hi!" + "'", str12.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "" + "'", str15.equals(""));
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "" + "'", str16.equals(""));
        org.junit.Assert.assertTrue("'" + b21 + "' != '" + true + "'", b21.equals(true));
    }

    @Test
    public void test273() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test273");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        String str8 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment0.setReservation(reservation9);
        payment0.setCcName("hi!");
        String str13 = payment0.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation14 = null;
        payment0.setReservation(reservation14);
        Boolean b16 = payment0.isValid();
        payment0.setCcName("");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "" + "'", str13.equals(""));
        org.junit.Assert.assertTrue("'" + b16 + "' != '" + true + "'", b16.equals(true));
    }

    @Test
    public void test274() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test274");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        payment1.setCcExpDate("hi!");
        String str6 = payment1.getCcName();
        payment1.setFee((Long) 0L);
        String str9 = payment1.getCcExpDate();
        payment1.setCcNum("hi!");
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "" + "'", str6.equals(""));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "hi!" + "'", str9.equals("hi!"));
    }

    @Test
    public void test275() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test275");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcNum("hi!");
        payment1.setCcName("hi!");
        Long long7 = payment1.getFee();
        String str8 = payment1.getCcNum();
        payment1.setFee((Long) 100L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation11 = payment1.getReservation();
        String str12 = payment1.getCcNum();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 1L + "'", long7.equals(1L));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "hi!" + "'", str8.equals("hi!"));
        org.junit.Assert.assertNull(reservation11);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "hi!" + "'", str12.equals("hi!"));
    }

    @Test
    public void test276() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test276");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setCcNum("hi!");
        String str9 = payment1.getCcNum();
        Long long10 = payment1.getFee();
        payment1.setCcName("");
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "hi!" + "'", str9.equals("hi!"));
        org.junit.Assert.assertTrue("'" + long10 + "' != '" + 1L + "'", long10.equals(1L));
    }

    @Test
    public void test277() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test277");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        String str8 = payment0.getCcNum();
        String str9 = payment0.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation10 = payment0.getReservation();
        String str11 = payment0.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation12 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = null;
        payment0.setReservation(reservation13);
        Long long15 = payment0.getFee();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "" + "'", str9.equals(""));
        org.junit.Assert.assertNull(reservation10);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "" + "'", str11.equals(""));
        org.junit.Assert.assertNull(reservation12);
        org.junit.Assert.assertNull(long15);
    }

    @Test
    public void test278() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test278");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        String str8 = payment0.getCcExpDate();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
    }

    @Test
    public void test279() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test279");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        Boolean b5 = payment1.isValid();
        payment1.setFee((Long) 0L);
        String str8 = payment1.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment1.setReservation(reservation9);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation11 = null;
        payment1.setReservation(reservation11);
        payment1.setFee((Long) 100L);
        Long long15 = payment1.getFee();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b5 + "' != '" + false + "'", b5.equals(false));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertTrue("'" + long15 + "' != '" + 100L + "'", long15.equals(100L));
    }

    @Test
    public void test280() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test280");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        payment0.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment0.setReservation(reservation9);
        String str11 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation12 = payment0.getReservation();
        Long long13 = payment0.getFee();
        payment0.setCcName("");
        payment0.setCcExpDate("hi!");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "" + "'", str6.equals(""));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "" + "'", str11.equals(""));
        org.junit.Assert.assertNull(reservation12);
        org.junit.Assert.assertNull(long13);
    }

    @Test
    public void test281() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test281");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcNum("hi!");
        Long long5 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = null;
        payment1.setReservation(reservation6);
        try {
            String str8 = payment1.getCcName();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException");
        } catch (NullPointerException e) {
        }
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long5 + "' != '" + 1L + "'", long5.equals(1L));
    }

    @Test
    public void test282() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test282");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        Boolean b7 = payment1.isValid();
        Boolean b8 = payment1.isValid();
        payment1.setCcNum("");
        Long long11 = payment1.getFee();
        String str12 = payment1.getCcNum();
        payment1.setFee((Long) 0L);
        payment1.setFee((Long) 10L);
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b7 + "' != '" + false + "'", b7.equals(false));
        org.junit.Assert.assertTrue("'" + b8 + "' != '" + false + "'", b8.equals(false));
        org.junit.Assert.assertTrue("'" + long11 + "' != '" + 1L + "'", long11.equals(1L));
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "" + "'", str12.equals(""));
    }

    @Test
    public void test283() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test283");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = null;
        payment1.setReservation(reservation4);
        payment1.setCcNum("");
        payment1.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation10 = null;
        payment1.setReservation(reservation10);
    }

    @Test
    public void test284() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test284");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment0.getReservation();
        payment0.setCcExpDate("hi!");
        String str11 = payment0.getCcExpDate();
        payment0.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation14 = payment0.getReservation();
        String str15 = payment0.getCcExpDate();
        payment0.setCcNum("");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "" + "'", str6.equals(""));
        org.junit.Assert.assertNull(reservation7);
        org.junit.Assert.assertNull(reservation8);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "hi!" + "'", str11.equals("hi!"));
        org.junit.Assert.assertNull(reservation14);
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "" + "'", str15.equals(""));
    }

    @Test
    public void test285() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test285");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcName("");
        payment0.setCcExpDate("hi!");
        payment0.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation10 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation11 = payment0.getReservation();
        payment0.setFee((Long) 0L);
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertNull(reservation10);
        org.junit.Assert.assertNull(reservation11);
    }

    @Test
    public void test286() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test286");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        payment0.setCcNum("hi!");
        payment0.setCcNum("");
        payment0.setCcExpDate("hi!");
        Boolean b9 = payment0.isValid();
        org.junit.Assert.assertTrue("'" + b9 + "' != '" + false + "'", b9.equals(false));
    }

    @Test
    public void test287() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test287");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        Long long4 = payment1.getFee();
        Long long5 = payment1.getFee();
        payment1.setFee((Long) 10L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment1.getReservation();
        payment1.setCcName("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation11 = null;
        payment1.setReservation(reservation11);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = payment1.getReservation();
        Boolean b14 = payment1.isValid();
        Long long15 = payment1.getFee();
        payment1.setCcExpDate("hi!");
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + long3 + "' != '" + 1L + "'", long3.equals(1L));
        org.junit.Assert.assertTrue("'" + long4 + "' != '" + 1L + "'", long4.equals(1L));
        org.junit.Assert.assertTrue("'" + long5 + "' != '" + 1L + "'", long5.equals(1L));
        org.junit.Assert.assertNull(reservation8);
        org.junit.Assert.assertNull(reservation13);
        org.junit.Assert.assertTrue("'" + b14 + "' != '" + false + "'", b14.equals(false));
        org.junit.Assert.assertTrue("'" + long15 + "' != '" + 10L + "'", long15.equals(10L));
    }

    @Test
    public void test288() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test288");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        Long long7 = payment1.getFee();
        payment1.setFee((Long) (-1L));
        String str10 = payment1.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation11 = payment1.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation12 = payment1.getReservation();
        payment1.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation15 = payment1.getReservation();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 1L + "'", long7.equals(1L));
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertNull(reservation11);
        org.junit.Assert.assertNull(reservation12);
        org.junit.Assert.assertNull(reservation15);
    }

    @Test
    public void test289() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test289");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        String str12 = payment0.getCcName();
        String str13 = payment0.getCcNum();
        String str14 = payment0.getCcNum();
        String str15 = payment0.getCcName();
        payment0.setCcName("");
        payment0.setCcNum("hi!");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "hi!" + "'", str12.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "" + "'", str13.equals(""));
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "" + "'", str14.equals(""));
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "hi!" + "'", str15.equals("hi!"));
    }

    @Test
    public void test290() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test290");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment0.getReservation();
        payment0.setCcExpDate("hi!");
        payment0.setFee((Long) 0L);
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "" + "'", str6.equals(""));
        org.junit.Assert.assertNull(reservation7);
        org.junit.Assert.assertNull(reservation8);
    }

    @Test
    public void test291() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test291");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcName("");
        payment0.setCcExpDate("");
        payment0.setCcExpDate("");
        Boolean b10 = payment0.isValid();
        payment0.setCcExpDate("hi!");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + b10 + "' != '" + true + "'", b10.equals(true));
    }

    @Test
    public void test292() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test292");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        payment1.setCcNum("hi!");
        Long long13 = payment1.getFee();
        Long long14 = payment1.getFee();
        String str15 = payment1.getCcNum();
        String str16 = payment1.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation17 = null;
        payment1.setReservation(reservation17);
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long13 + "' != '" + 100L + "'", long13.equals(100L));
        org.junit.Assert.assertTrue("'" + long14 + "' != '" + 100L + "'", long14.equals(100L));
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "hi!" + "'", str15.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "" + "'", str16.equals(""));
    }

    @Test
    public void test293() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test293");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        payment1.setCcNum("hi!");
        payment1.setCcNum("");
        Boolean b15 = payment1.isValid();
        String str16 = payment1.getCcExpDate();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b15 + "' != '" + true + "'", b15.equals(true));
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "" + "'", str16.equals(""));
    }

    @Test
    public void test294() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test294");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        String str10 = payment0.getCcExpDate();
        payment0.setCcExpDate("");
        payment0.setCcNum("hi!");
        String str15 = payment0.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation16 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation17 = null;
        payment0.setReservation(reservation17);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation19 = null;
        payment0.setReservation(reservation19);
        String str21 = payment0.getCcName();
        Boolean b22 = payment0.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation23 = null;
        payment0.setReservation(reservation23);
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "" + "'", str15.equals(""));
        org.junit.Assert.assertNull(reservation16);
        org.junit.Assert.assertTrue("'" + str21 + "' != '" + "hi!" + "'", str21.equals("hi!"));
        org.junit.Assert.assertTrue("'" + b22 + "' != '" + true + "'", b22.equals(true));
    }

    @Test
    public void test295() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test295");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        payment1.setCcNum("");
        Boolean b4 = payment1.isValid();
        payment1.setCcExpDate("");
        org.junit.Assert.assertTrue("'" + b4 + "' != '" + false + "'", b4.equals(false));
    }

    @Test
    public void test296() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test296");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcName("");
        payment0.setCcExpDate("");
        Boolean b8 = payment0.isValid();
        Boolean b9 = payment0.isValid();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + b8 + "' != '" + true + "'", b8.equals(true));
        org.junit.Assert.assertTrue("'" + b9 + "' != '" + true + "'", b9.equals(true));
    }

    @Test
    public void test297() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test297");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment0.getReservation();
        payment0.setCcExpDate("hi!");
        String str11 = payment0.getCcExpDate();
        payment0.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation14 = null;
        payment0.setReservation(reservation14);
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "" + "'", str6.equals(""));
        org.junit.Assert.assertNull(reservation7);
        org.junit.Assert.assertNull(reservation8);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "hi!" + "'", str11.equals("hi!"));
    }

    @Test
    public void test298() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test298");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation11 = payment1.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation12 = payment1.getReservation();
        String str13 = payment1.getCcName();
        String str14 = payment1.getCcExpDate();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertNull(reservation11);
        org.junit.Assert.assertNull(reservation12);
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "hi!" + "'", str13.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "" + "'", str14.equals(""));
    }

    @Test
    public void test299() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test299");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = null;
        payment1.setReservation(reservation4);
        payment1.setFee((Long) 1L);
        String str8 = payment1.getCcName();
        Boolean b9 = payment1.isValid();
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertTrue("'" + b9 + "' != '" + false + "'", b9.equals(false));
    }

    @Test
    public void test300() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test300");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        String str10 = payment0.getCcExpDate();
        payment0.setCcExpDate("");
        payment0.setCcNum("hi!");
        String str15 = payment0.getCcExpDate();
        payment0.setFee((Long) (-1L));
        Boolean b18 = payment0.isValid();
        String str19 = payment0.getCcName();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "" + "'", str15.equals(""));
        org.junit.Assert.assertTrue("'" + b18 + "' != '" + true + "'", b18.equals(true));
        org.junit.Assert.assertTrue("'" + str19 + "' != '" + "hi!" + "'", str19.equals("hi!"));
    }

    @Test
    public void test301() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test301");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        String str12 = payment0.getCcName();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = null;
        payment0.setReservation(reservation13);
        String str15 = payment0.getCcNum();
        Boolean b16 = payment0.isValid();
        String str17 = payment0.getCcExpDate();
        payment0.setCcExpDate("hi!");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "hi!" + "'", str12.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "" + "'", str15.equals(""));
        org.junit.Assert.assertTrue("'" + b16 + "' != '" + true + "'", b16.equals(true));
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "" + "'", str17.equals(""));
    }

    @Test
    public void test302() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test302");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = null;
        payment1.setReservation(reservation5);
        payment1.setCcName("");
        payment1.setCcExpDate("hi!");
        org.junit.Assert.assertNull(reservation2);
    }

    @Test
    public void test303() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test303");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        String str10 = payment0.getCcExpDate();
        payment0.setCcExpDate("");
        payment0.setCcNum("hi!");
        String str15 = payment0.getCcExpDate();
        payment0.setFee((Long) (-1L));
        String str18 = payment0.getCcExpDate();
        payment0.setCcName("");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "" + "'", str15.equals(""));
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "" + "'", str18.equals(""));
    }

    @Test
    public void test304() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test304");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = null;
        payment1.setReservation(reservation4);
        payment1.setFee((Long) 0L);
        String str8 = payment1.getCcName();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = payment1.getReservation();
        payment1.setCcName("");
        payment1.setCcNum("");
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertNull(reservation9);
    }

    @Test
    public void test305() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test305");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        payment1.setFee((Long) 10L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = payment1.getReservation();
        Boolean b7 = payment1.isValid();
        payment1.setCcName("");
        payment1.setCcName("");
        Boolean b12 = payment1.isValid();
        payment1.setCcExpDate("hi!");
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + long3 + "' != '" + 1L + "'", long3.equals(1L));
        org.junit.Assert.assertNull(reservation6);
        org.junit.Assert.assertTrue("'" + b7 + "' != '" + false + "'", b7.equals(false));
        org.junit.Assert.assertTrue("'" + b12 + "' != '" + false + "'", b12.equals(false));
    }

    @Test
    public void test306() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test306");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        payment1.setCcNum("");
        payment1.setCcExpDate("hi!");
        payment1.setCcNum("hi!");
        Boolean b8 = payment1.isValid();
        org.junit.Assert.assertTrue("'" + b8 + "' != '" + false + "'", b8.equals(false));
    }

    @Test
    public void test307() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test307");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcNum("hi!");
        payment1.setCcName("hi!");
        Long long7 = payment1.getFee();
        payment1.setCcName("hi!");
        payment1.setFee((Long) 0L);
        payment1.setCcExpDate("hi!");
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 1L + "'", long7.equals(1L));
    }

    @Test
    public void test308() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test308");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) (-1L));
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = payment1.getReservation();
        Long long4 = payment1.getFee();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertNull(reservation3);
        org.junit.Assert.assertTrue("'" + long4 + "' != '" + (-1L) + "'", long4.equals((-1L)));
    }

    @Test
    public void test309() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test309");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        String str10 = payment0.getCcExpDate();
        payment0.setCcExpDate("");
        payment0.setFee((Long) 0L);
        payment0.setCcExpDate("");
        Boolean b17 = payment0.isValid();
        String str18 = payment0.getCcName();
        Long long19 = payment0.getFee();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertTrue("'" + b17 + "' != '" + true + "'", b17.equals(true));
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "hi!" + "'", str18.equals("hi!"));
        org.junit.Assert.assertTrue("'" + long19 + "' != '" + 0L + "'", long19.equals(0L));
    }

    @Test
    public void test310() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test310");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        String str12 = payment0.getCcName();
        String str13 = payment0.getCcNum();
        Boolean b14 = payment0.isValid();
        payment0.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation17 = payment0.getReservation();
        payment0.setFee((Long) 10L);
        payment0.setFee((Long) 0L);
        String str22 = payment0.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation23 = payment0.getReservation();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "hi!" + "'", str12.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "" + "'", str13.equals(""));
        org.junit.Assert.assertTrue("'" + b14 + "' != '" + true + "'", b14.equals(true));
        org.junit.Assert.assertNull(reservation17);
        org.junit.Assert.assertTrue("'" + str22 + "' != '" + "" + "'", str22.equals(""));
        org.junit.Assert.assertNull(reservation23);
    }

    @Test
    public void test311() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test311");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setCcNum("hi!");
        String str9 = payment1.getCcNum();
        Long long10 = payment1.getFee();
        payment1.setCcNum("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = payment1.getReservation();
        payment1.setCcName("");
        payment1.setCcName("");
        Long long18 = payment1.getFee();
        payment1.setFee((Long) 100L);
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        payment1.setFee((Long) 10L);
        payment1.setCcName("");
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "hi!" + "'", str9.equals("hi!"));
        org.junit.Assert.assertTrue("'" + long10 + "' != '" + 1L + "'", long10.equals(1L));
        org.junit.Assert.assertNull(reservation13);
        org.junit.Assert.assertTrue("'" + long18 + "' != '" + 1L + "'", long18.equals(1L));
    }

    @Test
    public void test312() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test312");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment0.getReservation();
        payment0.setCcExpDate("hi!");
        Long long5 = payment0.getFee();
        Long long6 = payment0.getFee();
        payment0.setCcName("");
        payment0.setCcExpDate("hi!");
        payment0.setCcNum("");
        org.junit.Assert.assertNull(long1);
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertNull(long5);
        org.junit.Assert.assertNull(long6);
    }

    @Test
    public void test313() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test313");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        String str10 = payment0.getCcExpDate();
        payment0.setCcExpDate("");
        String str13 = payment0.getCcNum();
        String str14 = payment0.getCcName();
        payment0.setCcName("");
        payment0.setCcExpDate("");
        payment0.setCcNum("hi!");
        Long long21 = payment0.getFee();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "" + "'", str13.equals(""));
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "hi!" + "'", str14.equals("hi!"));
        org.junit.Assert.assertNull(long21);
    }

    @Test
    public void test314() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test314");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        String str12 = payment0.getCcName();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = null;
        payment0.setReservation(reservation13);
        String str15 = payment0.getCcNum();
        String str16 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation17 = null;
        payment0.setReservation(reservation17);
        String str19 = payment0.getCcExpDate();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "hi!" + "'", str12.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "" + "'", str15.equals(""));
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "" + "'", str16.equals(""));
        org.junit.Assert.assertTrue("'" + str19 + "' != '" + "" + "'", str19.equals(""));
    }

    @Test
    public void test315() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test315");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) (-1L));
        payment1.setCcExpDate("hi!");
        payment1.setCcNum("hi!");
        Long long6 = payment1.getFee();
        Long long7 = payment1.getFee();
        org.junit.Assert.assertTrue("'" + long6 + "' != '" + (-1L) + "'", long6.equals((-1L)));
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + (-1L) + "'", long7.equals((-1L)));
    }

    @Test
    public void test316() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test316");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment0.getReservation();
        payment0.setCcName("");
        payment0.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation12 = payment0.getReservation();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "" + "'", str6.equals(""));
        org.junit.Assert.assertNull(reservation7);
        org.junit.Assert.assertNull(reservation12);
    }

    @Test
    public void test317() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test317");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        Boolean b5 = payment1.isValid();
        payment1.setFee((Long) 0L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment1.getReservation();
        Boolean b9 = payment1.isValid();
        Boolean b10 = payment1.isValid();
        String str11 = payment1.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation12 = payment1.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = null;
        payment1.setReservation(reservation13);
        payment1.setCcNum("hi!");
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b5 + "' != '" + false + "'", b5.equals(false));
        org.junit.Assert.assertNull(reservation8);
        org.junit.Assert.assertTrue("'" + b9 + "' != '" + false + "'", b9.equals(false));
        org.junit.Assert.assertTrue("'" + b10 + "' != '" + false + "'", b10.equals(false));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "" + "'", str11.equals(""));
        org.junit.Assert.assertNull(reservation12);
    }

    @Test
    public void test318() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test318");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = null;
        payment1.setReservation(reservation4);
        payment1.setFee((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment1.getReservation();
        String str9 = payment1.getCcName();
        org.junit.Assert.assertNull(reservation8);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "" + "'", str9.equals(""));
    }

    @Test
    public void test319() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test319");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        Boolean b7 = payment1.isValid();
        Boolean b8 = payment1.isValid();
        payment1.setCcNum("");
        payment1.setFee((Long) 0L);
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b7 + "' != '" + false + "'", b7.equals(false));
        org.junit.Assert.assertTrue("'" + b8 + "' != '" + false + "'", b8.equals(false));
    }

    @Test
    public void test320() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test320");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = null;
        payment0.setReservation(reservation5);
    }

    @Test
    public void test321() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test321");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment0.getReservation();
        payment0.setCcExpDate("");
        String str5 = payment0.getCcExpDate();
        payment0.setCcNum("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = null;
        payment0.setReservation(reservation8);
        org.junit.Assert.assertNull(long1);
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "" + "'", str5.equals(""));
    }

    @Test
    public void test322() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test322");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = null;
        payment1.setReservation(reservation4);
        payment1.setCcNum("");
        payment1.setCcName("");
        String str10 = payment1.getCcName();
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
    }

    @Test
    public void test323() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test323");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        Long long4 = payment1.getFee();
        Long long5 = payment1.getFee();
        payment1.setCcExpDate("hi!");
        payment1.setCcExpDate("hi!");
        payment1.setCcExpDate("");
        payment1.setCcName("hi!");
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + long3 + "' != '" + 1L + "'", long3.equals(1L));
        org.junit.Assert.assertTrue("'" + long4 + "' != '" + 1L + "'", long4.equals(1L));
        org.junit.Assert.assertTrue("'" + long5 + "' != '" + 1L + "'", long5.equals(1L));
    }

    @Test
    public void test324() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test324");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        String str12 = payment0.getCcName();
        String str13 = payment0.getCcNum();
        Boolean b14 = payment0.isValid();
        payment0.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation17 = payment0.getReservation();
        payment0.setCcExpDate("hi!");
        Long long20 = payment0.getFee();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "hi!" + "'", str12.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "" + "'", str13.equals(""));
        org.junit.Assert.assertTrue("'" + b14 + "' != '" + true + "'", b14.equals(true));
        org.junit.Assert.assertNull(reservation17);
        org.junit.Assert.assertNull(long20);
    }

    @Test
    public void test325() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test325");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 0L);
        Long long2 = payment1.getFee();
        Boolean b3 = payment1.isValid();
        payment1.setCcName("");
        org.junit.Assert.assertTrue("'" + long2 + "' != '" + 0L + "'", long2.equals(0L));
        org.junit.Assert.assertTrue("'" + b3 + "' != '" + false + "'", b3.equals(false));
    }

    @Test
    public void test326() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test326");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        String str8 = payment0.getCcNum();
        String str9 = payment0.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation10 = payment0.getReservation();
        payment0.setCcExpDate("");
        String str13 = payment0.getCcExpDate();
        String str14 = payment0.getCcExpDate();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "" + "'", str9.equals(""));
        org.junit.Assert.assertNull(reservation10);
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "" + "'", str13.equals(""));
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "" + "'", str14.equals(""));
    }

    @Test
    public void test327() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test327");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = null;
        payment1.setReservation(reservation3);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = null;
        payment1.setReservation(reservation5);
        payment1.setCcNum("hi!");
        Boolean b9 = payment1.isValid();
        payment1.setCcName("");
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + b9 + "' != '" + false + "'", b9.equals(false));
    }

    @Test
    public void test328() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test328");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = null;
        payment1.setReservation(reservation4);
        payment1.setFee((Long) 0L);
        payment1.setCcExpDate("");
        payment1.setCcName("hi!");
        payment1.setCcNum("");
        Boolean b14 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation15 = payment1.getReservation();
        org.junit.Assert.assertTrue("'" + b14 + "' != '" + true + "'", b14.equals(true));
        org.junit.Assert.assertNull(reservation15);
    }

    @Test
    public void test329() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test329");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        String str12 = payment0.getCcName();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = null;
        payment0.setReservation(reservation13);
        Boolean b15 = payment0.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation16 = null;
        payment0.setReservation(reservation16);
        Boolean b18 = payment0.isValid();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "hi!" + "'", str12.equals("hi!"));
        org.junit.Assert.assertTrue("'" + b15 + "' != '" + true + "'", b15.equals(true));
        org.junit.Assert.assertTrue("'" + b18 + "' != '" + true + "'", b18.equals(true));
    }

    @Test
    public void test330() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test330");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setCcNum("hi!");
        String str9 = payment1.getCcNum();
        Long long10 = payment1.getFee();
        payment1.setCcNum("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = payment1.getReservation();
        payment1.setCcName("");
        payment1.setCcName("");
        Long long18 = payment1.getFee();
        payment1.setFee((Long) 100L);
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        payment1.setFee((Long) 10L);
        Boolean b27 = payment1.isValid();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "hi!" + "'", str9.equals("hi!"));
        org.junit.Assert.assertTrue("'" + long10 + "' != '" + 1L + "'", long10.equals(1L));
        org.junit.Assert.assertNull(reservation13);
        org.junit.Assert.assertTrue("'" + long18 + "' != '" + 1L + "'", long18.equals(1L));
        org.junit.Assert.assertTrue("'" + b27 + "' != '" + true + "'", b27.equals(true));
    }

    @Test
    public void test331() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test331");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Boolean b3 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = payment1.getReservation();
        payment1.setCcName("hi!");
        Boolean b7 = payment1.isValid();
        payment1.setFee((Long) 100L);
        Boolean b10 = payment1.isValid();
        payment1.setCcNum("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = null;
        payment1.setReservation(reservation13);
        payment1.setCcExpDate("hi!");
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + b3 + "' != '" + false + "'", b3.equals(false));
        org.junit.Assert.assertNull(reservation4);
        org.junit.Assert.assertTrue("'" + b7 + "' != '" + false + "'", b7.equals(false));
        org.junit.Assert.assertTrue("'" + b10 + "' != '" + false + "'", b10.equals(false));
    }

    @Test
    public void test332() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test332");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        payment0.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment0.setReservation(reservation9);
        String str11 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation12 = payment0.getReservation();
        Long long13 = payment0.getFee();
        payment0.setCcName("");
        payment0.setCcName("hi!");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "" + "'", str6.equals(""));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "" + "'", str11.equals(""));
        org.junit.Assert.assertNull(reservation12);
        org.junit.Assert.assertNull(long13);
    }

    @Test
    public void test333() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test333");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) (-1L));
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = null;
        payment1.setReservation(reservation2);
        payment1.setCcName("hi!");
        Long long6 = payment1.getFee();
        payment1.setFee((Long) 100L);
        org.junit.Assert.assertTrue("'" + long6 + "' != '" + (-1L) + "'", long6.equals((-1L)));
    }

    @Test
    public void test334() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test334");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) (-1L));
        payment1.setFee((Long) 0L);
        payment1.setCcExpDate("hi!");
        payment1.setCcName("hi!");
    }

    @Test
    public void test335() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test335");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        String str12 = payment0.getCcName();
        String str13 = payment0.getCcNum();
        Boolean b14 = payment0.isValid();
        payment0.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation17 = payment0.getReservation();
        Boolean b18 = payment0.isValid();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "hi!" + "'", str12.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "" + "'", str13.equals(""));
        org.junit.Assert.assertTrue("'" + b14 + "' != '" + true + "'", b14.equals(true));
        org.junit.Assert.assertNull(reservation17);
        org.junit.Assert.assertTrue("'" + b18 + "' != '" + true + "'", b18.equals(true));
    }

    @Test
    public void test336() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test336");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setFee((Long) 0L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation11 = null;
        payment1.setReservation(reservation11);
        payment1.setCcName("hi!");
        org.junit.Assert.assertNull(reservation2);
    }

    @Test
    public void test337() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test337");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcExpDate("hi!");
        Boolean b11 = payment1.isValid();
        String str12 = payment1.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = null;
        payment1.setReservation(reservation13);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation15 = payment1.getReservation();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b11 + "' != '" + false + "'", b11.equals(false));
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "hi!" + "'", str12.equals("hi!"));
        org.junit.Assert.assertNull(reservation15);
    }

    @Test
    public void test338() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test338");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = null;
        payment1.setReservation(reservation4);
    }

    @Test
    public void test339() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test339");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        String str5 = payment1.getCcExpDate();
        payment1.setCcExpDate("hi!");
        Boolean b8 = payment1.isValid();
        Boolean b9 = payment1.isValid();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "" + "'", str5.equals(""));
        org.junit.Assert.assertTrue("'" + b8 + "' != '" + false + "'", b8.equals(false));
        org.junit.Assert.assertTrue("'" + b9 + "' != '" + false + "'", b9.equals(false));
    }

    @Test
    public void test340() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test340");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        payment0.setCcNum("hi!");
        payment0.setCcNum("");
        payment0.setCcExpDate("hi!");
        payment0.setCcName("hi!");
    }

    @Test
    public void test341() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test341");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = null;
        payment1.setReservation(reservation5);
        try {
            String str7 = payment1.getCcName();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException");
        } catch (NullPointerException e) {
        }
        org.junit.Assert.assertNull(reservation2);
    }

    @Test
    public void test342() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test342");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        payment1.setCcNum("hi!");
        Long long13 = payment1.getFee();
        payment1.setCcExpDate("");
        String str16 = payment1.getCcNum();
        Long long17 = payment1.getFee();
        Long long18 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation19 = null;
        payment1.setReservation(reservation19);
        Boolean b21 = payment1.isValid();
        Boolean b22 = payment1.isValid();
        payment1.setCcNum("");
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long13 + "' != '" + 100L + "'", long13.equals(100L));
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "hi!" + "'", str16.equals("hi!"));
        org.junit.Assert.assertTrue("'" + long17 + "' != '" + 100L + "'", long17.equals(100L));
        org.junit.Assert.assertTrue("'" + long18 + "' != '" + 100L + "'", long18.equals(100L));
        org.junit.Assert.assertTrue("'" + b21 + "' != '" + true + "'", b21.equals(true));
        org.junit.Assert.assertTrue("'" + b22 + "' != '" + true + "'", b22.equals(true));
    }

    @Test
    public void test343() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test343");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        payment0.setCcNum("hi!");
        payment0.setCcNum("");
        Long long7 = payment0.getFee();
        payment0.setCcNum("");
        org.junit.Assert.assertNull(long7);
    }

    @Test
    public void test344() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test344");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment0.getReservation();
        Long long8 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment0.setReservation(reservation9);
        payment0.setCcName("hi!");
        payment0.setCcExpDate("");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "" + "'", str6.equals(""));
        org.junit.Assert.assertNull(reservation7);
        org.junit.Assert.assertNull(long8);
    }

    @Test
    public void test345() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test345");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment0.getReservation();
        payment0.setCcExpDate("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = null;
        payment0.setReservation(reservation5);
        Boolean b7 = payment0.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = null;
        payment0.setReservation(reservation8);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation10 = null;
        payment0.setReservation(reservation10);
        org.junit.Assert.assertNull(long1);
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b7 + "' != '" + false + "'", b7.equals(false));
    }

    @Test
    public void test346() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test346");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) (-1L));
        payment1.setCcName("");
        payment1.setCcExpDate("");
        String str6 = payment1.getCcExpDate();
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "" + "'", str6.equals(""));
    }

    @Test
    public void test347() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test347");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        String str8 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment0.setReservation(reservation9);
        payment0.setCcName("hi!");
        String str13 = payment0.getCcExpDate();
        payment0.setCcExpDate("hi!");
        Long long16 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation17 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation18 = null;
        payment0.setReservation(reservation18);
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "" + "'", str13.equals(""));
        org.junit.Assert.assertNull(long16);
        org.junit.Assert.assertNull(reservation17);
    }

    @Test
    public void test348() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test348");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        payment0.setCcName("hi!");
        Boolean b14 = payment0.isValid();
        String str15 = payment0.getCcName();
        String str16 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation17 = null;
        payment0.setReservation(reservation17);
        String str19 = payment0.getCcNum();
        String str20 = payment0.getCcNum();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + b14 + "' != '" + true + "'", b14.equals(true));
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "hi!" + "'", str15.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "" + "'", str16.equals(""));
        org.junit.Assert.assertTrue("'" + str19 + "' != '" + "" + "'", str19.equals(""));
        org.junit.Assert.assertTrue("'" + str20 + "' != '" + "" + "'", str20.equals(""));
    }

    @Test
    public void test349() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test349");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        Long long4 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = payment1.getReservation();
        payment1.setCcName("");
        Long long8 = payment1.getFee();
        payment1.setFee((Long) (-1L));
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation11 = payment1.getReservation();
        Long long12 = payment1.getFee();
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + long3 + "' != '" + 1L + "'", long3.equals(1L));
        org.junit.Assert.assertTrue("'" + long4 + "' != '" + 1L + "'", long4.equals(1L));
        org.junit.Assert.assertNull(reservation5);
        org.junit.Assert.assertTrue("'" + long8 + "' != '" + 1L + "'", long8.equals(1L));
        org.junit.Assert.assertNull(reservation11);
        org.junit.Assert.assertTrue("'" + long12 + "' != '" + (-1L) + "'", long12.equals((-1L)));
    }

    @Test
    public void test350() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test350");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        String str10 = payment0.getCcExpDate();
        payment0.setCcExpDate("");
        payment0.setCcNum("hi!");
        String str15 = payment0.getCcExpDate();
        payment0.setFee((Long) (-1L));
        Boolean b18 = payment0.isValid();
        payment0.setCcNum("hi!");
        Boolean b21 = payment0.isValid();
        payment0.setCcExpDate("");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "" + "'", str15.equals(""));
        org.junit.Assert.assertTrue("'" + b18 + "' != '" + true + "'", b18.equals(true));
        org.junit.Assert.assertTrue("'" + b21 + "' != '" + true + "'", b21.equals(true));
    }

    @Test
    public void test351() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test351");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        Boolean b2 = payment0.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = payment0.getReservation();
        Long long4 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = payment0.getReservation();
        payment0.setCcExpDate("");
        Long long8 = payment0.getFee();
        Long long9 = payment0.getFee();
        payment0.setCcNum("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation12 = null;
        payment0.setReservation(reservation12);
        org.junit.Assert.assertNull(long1);
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertNull(reservation3);
        org.junit.Assert.assertNull(long4);
        org.junit.Assert.assertNull(reservation5);
        org.junit.Assert.assertNull(long8);
        org.junit.Assert.assertNull(long9);
    }

    @Test
    public void test352() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test352");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        String str12 = payment0.getCcName();
        String str13 = payment0.getCcNum();
        Boolean b14 = payment0.isValid();
        payment0.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation17 = payment0.getReservation();
        payment0.setFee((Long) 10L);
        String str20 = payment0.getCcNum();
        String str21 = payment0.getCcNum();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "hi!" + "'", str12.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "" + "'", str13.equals(""));
        org.junit.Assert.assertTrue("'" + b14 + "' != '" + true + "'", b14.equals(true));
        org.junit.Assert.assertNull(reservation17);
        org.junit.Assert.assertTrue("'" + str20 + "' != '" + "" + "'", str20.equals(""));
        org.junit.Assert.assertTrue("'" + str21 + "' != '" + "" + "'", str21.equals(""));
    }

    @Test
    public void test353() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test353");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        String str5 = payment1.getCcExpDate();
        payment1.setFee((Long) (-1L));
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = null;
        payment1.setReservation(reservation8);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation10 = null;
        payment1.setReservation(reservation10);
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "" + "'", str5.equals(""));
    }

    @Test
    public void test354() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test354");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        Boolean b4 = payment0.isValid();
        String str5 = payment0.getCcNum();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + b4 + "' != '" + false + "'", b4.equals(false));
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "" + "'", str5.equals(""));
    }

    @Test
    public void test355() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test355");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = null;
        payment1.setReservation(reservation3);
        payment1.setCcExpDate("");
        payment1.setFee((Long) (-1L));
        payment1.setFee((Long) 100L);
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
    }

    @Test
    public void test356() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test356");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment0.getReservation();
        payment0.setCcExpDate("hi!");
        String str11 = payment0.getCcExpDate();
        payment0.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation14 = payment0.getReservation();
        String str15 = payment0.getCcExpDate();
        Boolean b16 = payment0.isValid();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "" + "'", str6.equals(""));
        org.junit.Assert.assertNull(reservation7);
        org.junit.Assert.assertNull(reservation8);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "hi!" + "'", str11.equals("hi!"));
        org.junit.Assert.assertNull(reservation14);
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "" + "'", str15.equals(""));
        org.junit.Assert.assertTrue("'" + b16 + "' != '" + false + "'", b16.equals(false));
    }

    @Test
    public void test357() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test357");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcName("");
        Boolean b7 = payment1.isValid();
        String str8 = payment1.getCcExpDate();
        Boolean b9 = payment1.isValid();
        payment1.setCcNum("hi!");
        payment1.setCcNum("");
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b7 + "' != '" + false + "'", b7.equals(false));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertTrue("'" + b9 + "' != '" + false + "'", b9.equals(false));
    }

    @Test
    public void test358() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test358");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 100L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = null;
        payment1.setReservation(reservation2);
    }

    @Test
    public void test359() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test359");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        String str8 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment0.setReservation(reservation9);
        payment0.setCcName("hi!");
        payment0.setCcName("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation15 = payment0.getReservation();
        String str16 = payment0.getCcExpDate();
        Boolean b17 = payment0.isValid();
        String str18 = payment0.getCcNum();
        payment0.setCcExpDate("");
        payment0.setFee((Long) 100L);
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertNull(reservation15);
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "" + "'", str16.equals(""));
        org.junit.Assert.assertTrue("'" + b17 + "' != '" + true + "'", b17.equals(true));
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "" + "'", str18.equals(""));
    }

    @Test
    public void test360() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test360");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        payment1.setFee((Long) 10L);
        payment1.setCcExpDate("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment1.getReservation();
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + long3 + "' != '" + 1L + "'", long3.equals(1L));
        org.junit.Assert.assertNull(reservation8);
    }

    @Test
    public void test361() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test361");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        String str12 = payment0.getCcName();
        String str13 = payment0.getCcNum();
        Boolean b14 = payment0.isValid();
        payment0.setCcName("");
        payment0.setFee((Long) 10L);
        payment0.setFee((Long) 0L);
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "hi!" + "'", str12.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "" + "'", str13.equals(""));
        org.junit.Assert.assertTrue("'" + b14 + "' != '" + true + "'", b14.equals(true));
    }

    @Test
    public void test362() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test362");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) (-1L));
        payment1.setFee((Long) 0L);
        Boolean b4 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = null;
        payment1.setReservation(reservation5);
        try {
            String str7 = payment1.getCcExpDate();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException");
        } catch (NullPointerException e) {
        }
        org.junit.Assert.assertTrue("'" + b4 + "' != '" + false + "'", b4.equals(false));
    }

    @Test
    public void test363() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test363");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        Boolean b5 = payment1.isValid();
        payment1.setCcNum("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = null;
        payment1.setReservation(reservation8);
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b5 + "' != '" + false + "'", b5.equals(false));
    }

    @Test
    public void test364() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test364");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 100L);
        payment1.setCcName("");
        payment1.setCcExpDate("hi!");
        Long long6 = payment1.getFee();
        org.junit.Assert.assertTrue("'" + long6 + "' != '" + 100L + "'", long6.equals(100L));
    }

    @Test
    public void test365() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test365");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        String str10 = payment0.getCcExpDate();
        String str11 = payment0.getCcNum();
        payment0.setFee((Long) 100L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation14 = null;
        payment0.setReservation(reservation14);
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "" + "'", str11.equals(""));
    }

    @Test
    public void test366() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test366");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = null;
        payment1.setReservation(reservation7);
        String str9 = payment1.getCcExpDate();
        payment1.setFee((Long) 100L);
        Long long12 = payment1.getFee();
        String str13 = payment1.getCcExpDate();
        String str14 = payment1.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation15 = payment1.getReservation();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "" + "'", str9.equals(""));
        org.junit.Assert.assertTrue("'" + long12 + "' != '" + 100L + "'", long12.equals(100L));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "" + "'", str13.equals(""));
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "" + "'", str14.equals(""));
        org.junit.Assert.assertNull(reservation15);
    }

    @Test
    public void test367() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test367");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        Boolean b5 = payment0.isValid();
        String str6 = payment0.getCcExpDate();
        payment0.setCcName("hi!");
        Long long9 = payment0.getFee();
        Boolean b10 = payment0.isValid();
        org.junit.Assert.assertTrue("'" + b5 + "' != '" + false + "'", b5.equals(false));
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "" + "'", str6.equals(""));
        org.junit.Assert.assertNull(long9);
        org.junit.Assert.assertTrue("'" + b10 + "' != '" + true + "'", b10.equals(true));
    }

    @Test
    public void test368() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test368");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment0.getReservation();
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        Long long13 = payment0.getFee();
        String str14 = payment0.getCcNum();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertNull(reservation8);
        org.junit.Assert.assertNull(long13);
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "" + "'", str14.equals(""));
    }

    @Test
    public void test369() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test369");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        payment1.setCcNum("hi!");
        Long long13 = payment1.getFee();
        payment1.setCcExpDate("");
        String str16 = payment1.getCcNum();
        Boolean b17 = payment1.isValid();
        String str18 = payment1.getCcExpDate();
        String str19 = payment1.getCcName();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long13 + "' != '" + 100L + "'", long13.equals(100L));
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "hi!" + "'", str16.equals("hi!"));
        org.junit.Assert.assertTrue("'" + b17 + "' != '" + true + "'", b17.equals(true));
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "" + "'", str18.equals(""));
        org.junit.Assert.assertTrue("'" + str19 + "' != '" + "hi!" + "'", str19.equals("hi!"));
    }

    @Test
    public void test370() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test370");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Boolean b3 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = payment1.getReservation();
        payment1.setCcName("hi!");
        payment1.setCcName("");
        Boolean b9 = payment1.isValid();
        Boolean b10 = payment1.isValid();
        payment1.setCcNum("");
        payment1.setCcNum("hi!");
        payment1.setCcExpDate("hi!");
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + b3 + "' != '" + false + "'", b3.equals(false));
        org.junit.Assert.assertNull(reservation4);
        org.junit.Assert.assertTrue("'" + b9 + "' != '" + false + "'", b9.equals(false));
        org.junit.Assert.assertTrue("'" + b10 + "' != '" + false + "'", b10.equals(false));
    }

    @Test
    public void test371() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test371");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        payment0.setCcName("hi!");
        Long long14 = payment0.getFee();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertNull(long14);
    }

    @Test
    public void test372() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test372");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        String str10 = payment0.getCcExpDate();
        payment0.setCcExpDate("");
        String str13 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation14 = null;
        payment0.setReservation(reservation14);
        String str16 = payment0.getCcNum();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "" + "'", str13.equals(""));
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "" + "'", str16.equals(""));
    }

    @Test
    public void test373() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test373");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        payment1.setCcNum("hi!");
        Long long13 = payment1.getFee();
        String str14 = payment1.getCcExpDate();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long13 + "' != '" + 100L + "'", long13.equals(100L));
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "" + "'", str14.equals(""));
    }

    @Test
    public void test374() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test374");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        payment1.setCcNum("hi!");
        Long long13 = payment1.getFee();
        payment1.setCcExpDate("");
        Long long16 = payment1.getFee();
        payment1.setFee((Long) (-1L));
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation19 = payment1.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation20 = null;
        payment1.setReservation(reservation20);
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long13 + "' != '" + 100L + "'", long13.equals(100L));
        org.junit.Assert.assertTrue("'" + long16 + "' != '" + 100L + "'", long16.equals(100L));
        org.junit.Assert.assertNull(reservation19);
    }

    @Test
    public void test375() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test375");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        payment0.setCcExpDate("");
        payment0.setCcNum("hi!");
        payment0.setFee((Long) 0L);
        payment0.setFee((Long) 100L);
        payment0.setCcNum("hi!");
        payment0.setCcExpDate("");
        payment0.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation21 = payment0.getReservation();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "" + "'", str6.equals(""));
        org.junit.Assert.assertNull(reservation21);
    }

    @Test
    public void test376() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test376");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        String str5 = payment1.getCcExpDate();
        Long long6 = payment1.getFee();
        Long long7 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = null;
        payment1.setReservation(reservation8);
        payment1.setCcNum("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation12 = payment1.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = null;
        payment1.setReservation(reservation13);
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + str5 + "' != '" + "" + "'", str5.equals(""));
        org.junit.Assert.assertTrue("'" + long6 + "' != '" + 1L + "'", long6.equals(1L));
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 1L + "'", long7.equals(1L));
        org.junit.Assert.assertNull(reservation12);
    }

    @Test
    public void test377() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test377");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = null;
        payment1.setReservation(reservation3);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = null;
        payment1.setReservation(reservation5);
        payment1.setCcNum("hi!");
        payment1.setCcExpDate("");
        Long long11 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation12 = payment1.getReservation();
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + long11 + "' != '" + 1L + "'", long11.equals(1L));
        org.junit.Assert.assertNull(reservation12);
    }

    @Test
    public void test378() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test378");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        payment1.setCcNum("hi!");
        Long long13 = payment1.getFee();
        payment1.setCcExpDate("");
        String str16 = payment1.getCcNum();
        Boolean b17 = payment1.isValid();
        payment1.setFee((Long) 1L);
        Long long20 = payment1.getFee();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long13 + "' != '" + 100L + "'", long13.equals(100L));
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "hi!" + "'", str16.equals("hi!"));
        org.junit.Assert.assertTrue("'" + b17 + "' != '" + true + "'", b17.equals(true));
        org.junit.Assert.assertTrue("'" + long20 + "' != '" + 1L + "'", long20.equals(1L));
    }

    @Test
    public void test379() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test379");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        Long long4 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = payment1.getReservation();
        payment1.setCcName("");
        Long long8 = payment1.getFee();
        payment1.setCcExpDate("hi!");
        payment1.setCcName("hi!");
        Boolean b13 = payment1.isValid();
        payment1.setCcExpDate("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation16 = payment1.getReservation();
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + long3 + "' != '" + 1L + "'", long3.equals(1L));
        org.junit.Assert.assertTrue("'" + long4 + "' != '" + 1L + "'", long4.equals(1L));
        org.junit.Assert.assertNull(reservation5);
        org.junit.Assert.assertTrue("'" + long8 + "' != '" + 1L + "'", long8.equals(1L));
        org.junit.Assert.assertTrue("'" + b13 + "' != '" + false + "'", b13.equals(false));
        org.junit.Assert.assertNull(reservation16);
    }

    @Test
    public void test380() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test380");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        payment1.setCcNum("hi!");
        Long long13 = payment1.getFee();
        payment1.setCcExpDate("");
        Long long16 = payment1.getFee();
        payment1.setFee((Long) (-1L));
        payment1.setCcName("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation21 = payment1.getReservation();
        payment1.setCcName("hi!");
        payment1.setCcNum("");
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long13 + "' != '" + 100L + "'", long13.equals(100L));
        org.junit.Assert.assertTrue("'" + long16 + "' != '" + 100L + "'", long16.equals(100L));
        org.junit.Assert.assertNull(reservation21);
    }

    @Test
    public void test381() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test381");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        payment1.setCcNum("hi!");
        Long long13 = payment1.getFee();
        payment1.setCcExpDate("");
        Long long16 = payment1.getFee();
        String str17 = payment1.getCcNum();
        payment1.setFee((Long) 1L);
        String str20 = payment1.getCcNum();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long13 + "' != '" + 100L + "'", long13.equals(100L));
        org.junit.Assert.assertTrue("'" + long16 + "' != '" + 100L + "'", long16.equals(100L));
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "hi!" + "'", str17.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str20 + "' != '" + "hi!" + "'", str20.equals("hi!"));
    }

    @Test
    public void test382() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test382");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        String str12 = payment0.getCcName();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = null;
        payment0.setReservation(reservation13);
        Boolean b15 = payment0.isValid();
        payment0.setCcExpDate("hi!");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "hi!" + "'", str12.equals("hi!"));
        org.junit.Assert.assertTrue("'" + b15 + "' != '" + true + "'", b15.equals(true));
    }

    @Test
    public void test383() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test383");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = null;
        payment1.setReservation(reservation3);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = null;
        payment1.setReservation(reservation5);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment1.getReservation();
        payment1.setCcName("hi!");
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertNull(reservation7);
    }

    @Test
    public void test384() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test384");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = null;
        payment1.setReservation(reservation3);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = null;
        payment1.setReservation(reservation5);
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 0L);
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
    }

    @Test
    public void test385() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test385");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = null;
        payment1.setReservation(reservation3);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = null;
        payment1.setReservation(reservation5);
        Long long7 = payment1.getFee();
        payment1.setCcName("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation10 = payment1.getReservation();
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 1L + "'", long7.equals(1L));
        org.junit.Assert.assertNull(reservation10);
    }

    @Test
    public void test386() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test386");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) (-1L));
        payment1.setCcName("");
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setCcName("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation10 = payment1.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation11 = null;
        payment1.setReservation(reservation11);
        org.junit.Assert.assertNull(reservation10);
    }

    @Test
    public void test387() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test387");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        payment1.setCcNum("hi!");
        Long long13 = payment1.getFee();
        payment1.setCcExpDate("");
        String str16 = payment1.getCcNum();
        Long long17 = payment1.getFee();
        Long long18 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation19 = payment1.getReservation();
        String str20 = payment1.getCcName();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long13 + "' != '" + 100L + "'", long13.equals(100L));
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "hi!" + "'", str16.equals("hi!"));
        org.junit.Assert.assertTrue("'" + long17 + "' != '" + 100L + "'", long17.equals(100L));
        org.junit.Assert.assertTrue("'" + long18 + "' != '" + 100L + "'", long18.equals(100L));
        org.junit.Assert.assertNull(reservation19);
        org.junit.Assert.assertTrue("'" + str20 + "' != '" + "hi!" + "'", str20.equals("hi!"));
    }

    @Test
    public void test388() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test388");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcName("");
        payment0.setCcExpDate("");
        Boolean b8 = payment0.isValid();
        Long long9 = payment0.getFee();
        String str10 = payment0.getCcName();
        payment0.setCcNum("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = null;
        payment0.setReservation(reservation13);
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + b8 + "' != '" + true + "'", b8.equals(true));
        org.junit.Assert.assertNull(long9);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
    }

    @Test
    public void test389() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test389");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        String str12 = payment0.getCcName();
        String str13 = payment0.getCcNum();
        String str14 = payment0.getCcNum();
        payment0.setCcName("");
        String str17 = payment0.getCcNum();
        String str18 = payment0.getCcNum();
        payment0.setCcName("hi!");
        payment0.setCcExpDate("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation23 = null;
        payment0.setReservation(reservation23);
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "hi!" + "'", str12.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "" + "'", str13.equals(""));
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "" + "'", str14.equals(""));
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "" + "'", str17.equals(""));
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "" + "'", str18.equals(""));
    }

    @Test
    public void test390() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test390");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        payment1.setCcNum("");
        payment1.setCcExpDate("hi!");
        payment1.setCcNum("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment1.getReservation();
        payment1.setFee((Long) 1L);
        String str11 = payment1.getCcExpDate();
        org.junit.Assert.assertNull(reservation8);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "hi!" + "'", str11.equals("hi!"));
    }

    @Test
    public void test391() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test391");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        String str10 = payment0.getCcExpDate();
        payment0.setCcExpDate("");
        payment0.setCcNum("hi!");
        String str15 = payment0.getCcExpDate();
        payment0.setFee((Long) (-1L));
        String str18 = payment0.getCcExpDate();
        Long long19 = payment0.getFee();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "" + "'", str15.equals(""));
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "" + "'", str18.equals(""));
        org.junit.Assert.assertTrue("'" + long19 + "' != '" + (-1L) + "'", long19.equals((-1L)));
    }

    @Test
    public void test392() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test392");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        String str10 = payment0.getCcExpDate();
        String str11 = payment0.getCcNum();
        String str12 = payment0.getCcName();
        String str13 = payment0.getCcName();
        payment0.setCcExpDate("");
        Long long16 = payment0.getFee();
        payment0.setFee((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation19 = null;
        payment0.setReservation(reservation19);
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "" + "'", str11.equals(""));
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "hi!" + "'", str12.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "hi!" + "'", str13.equals("hi!"));
        org.junit.Assert.assertNull(long16);
    }

    @Test
    public void test393() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test393");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = null;
        payment0.setReservation(reservation6);
        payment0.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation10 = payment0.getReservation();
        String str11 = payment0.getCcNum();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertNull(reservation10);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "" + "'", str11.equals(""));
    }

    @Test
    public void test394() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test394");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        payment0.setCcExpDate("");
        payment0.setCcNum("hi!");
        payment0.setFee((Long) 0L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = null;
        payment0.setReservation(reservation13);
        payment0.setFee((Long) 100L);
        payment0.setCcNum("hi!");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "" + "'", str6.equals(""));
    }

    @Test
    public void test395() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test395");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        payment0.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment0.setReservation(reservation9);
        String str11 = payment0.getCcNum();
        payment0.setFee((Long) 1L);
        Long long14 = payment0.getFee();
        String str15 = payment0.getCcExpDate();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "" + "'", str6.equals(""));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "" + "'", str11.equals(""));
        org.junit.Assert.assertTrue("'" + long14 + "' != '" + 1L + "'", long14.equals(1L));
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "" + "'", str15.equals(""));
    }

    @Test
    public void test396() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test396");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Boolean b3 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = payment1.getReservation();
        payment1.setCcName("hi!");
        Long long7 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = null;
        payment1.setReservation(reservation8);
        String str10 = payment1.getCcName();
        Long long11 = payment1.getFee();
        Long long12 = payment1.getFee();
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + b3 + "' != '" + false + "'", b3.equals(false));
        org.junit.Assert.assertNull(reservation4);
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 1L + "'", long7.equals(1L));
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "hi!" + "'", str10.equals("hi!"));
        org.junit.Assert.assertTrue("'" + long11 + "' != '" + 1L + "'", long11.equals(1L));
        org.junit.Assert.assertTrue("'" + long12 + "' != '" + 1L + "'", long12.equals(1L));
    }

    @Test
    public void test397() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test397");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setCcNum("hi!");
        String str9 = payment1.getCcNum();
        Long long10 = payment1.getFee();
        payment1.setCcName("hi!");
        payment1.setCcName("");
        payment1.setCcExpDate("");
        String str17 = payment1.getCcName();
        String str18 = payment1.getCcName();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "hi!" + "'", str9.equals("hi!"));
        org.junit.Assert.assertTrue("'" + long10 + "' != '" + 1L + "'", long10.equals(1L));
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "" + "'", str17.equals(""));
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "" + "'", str18.equals(""));
    }

    @Test
    public void test398() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test398");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        Boolean b5 = payment1.isValid();
        payment1.setFee((Long) (-1L));
        String str8 = payment1.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = payment1.getReservation();
        payment1.setCcName("");
        payment1.setFee((Long) (-1L));
        Long long14 = payment1.getFee();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b5 + "' != '" + false + "'", b5.equals(false));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertNull(reservation9);
        org.junit.Assert.assertTrue("'" + long14 + "' != '" + (-1L) + "'", long14.equals((-1L)));
    }

    @Test
    public void test399() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test399");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        Boolean b5 = payment0.isValid();
        String str6 = payment0.getCcExpDate();
        payment0.setFee((Long) 100L);
        payment0.setCcNum("");
        org.junit.Assert.assertTrue("'" + b5 + "' != '" + false + "'", b5.equals(false));
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "" + "'", str6.equals(""));
    }

    @Test
    public void test400() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test400");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = null;
        payment1.setReservation(reservation5);
        payment1.setCcNum("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment1.setReservation(reservation9);
        String str11 = payment1.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation12 = null;
        payment1.setReservation(reservation12);
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "hi!" + "'", str11.equals("hi!"));
    }

    @Test
    public void test401() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test401");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        Boolean b2 = payment0.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = payment0.getReservation();
        payment0.setCcName("");
        payment0.setCcExpDate("hi!");
        String str9 = payment0.getCcName();
        org.junit.Assert.assertNull(long1);
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertNull(reservation3);
        org.junit.Assert.assertNull(reservation4);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "" + "'", str9.equals(""));
    }

    @Test
    public void test402() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test402");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) (-1L));
        payment1.setFee((Long) 0L);
        payment1.setCcExpDate("hi!");
        payment1.setCcExpDate("");
        Long long8 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = payment1.getReservation();
        org.junit.Assert.assertTrue("'" + long8 + "' != '" + 0L + "'", long8.equals(0L));
        org.junit.Assert.assertNull(reservation9);
    }

    @Test
    public void test403() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test403");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        String str10 = payment0.getCcExpDate();
        String str11 = payment0.getCcNum();
        payment0.setFee((Long) 100L);
        String str14 = payment0.getCcNum();
        String str15 = payment0.getCcNum();
        payment0.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation18 = payment0.getReservation();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "" + "'", str11.equals(""));
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "" + "'", str14.equals(""));
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "" + "'", str15.equals(""));
        org.junit.Assert.assertNull(reservation18);
    }

    @Test
    public void test404() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test404");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = null;
        payment1.setReservation(reservation4);
        payment1.setFee((Long) 0L);
        String str8 = payment1.getCcName();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = payment1.getReservation();
        payment1.setCcName("");
        payment1.setCcName("");
        String str14 = payment1.getCcName();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation15 = payment1.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation16 = payment1.getReservation();
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertNull(reservation9);
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "" + "'", str14.equals(""));
        org.junit.Assert.assertNull(reservation15);
        org.junit.Assert.assertNull(reservation16);
    }

    @Test
    public void test405() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test405");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        payment1.setCcNum("");
        Boolean b4 = payment1.isValid();
        Long long5 = payment1.getFee();
        payment1.setCcName("");
        payment1.setCcNum("hi!");
        org.junit.Assert.assertTrue("'" + b4 + "' != '" + false + "'", b4.equals(false));
        org.junit.Assert.assertTrue("'" + long5 + "' != '" + 1L + "'", long5.equals(1L));
    }

    @Test
    public void test406() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test406");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment0.getReservation();
        payment0.setCcExpDate("hi!");
        String str11 = payment0.getCcExpDate();
        payment0.setCcNum("hi!");
        Long long14 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation15 = null;
        payment0.setReservation(reservation15);
        payment0.setFee((Long) (-1L));
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "" + "'", str6.equals(""));
        org.junit.Assert.assertNull(reservation7);
        org.junit.Assert.assertNull(reservation8);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "hi!" + "'", str11.equals("hi!"));
        org.junit.Assert.assertNull(long14);
    }

    @Test
    public void test407() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test407");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        Boolean b5 = payment0.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = null;
        payment0.setReservation(reservation6);
        String str8 = payment0.getCcNum();
        org.junit.Assert.assertTrue("'" + b5 + "' != '" + false + "'", b5.equals(false));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
    }

    @Test
    public void test408() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test408");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = null;
        payment1.setReservation(reservation7);
        String str9 = payment1.getCcExpDate();
        payment1.setFee((Long) 100L);
        Long long12 = payment1.getFee();
        payment1.setFee((Long) 1L);
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "" + "'", str9.equals(""));
        org.junit.Assert.assertTrue("'" + long12 + "' != '" + 100L + "'", long12.equals(100L));
    }

    @Test
    public void test409() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test409");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        String str8 = payment0.getCcNum();
        payment0.setCcNum("hi!");
        String str11 = payment0.getCcExpDate();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "" + "'", str11.equals(""));
    }

    @Test
    public void test410() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test410");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcNum("hi!");
        payment1.setCcName("hi!");
        payment1.setCcExpDate("");
        payment1.setCcName("hi!");
        org.junit.Assert.assertNull(reservation2);
    }

    @Test
    public void test411() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test411");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        payment1.setCcNum("hi!");
        Long long13 = payment1.getFee();
        payment1.setCcExpDate("");
        String str16 = payment1.getCcNum();
        Long long17 = payment1.getFee();
        Long long18 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation19 = null;
        payment1.setReservation(reservation19);
        Boolean b21 = payment1.isValid();
        payment1.setFee((Long) 100L);
        payment1.setFee((Long) 100L);
        String str26 = payment1.getCcNum();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long13 + "' != '" + 100L + "'", long13.equals(100L));
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "hi!" + "'", str16.equals("hi!"));
        org.junit.Assert.assertTrue("'" + long17 + "' != '" + 100L + "'", long17.equals(100L));
        org.junit.Assert.assertTrue("'" + long18 + "' != '" + 100L + "'", long18.equals(100L));
        org.junit.Assert.assertTrue("'" + b21 + "' != '" + true + "'", b21.equals(true));
        org.junit.Assert.assertTrue("'" + str26 + "' != '" + "hi!" + "'", str26.equals("hi!"));
    }

    @Test
    public void test412() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test412");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        String str10 = payment0.getCcExpDate();
        payment0.setCcExpDate("");
        Long long13 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation14 = null;
        payment0.setReservation(reservation14);
        payment0.setFee((Long) 10L);
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertNull(long13);
    }

    @Test
    public void test413() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test413");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) (-1L));
        payment1.setFee((Long) 0L);
        payment1.setCcExpDate("hi!");
        payment1.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment1.getReservation();
        payment1.setCcExpDate("hi!");
        payment1.setCcNum("");
        org.junit.Assert.assertNull(reservation8);
    }

    @Test
    public void test414() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test414");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        payment1.setCcNum("hi!");
        payment1.setCcName("hi!");
        String str15 = payment1.getCcExpDate();
        payment1.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation18 = null;
        payment1.setReservation(reservation18);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation20 = null;
        payment1.setReservation(reservation20);
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "" + "'", str15.equals(""));
    }

    @Test
    public void test415() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test415");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        String str10 = payment0.getCcExpDate();
        payment0.setCcExpDate("");
        payment0.setFee((Long) 0L);
        payment0.setCcExpDate("");
        payment0.setFee((Long) 10L);
        String str19 = payment0.getCcNum();
        payment0.setCcExpDate("hi!");
        String str22 = payment0.getCcName();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertTrue("'" + str19 + "' != '" + "" + "'", str19.equals(""));
        org.junit.Assert.assertTrue("'" + str22 + "' != '" + "hi!" + "'", str22.equals("hi!"));
    }

    @Test
    public void test416() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test416");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        payment1.setCcExpDate("hi!");
        payment1.setCcNum("");
        String str8 = payment1.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment1.setReservation(reservation9);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
    }

    @Test
    public void test417() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test417");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        Boolean b7 = payment1.isValid();
        payment1.setCcNum("");
        payment1.setFee((Long) (-1L));
        Boolean b12 = payment1.isValid();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b7 + "' != '" + false + "'", b7.equals(false));
        org.junit.Assert.assertTrue("'" + b12 + "' != '" + false + "'", b12.equals(false));
    }

    @Test
    public void test418() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test418");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment0.getReservation();
        payment0.setCcExpDate("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = null;
        payment0.setReservation(reservation5);
        Boolean b7 = payment0.isValid();
        String str8 = payment0.getCcExpDate();
        payment0.setCcName("hi!");
        org.junit.Assert.assertNull(long1);
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b7 + "' != '" + false + "'", b7.equals(false));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "hi!" + "'", str8.equals("hi!"));
    }

    @Test
    public void test419() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test419");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        String str10 = payment0.getCcExpDate();
        payment0.setCcExpDate("");
        payment0.setFee((Long) 0L);
        payment0.setCcExpDate("");
        payment0.setFee((Long) 10L);
        payment0.setCcExpDate("");
        payment0.setFee((Long) (-1L));
        String str23 = payment0.getCcExpDate();
        payment0.setCcExpDate("hi!");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertTrue("'" + str23 + "' != '" + "" + "'", str23.equals(""));
    }

    @Test
    public void test420() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test420");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        payment0.setCcExpDate("");
        payment0.setCcNum("hi!");
        payment0.setFee((Long) 0L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = null;
        payment0.setReservation(reservation13);
        payment0.setCcName("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation17 = null;
        payment0.setReservation(reservation17);
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "" + "'", str6.equals(""));
    }

    @Test
    public void test421() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test421");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        String str12 = payment0.getCcName();
        String str13 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation14 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation15 = payment0.getReservation();
        String str16 = payment0.getCcName();
        payment0.setCcNum("hi!");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "hi!" + "'", str12.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "" + "'", str13.equals(""));
        org.junit.Assert.assertNull(reservation14);
        org.junit.Assert.assertNull(reservation15);
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "hi!" + "'", str16.equals("hi!"));
    }

    @Test
    public void test422() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test422");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        payment1.setCcNum("hi!");
        payment1.setCcName("hi!");
        String str15 = payment1.getCcExpDate();
        payment1.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation18 = null;
        payment1.setReservation(reservation18);
        payment1.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation22 = payment1.getReservation();
        payment1.setCcNum("");
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "" + "'", str15.equals(""));
        org.junit.Assert.assertNull(reservation22);
    }

    @Test
    public void test423() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test423");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = null;
        payment1.setReservation(reservation4);
        payment1.setFee((Long) 0L);
        String str8 = payment1.getCcName();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = payment1.getReservation();
        payment1.setCcName("");
        payment1.setCcName("");
        String str14 = payment1.getCcName();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation15 = payment1.getReservation();
        String str16 = payment1.getCcName();
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertNull(reservation9);
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "" + "'", str14.equals(""));
        org.junit.Assert.assertNull(reservation15);
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "" + "'", str16.equals(""));
    }

    @Test
    public void test424() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test424");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        String str8 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment0.setReservation(reservation9);
        payment0.setCcName("hi!");
        String str13 = payment0.getCcExpDate();
        payment0.setCcExpDate("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation16 = payment0.getReservation();
        String str17 = payment0.getCcName();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "" + "'", str13.equals(""));
        org.junit.Assert.assertNull(reservation16);
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "hi!" + "'", str17.equals("hi!"));
    }

    @Test
    public void test425() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test425");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        String str8 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment0.setReservation(reservation9);
        Long long11 = payment0.getFee();
        payment0.setFee((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation14 = payment0.getReservation();
        String str15 = payment0.getCcExpDate();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertNull(long11);
        org.junit.Assert.assertNull(reservation14);
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "" + "'", str15.equals(""));
    }

    @Test
    public void test426() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test426");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        payment1.setFee((Long) 10L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = payment1.getReservation();
        Boolean b7 = payment1.isValid();
        payment1.setCcExpDate("");
        payment1.setFee((Long) (-1L));
        Boolean b12 = payment1.isValid();
        payment1.setFee((Long) 0L);
        Boolean b15 = payment1.isValid();
        Long long16 = payment1.getFee();
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + long3 + "' != '" + 1L + "'", long3.equals(1L));
        org.junit.Assert.assertNull(reservation6);
        org.junit.Assert.assertTrue("'" + b7 + "' != '" + false + "'", b7.equals(false));
        org.junit.Assert.assertTrue("'" + b12 + "' != '" + false + "'", b12.equals(false));
        org.junit.Assert.assertTrue("'" + b15 + "' != '" + false + "'", b15.equals(false));
        org.junit.Assert.assertTrue("'" + long16 + "' != '" + 0L + "'", long16.equals(0L));
    }

    @Test
    public void test427() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test427");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = null;
        payment1.setReservation(reservation4);
        payment1.setFee((Long) (-1L));
        payment1.setFee((Long) 1L);
        payment1.setCcExpDate("hi!");
        payment1.setCcExpDate("");
        String str14 = payment1.getCcName();
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "" + "'", str14.equals(""));
    }

    @Test
    public void test428() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test428");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setCcNum("hi!");
        String str9 = payment1.getCcNum();
        Long long10 = payment1.getFee();
        payment1.setCcName("hi!");
        payment1.setCcName("");
        payment1.setCcExpDate("");
        String str17 = payment1.getCcName();
        String str18 = payment1.getCcNum();
        payment1.setCcExpDate("");
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "hi!" + "'", str9.equals("hi!"));
        org.junit.Assert.assertTrue("'" + long10 + "' != '" + 1L + "'", long10.equals(1L));
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "" + "'", str17.equals(""));
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "hi!" + "'", str18.equals("hi!"));
    }

    @Test
    public void test429() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test429");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        String str10 = payment0.getCcExpDate();
        payment0.setCcExpDate("");
        payment0.setCcNum("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation15 = payment0.getReservation();
        payment0.setFee((Long) 100L);
        String str18 = payment0.getCcExpDate();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertNull(reservation15);
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "" + "'", str18.equals(""));
    }

    @Test
    public void test430() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test430");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        payment1.setCcNum("hi!");
        Long long13 = payment1.getFee();
        payment1.setCcExpDate("");
        String str16 = payment1.getCcNum();
        Long long17 = payment1.getFee();
        Long long18 = payment1.getFee();
        String str19 = payment1.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation20 = null;
        payment1.setReservation(reservation20);
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long13 + "' != '" + 100L + "'", long13.equals(100L));
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "hi!" + "'", str16.equals("hi!"));
        org.junit.Assert.assertTrue("'" + long17 + "' != '" + 100L + "'", long17.equals(100L));
        org.junit.Assert.assertTrue("'" + long18 + "' != '" + 100L + "'", long18.equals(100L));
        org.junit.Assert.assertTrue("'" + str19 + "' != '" + "hi!" + "'", str19.equals("hi!"));
    }

    @Test
    public void test431() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test431");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment0.getReservation();
        payment0.setCcExpDate("hi!");
        Long long5 = payment0.getFee();
        Long long6 = payment0.getFee();
        payment0.setCcName("");
        payment0.setCcExpDate("hi!");
        String str11 = payment0.getCcExpDate();
        String str12 = payment0.getCcExpDate();
        org.junit.Assert.assertNull(long1);
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertNull(long5);
        org.junit.Assert.assertNull(long6);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "hi!" + "'", str11.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "hi!" + "'", str12.equals("hi!"));
    }

    @Test
    public void test432() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test432");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = null;
        payment1.setReservation(reservation4);
        payment1.setFee((Long) (-1L));
        Boolean b8 = payment1.isValid();
        String str9 = payment1.getCcName();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation10 = payment1.getReservation();
        String str11 = payment1.getCcName();
        org.junit.Assert.assertTrue("'" + b8 + "' != '" + false + "'", b8.equals(false));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "" + "'", str9.equals(""));
        org.junit.Assert.assertNull(reservation10);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "" + "'", str11.equals(""));
    }

    @Test
    public void test433() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test433");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = null;
        payment1.setReservation(reservation3);
        payment1.setCcName("");
        Long long7 = payment1.getFee();
        String str8 = payment1.getCcName();
        Boolean b9 = payment1.isValid();
        Long long10 = payment1.getFee();
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 1L + "'", long7.equals(1L));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertTrue("'" + b9 + "' != '" + false + "'", b9.equals(false));
        org.junit.Assert.assertTrue("'" + long10 + "' != '" + 1L + "'", long10.equals(1L));
    }

    @Test
    public void test434() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test434");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcNum("hi!");
        payment1.setCcName("hi!");
        Long long7 = payment1.getFee();
        String str8 = payment1.getCcNum();
        String str9 = payment1.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation10 = null;
        payment1.setReservation(reservation10);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation12 = null;
        payment1.setReservation(reservation12);
        payment1.setCcNum("hi!");
        String str16 = payment1.getCcName();
        Boolean b17 = payment1.isValid();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 1L + "'", long7.equals(1L));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "hi!" + "'", str8.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "hi!" + "'", str9.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "hi!" + "'", str16.equals("hi!"));
        org.junit.Assert.assertTrue("'" + b17 + "' != '" + false + "'", b17.equals(false));
    }

    @Test
    public void test435() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test435");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        Boolean b2 = payment0.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = payment0.getReservation();
        Long long4 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = null;
        payment0.setReservation(reservation5);
        org.junit.Assert.assertNull(long1);
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertNull(reservation3);
        org.junit.Assert.assertNull(long4);
    }

    @Test
    public void test436() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test436");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment0.getReservation();
        payment0.setCcName("hi!");
        Long long5 = payment0.getFee();
        org.junit.Assert.assertNull(long1);
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertNull(long5);
    }

    @Test
    public void test437() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test437");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = null;
        payment1.setReservation(reservation4);
        payment1.setFee((Long) (-1L));
        Boolean b8 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = payment1.getReservation();
        payment1.setCcName("");
        payment1.setFee((Long) (-1L));
        org.junit.Assert.assertTrue("'" + b8 + "' != '" + false + "'", b8.equals(false));
        org.junit.Assert.assertNull(reservation9);
    }

    @Test
    public void test438() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test438");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        Boolean b2 = payment0.isValid();
        payment0.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = null;
        payment0.setReservation(reservation6);
        Boolean b8 = payment0.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = payment0.getReservation();
        try {
            String str10 = payment0.getCcExpDate();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException");
        } catch (NullPointerException e) {
        }
        org.junit.Assert.assertNull(long1);
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertNull(reservation5);
        org.junit.Assert.assertTrue("'" + b8 + "' != '" + false + "'", b8.equals(false));
        org.junit.Assert.assertNull(reservation9);
    }

    @Test
    public void test439() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test439");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        payment0.setCcExpDate("");
        payment0.setCcNum("hi!");
        payment0.setCcName("");
        Boolean b13 = payment0.isValid();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "" + "'", str6.equals(""));
        org.junit.Assert.assertTrue("'" + b13 + "' != '" + true + "'", b13.equals(true));
    }

    @Test
    public void test440() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test440");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        Boolean b2 = payment0.isValid();
        Boolean b3 = payment0.isValid();
        payment0.setCcName("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = null;
        payment0.setReservation(reservation6);
        String str8 = payment0.getCcName();
        payment0.setCcNum("");
        org.junit.Assert.assertNull(long1);
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + b3 + "' != '" + false + "'", b3.equals(false));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "hi!" + "'", str8.equals("hi!"));
    }

    @Test
    public void test441() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test441");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        String str8 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment0.setReservation(reservation9);
        payment0.setCcName("hi!");
        payment0.setCcName("hi!");
        Long long15 = payment0.getFee();
        payment0.setCcName("hi!");
        payment0.setFee((Long) (-1L));
        String str20 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation21 = null;
        payment0.setReservation(reservation21);
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertNull(long15);
        org.junit.Assert.assertTrue("'" + str20 + "' != '" + "" + "'", str20.equals(""));
    }

    @Test
    public void test442() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test442");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = null;
        payment1.setReservation(reservation3);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = null;
        payment1.setReservation(reservation5);
        payment1.setCcNum("hi!");
        Boolean b9 = payment1.isValid();
        Long long10 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation11 = payment1.getReservation();
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + b9 + "' != '" + false + "'", b9.equals(false));
        org.junit.Assert.assertTrue("'" + long10 + "' != '" + 1L + "'", long10.equals(1L));
        org.junit.Assert.assertNull(reservation11);
    }

    @Test
    public void test443() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test443");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        Boolean b2 = payment0.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation3 = payment0.getReservation();
        Long long4 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = payment0.getReservation();
        Long long6 = payment0.getFee();
        payment0.setCcName("hi!");
        org.junit.Assert.assertNull(long1);
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertNull(reservation3);
        org.junit.Assert.assertNull(long4);
        org.junit.Assert.assertNull(reservation5);
        org.junit.Assert.assertNull(long6);
    }

    @Test
    public void test444() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test444");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        Boolean b5 = payment1.isValid();
        payment1.setFee((Long) (-1L));
        String str8 = payment1.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment1.setReservation(reservation9);
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b5 + "' != '" + false + "'", b5.equals(false));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
    }

    @Test
    public void test445() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test445");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment0.getReservation();
        payment0.setCcExpDate("hi!");
        String str11 = payment0.getCcExpDate();
        payment0.setCcNum("hi!");
        Long long14 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation15 = null;
        payment0.setReservation(reservation15);
        payment0.setCcName("hi!");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "" + "'", str6.equals(""));
        org.junit.Assert.assertNull(reservation7);
        org.junit.Assert.assertNull(reservation8);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "hi!" + "'", str11.equals("hi!"));
        org.junit.Assert.assertNull(long14);
    }

    @Test
    public void test446() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test446");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcNum("hi!");
        payment1.setCcName("hi!");
        Long long7 = payment1.getFee();
        payment1.setCcName("hi!");
        String str10 = payment1.getCcNum();
        Long long11 = payment1.getFee();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 1L + "'", long7.equals(1L));
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "hi!" + "'", str10.equals("hi!"));
        org.junit.Assert.assertTrue("'" + long11 + "' != '" + 1L + "'", long11.equals(1L));
    }

    @Test
    public void test447() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test447");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = null;
        payment1.setReservation(reservation4);
        payment1.setFee((Long) 0L);
        String str8 = payment1.getCcName();
        payment1.setCcNum("");
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
    }

    @Test
    public void test448() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test448");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        Boolean b5 = payment1.isValid();
        payment1.setFee((Long) 0L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment1.getReservation();
        payment1.setCcExpDate("hi!");
        payment1.setCcExpDate("");
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b5 + "' != '" + false + "'", b5.equals(false));
        org.junit.Assert.assertNull(reservation8);
    }

    @Test
    public void test449() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test449");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcName("");
        payment0.setCcExpDate("");
        Boolean b8 = payment0.isValid();
        Long long9 = payment0.getFee();
        String str10 = payment0.getCcName();
        payment0.setCcNum("hi!");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + b8 + "' != '" + true + "'", b8.equals(true));
        org.junit.Assert.assertNull(long9);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
    }

    @Test
    public void test450() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test450");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcExpDate("hi!");
        Boolean b11 = payment1.isValid();
        payment1.setCcNum("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation14 = null;
        payment1.setReservation(reservation14);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation16 = null;
        payment1.setReservation(reservation16);
        payment1.setCcName("");
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b11 + "' != '" + false + "'", b11.equals(false));
    }

    @Test
    public void test451() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test451");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        Boolean b5 = payment1.isValid();
        payment1.setFee((Long) 0L);
        String str8 = payment1.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment1.setReservation(reservation9);
        String str11 = payment1.getCcExpDate();
        Long long12 = payment1.getFee();
        try {
            String str13 = payment1.getCcNum();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException");
        } catch (NullPointerException e) {
        }
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b5 + "' != '" + false + "'", b5.equals(false));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "" + "'", str11.equals(""));
        org.junit.Assert.assertTrue("'" + long12 + "' != '" + 0L + "'", long12.equals(0L));
    }

    @Test
    public void test452() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test452");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        payment0.setCcExpDate("");
        payment0.setCcNum("hi!");
        payment0.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = payment0.getReservation();
        payment0.setCcNum("");
        String str16 = payment0.getCcName();
        Boolean b17 = payment0.isValid();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "" + "'", str6.equals(""));
        org.junit.Assert.assertNull(reservation13);
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "" + "'", str16.equals(""));
        org.junit.Assert.assertTrue("'" + b17 + "' != '" + true + "'", b17.equals(true));
    }

    @Test
    public void test453() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test453");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        payment1.setFee((Long) 10L);
        payment1.setCcNum("hi!");
        payment1.setCcNum("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation10 = payment1.getReservation();
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + long3 + "' != '" + 1L + "'", long3.equals(1L));
        org.junit.Assert.assertNull(reservation10);
    }

    @Test
    public void test454() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test454");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment0.getReservation();
        Boolean b9 = payment0.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation10 = null;
        payment0.setReservation(reservation10);
        Long long12 = payment0.getFee();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "" + "'", str6.equals(""));
        org.junit.Assert.assertNull(reservation7);
        org.junit.Assert.assertNull(reservation8);
        org.junit.Assert.assertTrue("'" + b9 + "' != '" + false + "'", b9.equals(false));
        org.junit.Assert.assertNull(long12);
    }

    @Test
    public void test455() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test455");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = null;
        payment1.setReservation(reservation7);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = payment1.getReservation();
        payment1.setCcExpDate("hi!");
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertNull(reservation9);
    }

    @Test
    public void test456() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test456");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setCcNum("hi!");
        String str9 = payment1.getCcNum();
        Long long10 = payment1.getFee();
        payment1.setCcNum("");
        Long long13 = payment1.getFee();
        Boolean b14 = payment1.isValid();
        payment1.setFee((Long) (-1L));
        payment1.setCcExpDate("hi!");
        payment1.setCcName("");
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "hi!" + "'", str9.equals("hi!"));
        org.junit.Assert.assertTrue("'" + long10 + "' != '" + 1L + "'", long10.equals(1L));
        org.junit.Assert.assertTrue("'" + long13 + "' != '" + 1L + "'", long13.equals(1L));
        org.junit.Assert.assertTrue("'" + b14 + "' != '" + false + "'", b14.equals(false));
    }

    @Test
    public void test457() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test457");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        Boolean b5 = payment1.isValid();
        payment1.setFee((Long) (-1L));
        String str8 = payment1.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = payment1.getReservation();
        payment1.setFee((Long) 10L);
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b5 + "' != '" + false + "'", b5.equals(false));
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertNull(reservation9);
    }

    @Test
    public void test458() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test458");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        payment1.setCcNum("");
        payment1.setCcExpDate("hi!");
        Boolean b6 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation7 = null;
        payment1.setReservation(reservation7);
        payment1.setFee((Long) 100L);
        payment1.setCcNum("");
        payment1.setCcNum("");
        org.junit.Assert.assertTrue("'" + b6 + "' != '" + false + "'", b6.equals(false));
    }

    @Test
    public void test459() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test459");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Boolean b3 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = payment1.getReservation();
        payment1.setCcName("hi!");
        Long long7 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = null;
        payment1.setReservation(reservation8);
        payment1.setFee((Long) 10L);
        payment1.setFee((Long) 100L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation14 = null;
        payment1.setReservation(reservation14);
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + b3 + "' != '" + false + "'", b3.equals(false));
        org.junit.Assert.assertNull(reservation4);
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 1L + "'", long7.equals(1L));
    }

    @Test
    public void test460() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test460");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) (-1L));
        payment1.setCcName("");
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        Long long8 = payment1.getFee();
        Long long9 = payment1.getFee();
        org.junit.Assert.assertTrue("'" + long8 + "' != '" + (-1L) + "'", long8.equals((-1L)));
        org.junit.Assert.assertTrue("'" + long9 + "' != '" + (-1L) + "'", long9.equals((-1L)));
    }

    @Test
    public void test461() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test461");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcExpDate("");
        payment1.setFee((Long) 0L);
        String str6 = payment1.getCcExpDate();
        payment1.setCcName("");
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "" + "'", str6.equals(""));
    }

    @Test
    public void test462() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test462");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        String str12 = payment0.getCcName();
        String str13 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation14 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation15 = payment0.getReservation();
        String str16 = payment0.getCcName();
        String str17 = payment0.getCcName();
        payment0.setCcNum("hi!");
        Boolean b20 = payment0.isValid();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "hi!" + "'", str12.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "" + "'", str13.equals(""));
        org.junit.Assert.assertNull(reservation14);
        org.junit.Assert.assertNull(reservation15);
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "hi!" + "'", str16.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "hi!" + "'", str17.equals("hi!"));
        org.junit.Assert.assertTrue("'" + b20 + "' != '" + true + "'", b20.equals(true));
    }

    @Test
    public void test463() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test463");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        String str6 = payment0.getCcNum();
        payment0.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment0.setReservation(reservation9);
        payment0.setCcExpDate("hi!");
        payment0.setCcName("");
        Boolean b15 = payment0.isValid();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "" + "'", str6.equals(""));
        org.junit.Assert.assertTrue("'" + b15 + "' != '" + true + "'", b15.equals(true));
    }

    @Test
    public void test464() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test464");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = null;
        payment1.setReservation(reservation4);
        payment1.setFee((Long) (-1L));
        payment1.setFee((Long) 1L);
        payment1.setCcExpDate("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation12 = null;
        payment1.setReservation(reservation12);
        payment1.setFee((Long) 0L);
    }

    @Test
    public void test465() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test465");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment0.getReservation();
        payment0.setCcExpDate("hi!");
        Long long5 = payment0.getFee();
        String str6 = payment0.getCcExpDate();
        org.junit.Assert.assertNull(long1);
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertNull(long5);
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "hi!" + "'", str6.equals("hi!"));
    }

    @Test
    public void test466() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test466");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = null;
        payment1.setReservation(reservation5);
        Long long7 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = null;
        payment1.setReservation(reservation8);
        String str10 = payment1.getCcExpDate();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 1L + "'", long7.equals(1L));
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
    }

    @Test
    public void test467() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test467");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        Boolean b5 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = null;
        payment1.setReservation(reservation6);
        payment1.setCcName("");
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b5 + "' != '" + false + "'", b5.equals(false));
    }

    @Test
    public void test468() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test468");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        payment1.setCcNum("");
        payment1.setCcExpDate("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = payment1.getReservation();
        org.junit.Assert.assertNull(reservation6);
    }

    @Test
    public void test469() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test469");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setFee((Long) 100L);
        payment1.setCcName("hi!");
        payment1.setCcNum("hi!");
        Long long13 = payment1.getFee();
        payment1.setCcExpDate("");
        String str16 = payment1.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation17 = null;
        payment1.setReservation(reservation17);
        Long long19 = payment1.getFee();
        Boolean b20 = payment1.isValid();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long13 + "' != '" + 100L + "'", long13.equals(100L));
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "hi!" + "'", str16.equals("hi!"));
        org.junit.Assert.assertTrue("'" + long19 + "' != '" + 100L + "'", long19.equals(100L));
        org.junit.Assert.assertTrue("'" + b20 + "' != '" + true + "'", b20.equals(true));
    }

    @Test
    public void test470() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test470");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        payment1.setCcNum("hi!");
        String str9 = payment1.getCcNum();
        Long long10 = payment1.getFee();
        payment1.setCcNum("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = payment1.getReservation();
        payment1.setCcName("");
        payment1.setCcName("");
        Long long18 = payment1.getFee();
        payment1.setFee((Long) 100L);
        String str21 = payment1.getCcName();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation22 = payment1.getReservation();
        payment1.setFee((Long) 0L);
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "hi!" + "'", str9.equals("hi!"));
        org.junit.Assert.assertTrue("'" + long10 + "' != '" + 1L + "'", long10.equals(1L));
        org.junit.Assert.assertNull(reservation13);
        org.junit.Assert.assertTrue("'" + long18 + "' != '" + 1L + "'", long18.equals(1L));
        org.junit.Assert.assertTrue("'" + str21 + "' != '" + "" + "'", str21.equals(""));
        org.junit.Assert.assertNull(reservation22);
    }

    @Test
    public void test471() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test471");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        String str10 = payment0.getCcExpDate();
        payment0.setCcNum("hi!");
        String str13 = payment0.getCcName();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation14 = null;
        payment0.setReservation(reservation14);
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "hi!" + "'", str13.equals("hi!"));
    }

    @Test
    public void test472() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test472");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        String str10 = payment0.getCcExpDate();
        String str11 = payment0.getCcName();
        payment0.setCcExpDate("");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "hi!" + "'", str11.equals("hi!"));
    }

    @Test
    public void test473() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test473");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        String str8 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment0.setReservation(reservation9);
        payment0.setCcName("hi!");
        payment0.setCcName("hi!");
        String str15 = payment0.getCcExpDate();
        Long long16 = payment0.getFee();
        payment0.setCcExpDate("hi!");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "" + "'", str15.equals(""));
        org.junit.Assert.assertNull(long16);
    }

    @Test
    public void test474() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test474");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = null;
        payment1.setReservation(reservation4);
        payment1.setFee((Long) 0L);
        String str8 = payment1.getCcName();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = payment1.getReservation();
        payment1.setCcName("hi!");
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertNull(reservation9);
    }

    @Test
    public void test475() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test475");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        Long long7 = payment1.getFee();
        Long long8 = payment1.getFee();
        payment1.setCcNum("");
        String str11 = payment1.getCcNum();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 1L + "'", long7.equals(1L));
        org.junit.Assert.assertTrue("'" + long8 + "' != '" + 1L + "'", long8.equals(1L));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "" + "'", str11.equals(""));
    }

    @Test
    public void test476() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test476");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) (-1L));
        payment1.setFee((Long) 0L);
        payment1.setCcExpDate("hi!");
        payment1.setCcExpDate("");
        Long long8 = payment1.getFee();
        String str9 = payment1.getCcExpDate();
        payment1.setCcNum("hi!");
        org.junit.Assert.assertTrue("'" + long8 + "' != '" + 0L + "'", long8.equals(0L));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "" + "'", str9.equals(""));
    }

    @Test
    public void test477() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test477");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        String str12 = payment0.getCcName();
        String str13 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation14 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation15 = payment0.getReservation();
        String str16 = payment0.getCcName();
        String str17 = payment0.getCcName();
        String str18 = payment0.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation19 = payment0.getReservation();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "hi!" + "'", str12.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "" + "'", str13.equals(""));
        org.junit.Assert.assertNull(reservation14);
        org.junit.Assert.assertNull(reservation15);
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "hi!" + "'", str16.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "hi!" + "'", str17.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "" + "'", str18.equals(""));
        org.junit.Assert.assertNull(reservation19);
    }

    @Test
    public void test478() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test478");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = null;
        payment1.setReservation(reservation4);
        payment1.setFee((Long) (-1L));
        Boolean b8 = payment1.isValid();
        String str9 = payment1.getCcName();
        Boolean b10 = payment1.isValid();
        String str11 = payment1.getCcName();
        try {
            String str12 = payment1.getCcNum();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException");
        } catch (NullPointerException e) {
        }
        org.junit.Assert.assertTrue("'" + b8 + "' != '" + false + "'", b8.equals(false));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "" + "'", str9.equals(""));
        org.junit.Assert.assertTrue("'" + b10 + "' != '" + false + "'", b10.equals(false));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "" + "'", str11.equals(""));
    }

    @Test
    public void test479() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test479");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = null;
        payment1.setReservation(reservation4);
        payment1.setFee((Long) (-1L));
        payment1.setFee((Long) 1L);
        payment1.setCcExpDate("hi!");
        payment1.setCcExpDate("");
        String str14 = payment1.getCcExpDate();
        Long long15 = payment1.getFee();
        payment1.setCcName("hi!");
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "" + "'", str14.equals(""));
        org.junit.Assert.assertTrue("'" + long15 + "' != '" + 1L + "'", long15.equals(1L));
    }

    @Test
    public void test480() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test480");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        payment0.setCcName("hi!");
        Boolean b14 = payment0.isValid();
        String str15 = payment0.getCcName();
        String str16 = payment0.getCcExpDate();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + b14 + "' != '" + true + "'", b14.equals(true));
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "hi!" + "'", str15.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str16 + "' != '" + "" + "'", str16.equals(""));
    }

    @Test
    public void test481() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test481");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        String str12 = payment0.getCcName();
        String str13 = payment0.getCcNum();
        String str14 = payment0.getCcNum();
        String str15 = payment0.getCcExpDate();
        payment0.setCcNum("");
        String str18 = payment0.getCcExpDate();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "hi!" + "'", str12.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "" + "'", str13.equals(""));
        org.junit.Assert.assertTrue("'" + str14 + "' != '" + "" + "'", str14.equals(""));
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "" + "'", str15.equals(""));
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "" + "'", str18.equals(""));
    }

    @Test
    public void test482() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test482");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Boolean b3 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = payment1.getReservation();
        payment1.setCcName("hi!");
        Long long7 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = null;
        payment1.setReservation(reservation8);
        payment1.setFee((Long) 10L);
        payment1.setFee((Long) 100L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation14 = payment1.getReservation();
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + b3 + "' != '" + false + "'", b3.equals(false));
        org.junit.Assert.assertNull(reservation4);
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 1L + "'", long7.equals(1L));
        org.junit.Assert.assertNull(reservation14);
    }

    @Test
    public void test483() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test483");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation4 = null;
        payment1.setReservation(reservation4);
        payment1.setFee((Long) 0L);
        Long long8 = payment1.getFee();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment1.setReservation(reservation9);
        org.junit.Assert.assertTrue("'" + long8 + "' != '" + 0L + "'", long8.equals(0L));
    }

    @Test
    public void test484() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test484");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcExpDate("hi!");
        payment1.setCcExpDate("");
        String str9 = payment1.getCcExpDate();
        payment1.setCcExpDate("");
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "" + "'", str9.equals(""));
    }

    @Test
    public void test485() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test485");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        Long long1 = payment0.getFee();
        Boolean b2 = payment0.isValid();
        Boolean b3 = payment0.isValid();
        payment0.setCcNum("hi!");
        payment0.setCcName("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = null;
        payment0.setReservation(reservation8);
        try {
            String str10 = payment0.getCcExpDate();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException");
        } catch (NullPointerException e) {
        }
        org.junit.Assert.assertNull(long1);
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + b3 + "' != '" + false + "'", b3.equals(false));
    }

    @Test
    public void test486() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test486");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        String str12 = payment0.getCcName();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = null;
        payment0.setReservation(reservation13);
        String str15 = payment0.getCcNum();
        Boolean b16 = payment0.isValid();
        payment0.setFee((Long) 1L);
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "hi!" + "'", str12.equals("hi!"));
        org.junit.Assert.assertTrue("'" + str15 + "' != '" + "" + "'", str15.equals(""));
        org.junit.Assert.assertTrue("'" + b16 + "' != '" + true + "'", b16.equals(true));
    }

    @Test
    public void test487() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test487");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        payment0.setCcNum("");
        payment0.setCcName("hi!");
        Boolean b14 = payment0.isValid();
        Boolean b15 = payment0.isValid();
        Long long16 = payment0.getFee();
        String str17 = payment0.getCcNum();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + b14 + "' != '" + true + "'", b14.equals(true));
        org.junit.Assert.assertTrue("'" + b15 + "' != '" + true + "'", b15.equals(true));
        org.junit.Assert.assertNull(long16);
        org.junit.Assert.assertTrue("'" + str17 + "' != '" + "" + "'", str17.equals(""));
    }

    @Test
    public void test488() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test488");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        String str8 = payment0.getCcNum();
        String str9 = payment0.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation10 = payment0.getReservation();
        String str11 = payment0.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation12 = payment0.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = null;
        payment0.setReservation(reservation13);
        try {
            String str15 = payment0.getCcName();
            org.junit.Assert.fail("Expected exception of type java.lang.NullPointerException");
        } catch (NullPointerException e) {
        }
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str8 + "' != '" + "" + "'", str8.equals(""));
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "" + "'", str9.equals(""));
        org.junit.Assert.assertNull(reservation10);
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "" + "'", str11.equals(""));
        org.junit.Assert.assertNull(reservation12);
    }

    @Test
    public void test489() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test489");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        payment1.setCcNum("hi!");
        payment1.setCcNum("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment1.getReservation();
        String str9 = payment1.getCcNum();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation10 = payment1.getReservation();
        org.junit.Assert.assertNull(reservation8);
        org.junit.Assert.assertTrue("'" + str9 + "' != '" + "" + "'", str9.equals(""));
        org.junit.Assert.assertNull(reservation10);
    }

    @Test
    public void test490() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test490");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        Long long7 = payment1.getFee();
        payment1.setFee((Long) (-1L));
        payment1.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation12 = null;
        payment1.setReservation(reservation12);
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 1L + "'", long7.equals(1L));
    }

    @Test
    public void test491() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test491");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        Boolean b2 = payment1.isValid();
        Long long3 = payment1.getFee();
        payment1.setFee((Long) 10L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation6 = payment1.getReservation();
        Boolean b7 = payment1.isValid();
        payment1.setCcExpDate("");
        payment1.setCcExpDate("hi!");
        payment1.setCcName("hi!");
        org.junit.Assert.assertTrue("'" + b2 + "' != '" + false + "'", b2.equals(false));
        org.junit.Assert.assertTrue("'" + long3 + "' != '" + 1L + "'", long3.equals(1L));
        org.junit.Assert.assertNull(reservation6);
        org.junit.Assert.assertTrue("'" + b7 + "' != '" + false + "'", b7.equals(false));
    }

    @Test
    public void test492() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test492");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        String str10 = payment0.getCcExpDate();
        payment0.setCcExpDate("");
        payment0.setFee((Long) 0L);
        payment0.setCcExpDate("");
        Boolean b17 = payment0.isValid();
        String str18 = payment0.getCcExpDate();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertTrue("'" + b17 + "' != '" + true + "'", b17.equals(true));
        org.junit.Assert.assertTrue("'" + str18 + "' != '" + "" + "'", str18.equals(""));
    }

    @Test
    public void test493() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test493");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        Boolean b5 = payment0.isValid();
        String str6 = payment0.getCcExpDate();
        payment0.setFee((Long) 100L);
        payment0.setFee((Long) 10L);
        org.junit.Assert.assertTrue("'" + b5 + "' != '" + false + "'", b5.equals(false));
        org.junit.Assert.assertTrue("'" + str6 + "' != '" + "" + "'", str6.equals(""));
    }

    @Test
    public void test494() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test494");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        Boolean b5 = payment1.isValid();
        payment1.setFee((Long) 0L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment1.getReservation();
        Boolean b9 = payment1.isValid();
        Boolean b10 = payment1.isValid();
        String str11 = payment1.getCcExpDate();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation12 = payment1.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation13 = null;
        payment1.setReservation(reservation13);
        Boolean b15 = payment1.isValid();
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b5 + "' != '" + false + "'", b5.equals(false));
        org.junit.Assert.assertNull(reservation8);
        org.junit.Assert.assertTrue("'" + b9 + "' != '" + false + "'", b9.equals(false));
        org.junit.Assert.assertTrue("'" + b10 + "' != '" + false + "'", b10.equals(false));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "" + "'", str11.equals(""));
        org.junit.Assert.assertNull(reservation12);
        org.junit.Assert.assertTrue("'" + b15 + "' != '" + false + "'", b15.equals(false));
    }

    @Test
    public void test495() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test495");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation5 = null;
        payment1.setReservation(reservation5);
        Long long7 = payment1.getFee();
        payment1.setCcName("hi!");
        payment1.setCcName("");
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + long7 + "' != '" + 1L + "'", long7.equals(1L));
    }

    @Test
    public void test496() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test496");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcNum("");
        payment0.setCcExpDate("");
        payment0.setCcName("hi!");
        String str10 = payment0.getCcExpDate();
        String str11 = payment0.getCcNum();
        Long long12 = payment0.getFee();
        payment0.setCcName("hi!");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str10 + "' != '" + "" + "'", str10.equals(""));
        org.junit.Assert.assertTrue("'" + str11 + "' != '" + "" + "'", str11.equals(""));
        org.junit.Assert.assertNull(long12);
    }

    @Test
    public void test497() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test497");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        payment0.setCcName("");
        payment0.setCcExpDate("hi!");
        payment0.setCcName("");
        payment0.setCcNum("");
        String str12 = payment0.getCcName();
        String str13 = payment0.getCcName();
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + str12 + "' != '" + "" + "'", str12.equals(""));
        org.junit.Assert.assertTrue("'" + str13 + "' != '" + "" + "'", str13.equals(""));
    }

    @Test
    public void test498() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test498");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 1L);
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation2 = payment1.getReservation();
        payment1.setCcExpDate("");
        payment1.setCcNum("hi!");
        Boolean b7 = payment1.isValid();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation8 = payment1.getReservation();
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation9 = null;
        payment1.setReservation(reservation9);
        payment1.setCcName("hi!");
        payment1.setCcExpDate("");
        org.junit.Assert.assertNull(reservation2);
        org.junit.Assert.assertTrue("'" + b7 + "' != '" + false + "'", b7.equals(false));
        org.junit.Assert.assertNull(reservation8);
    }

    @Test
    public void test499() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test499");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment0 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment();
        payment0.setCcNum("");
        Long long3 = payment0.getFee();
        Boolean b4 = payment0.isValid();
        Long long5 = payment0.getFee();
        payment0.setCcName("");
        org.junit.Assert.assertNull(long3);
        org.junit.Assert.assertTrue("'" + b4 + "' != '" + false + "'", b4.equals(false));
        org.junit.Assert.assertNull(long5);
    }

    @Test
    public void test500() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test500");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Payment payment1 = new edu.cmu.tartan.edu.cmu.tartan.reservation.Payment((Long) 10L);
        payment1.setCcName("");
        payment1.setCcExpDate("hi!");
        payment1.setCcNum("");
        payment1.setCcName("hi!");
        edu.cmu.tartan.edu.cmu.tartan.reservation.Reservation reservation10 = payment1.getReservation();
        payment1.setFee((Long) 100L);
        org.junit.Assert.assertNull(reservation10);
    }
}

