package edu.cmu.tartan.test;

import edu.cmu.tartan.service.TartanParams;
import edu.cmu.tartan.service.TartanServiceMessageBus;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.jms.*;

import java.util.HashMap;

import static org.junit.Assert.*;

/**
 * Created by sungyun.cho on 2017-08-01.
 */
public class TartanServiceMessageBusTest {


    String TARTAN_TOPIC = "TARTAN_TOPIC";

    MessageProducer outChannel;
    MessageConsumer inChannel;
    TartanServiceMessageBus bus;

    @Before
    public void setUp() throws Exception {

        //set-up the test fixture and test default constructor
        boolean exceptionOccured = false;
        try {
            bus = TartanServiceMessageBus.connect();
            assertNotNull("Failed creating TartanServiceMessageBus object with default constructor !!!",bus);
        } catch (Exception e) {
            exceptionOccured = true;
        }
        assertEquals("Exception occurred while creating TartanServiceMessageBus object with default constructor !!!",exceptionOccured, false);

    }

    @Test
    public void test_getConsumerMethod() throws Exception {
        inChannel = bus.getConsumer(TartanServiceMessageBus.TARTAN_TOPIC);

        //Test getConsumer Method
        if (inChannel == null) {
            Assert.assertTrue(false);
        }
    }

    @Test
    public void test_releaseConsumerMethod() throws Exception {
        inChannel = bus.getConsumer(TartanServiceMessageBus.TARTAN_TOPIC);

        //Test releaseConsumer Method
        bus.releaseConsumer(inChannel);
        if (inChannel == null) {
            Assert.assertTrue(false);
        }
    }

    @Test
    public void test_getProducerMethod() throws Exception {
        outChannel = bus.getProducer(TartanServiceMessageBus.TARTAN_TOPIC);

        //Test getProducer Method
        if (outChannel == null) {
            Assert.assertTrue(false);
        }
    }

    @Test
    public void test_ReleaseProducerMethod() throws Exception {
        outChannel = bus.getProducer(TartanServiceMessageBus.TARTAN_TOPIC);

        //Test releaseProducer Method
        bus.releaseProducer(outChannel);
        if (outChannel == null) {
            Assert.assertTrue(false);
        }
    }

    // why fail??? 
   @Test
   public void test_generateMessage() throws Exception {

       HashMap<String, Object> messageBody = null;
       String serviceId = null;
       String targetService = null;

       messageBody.put(TartanParams.SOURCE_ID, serviceId);
       ObjectMessage msg = bus.generateMessage(messageBody, targetService);

       if (msg == null) {
           Assert.assertTrue(false);
       }
   }

    @Test
    public void test_disconnect() throws Exception {
        bus.disconnect();
    }

}