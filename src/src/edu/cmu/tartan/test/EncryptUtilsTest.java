package edu.cmu.tartan.test;

import edu.cmu.tartan.utils.EncryptUtils;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by suraj.malsingh on 7/26/2017.
 */
public class EncryptUtilsTest {
    // Check if the encryption decription works or not
    @Test
    public void encryptAndDecrypt() {
        String original = "Suraj";
        String enc = EncryptUtils.encrypt(original);
        assertNotNull(enc);
        String dec = EncryptUtils.decrypt(enc);
        // System.out.println(dec);
        assertNotNull(dec);
        assertEquals(true, original.equals(dec));
    }

    // If tried to encrypt a Null string, return NULL
    @Test
    public void ecryptNull() {
        String enc = EncryptUtils.encrypt(null);
        //System.out.println(enc);
        assertEquals(null,enc);
    }

    // If tried to encrypt a empty string , return empty string
    @Test
    public  void ecryptEmpty() {
        String enc = EncryptUtils.encrypt("");
        // System.out.println(enc);
        assertEquals("",enc);
    }

    // If tried to decrypt a Null string, return NULL
    @Test
    public void decryptNull() {
        String dec = EncryptUtils.decrypt(null);
        //System.out.println(dec);
        assertEquals(null,dec);
    }

    // If tried to decrypt a empty string , return empty string
    @Test
    public void decryptEmpty() {
        String dec = EncryptUtils.decrypt("");
        System.out.println(dec);
        assertEquals("",dec);
    }

}