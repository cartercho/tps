package edu.cmu.tartan.utils;

import edu.cmu.tartan.hardware.TartanSensors;
import edu.cmu.tartan.service.TartanParams;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
/**
 * @author suraj.malsingh
 *
 */
public class LoggerUtil {

	private static BufferedWriter writer = null;
	public static SimpleDateFormat sdFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

	public  static void LOG(String msg) {

		if(msg == null)
			return;

		String formatedNow = null;

		File f = new File("MessageLogs.txt");

		try {
			formatedNow = sdFormat.format(new Date(System.currentTimeMillis()));
			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(f, true)));
			out.println(EncryptUtils.encrypt(formatedNow + "#" + msg));
			if(out != null){
				out.close();
				out =  null;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}

	public static void resetLog(){
		File f = new File("MessageLogs.txt");
		try {
			if(f.exists())
				f.delete();

			if ((f == null) || !f.exists()) {
				f.createNewFile();
				System.out.println(f.getAbsolutePath());
			}
		}catch(IOException e){
			System.out.println("resetLogs : Exception while creating file !!!");
		}
	}

	public static TreeMap<String,Integer> getAllMessageLogCount(Date startTime , Date endTime ){
		TreeMap<String,Integer>  result = new TreeMap<String,Integer>();
		String line = "";
		Date logged_messageTime = null;
		String logged_message = "";

		try {
			BufferedReader br = new BufferedReader(new FileReader("MessageLogs.txt"));
			while ((line = br.readLine()) != null) {
				line = EncryptUtils.decrypt(line);
				String arr[] = line.split("#");
				if(arr != null && arr.length >=2 ) {
					logged_messageTime = sdFormat.parse(arr[0]);
					logged_message = arr[1];

					if (logged_message != null && logged_messageTime != null &&
							(logged_messageTime.after(startTime) || logged_messageTime.equals(startTime))
							&& (logged_messageTime.before(endTime) || logged_messageTime.equals(endTime))) {

							if (result.containsKey(logged_message))
								result.put(logged_message, result.get(logged_message) + 1);
							else
								result.put(logged_message, 1);

					}
				}
			}

			if(br != null){
				br.close();;
				br = null;
			}
		}catch(IOException e){
			e.printStackTrace();
			System.out.println("getAllMessageLogCount : IOException  !!!");
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("getAllMessageLogCount : Exception !!!");
		}
		return result;
	}

	public static int getMessageLogCount(Date startTime , Date endTime , String message ){
		int count = 0;
		String line = "";
		Date logged_messageTime = null;
		String logged_message = "";
		try {
			BufferedReader br = new BufferedReader(new FileReader("MessageLogs.txt"));
			while ((line = br.readLine()) != null) {
				line = EncryptUtils.decrypt(line);
				String arr[] = line.split("#");

				if(arr != null && arr.length >= 2) {

					try {
						logged_messageTime = sdFormat.parse(arr[0]);
					} catch (ParseException e) {
						continue; // If there is problem parsing the time in current line , go to next line.
					}
					logged_message = arr[1];

					if (logged_message != null && logged_messageTime != null &&
							logged_message.equals(message) &&
							(logged_messageTime.after(startTime) || logged_messageTime.equals(startTime)) &&
							(logged_messageTime.before(endTime) || logged_messageTime.equals(endTime))) {
						count++;
					}
				}
			}
			if(br != null){
				br.close();;
				br = null;
			}
		}catch(IOException e){
			System.out.println("getMessageLogCount : IOException !!!");
			e.printStackTrace();
		}
		return count;
	}

	public static TreeMap<String,Integer> getErrorTypeCount(Date startTime , Date endTime  ){

		TreeMap<String,Integer>  result = new TreeMap<String,Integer>();

		int count = 0;
		String line = "";
		Date logged_messageTime = null;
		String logged_message = "";
		String logged_message_payload = "";
		try {
			BufferedReader br = new BufferedReader(new FileReader("MessageLogs.txt"));
			while ((line = br.readLine()) != null) {
				line = EncryptUtils.decrypt(line);
				String arr[] = line.split("#");

				if(arr != null && arr.length >= 2 ) {
					try {
						logged_messageTime = sdFormat.parse(arr[0]);
					} catch (ParseException e) {
						continue; // If there is problem parsing the time in current line , go to next line.
					}
					logged_message = arr[1];

					if(arr.length >=3) { // Possible to have error message without any payload
						if (logged_message_payload == null || logged_message_payload.isEmpty())
							logged_message_payload = TartanParams.REASON_UNKNOWN;
						else
							logged_message_payload = arr[2];
					}else
						logged_message_payload = TartanParams.REASON_UNKNOWN;
				}

				if (logged_message != null && logged_messageTime != null &&
						logged_message.equals(TartanParams.ERROR) && (logged_messageTime.after(startTime)  || logged_messageTime.equals(startTime)) &&
						(logged_messageTime.before(endTime)   || logged_messageTime.equals(endTime))) {

					if (result.containsKey(logged_message_payload))
						result.put(logged_message_payload, result.get(logged_message_payload) + 1);
					else
						result.put(logged_message_payload, 1);
				}
			}
			if(br != null){
				br.close();;
				br = null;
			}
		}catch(IOException e){
			System.out.println("getErrorTypeCount : IOException !!!");
		}
		return result;
	}

	public static int getTotalRevenue(Date startTime , Date endTime ){
		int revenue = 0;
		String line = "";
		Date logged_messageTime = null;
		String logged_message = "";
		String logged_fees = "";
		try {
			BufferedReader br = new BufferedReader(new FileReader("MessageLogs.txt"));
			while ((line = br.readLine()) != null) {
				line = EncryptUtils.decrypt(line);
				String arr[] = line.split("#");
				if(arr != null && arr.length >= 2) {
					try {
						logged_messageTime = sdFormat.parse(arr[0]);
					} catch (ParseException e) {
						continue; // If there is problem parsing the time in current line , go to next line.
					}
					logged_message = arr[1];
				}

				if (logged_message != null && logged_message.equals(TartanParams.MSG_PAYMENT_COMPLETE)){
					if(arr.length >= 9)
							logged_fees = arr[8].split(":")[1];
					else
							logged_fees = "0";

					if(logged_message != null && (logged_messageTime.after(startTime)  || logged_messageTime.equals(startTime)) &&
					    (logged_messageTime.before(endTime)   || logged_messageTime.equals(endTime))){
							revenue += Integer.parseInt(logged_fees);
						}

				}
			}
			if(br != null){
				br.close();;
				br = null;
			}
		}catch(IOException e){
			System.out.println("getTotalRevenue : IOException !!!");
			e.printStackTrace();
		}
		return revenue;
	}

	public static float getParkingOccupancyPercentage(Date startTime , Date endTime ){
		int totalOccupiedCount =0,totalUnoccupiedcount = 0;

		float percentage = 0;
		String line = "";
		Date logged_messageTime = null;
		String logged_message = "";
		try {
			BufferedReader br = new BufferedReader(new FileReader("MessageLogs.txt"));
			while ((line = br.readLine()) != null) {
				line = EncryptUtils.decrypt(line);
				String arr[] = line.split("#");

				if(arr != null && arr.length >= 2) {

					try {
						logged_messageTime = sdFormat.parse(arr[0]);
					} catch (ParseException e) {
						continue; // If there is problem parsing the time in current line , go to next line.
					}
					logged_message = arr[1];

					if (logged_message != null && logged_messageTime != null &&
							logged_message.equals(TartanSensors.CURRENT_GARAGE_PARKING_STATE) &&
							(logged_messageTime.after(startTime) || logged_messageTime.equals(startTime)) &&
							(logged_messageTime.before(endTime) || logged_messageTime.equals(endTime))) {

							if(arr.length >= 2){
								for(int i = 2; i < arr.length ; i++){
									if(!arr[i].equalsIgnoreCase("null") ){
										if(Integer.parseInt(arr[i]) == 0)
											totalUnoccupiedcount++;
										else
											totalOccupiedCount++;
									}
								}
							}
					}
				}
			}
			if(br != null){
				br.close();;
				br = null;
			}
			System.out.println("Percentage Count : " + totalOccupiedCount + "" + totalUnoccupiedcount);
			if(totalOccupiedCount+totalUnoccupiedcount > 0)
				percentage = (totalOccupiedCount*100 / (totalOccupiedCount+totalUnoccupiedcount)) ;
			System.out.println("Percentage  : " + percentage);

		}catch(IOException e){
			System.out.println("getParkingOccupancyPercentage : IOException !!!");
			e.printStackTrace();
		}catch(ArithmeticException e){
			System.out.println("getParkingOccupancyPercentage : ArithmeticException !!!");
			e.printStackTrace();
		}
		return percentage;
	}


}
